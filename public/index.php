<?php
/******************************************************************************
 * Application name: Open Source Time Tracker
 * Author: Matthew Taft
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

// Define path to application directory
define('APPLICATION_PATH', (getenv('APPLICATION_PATH') ? getenv('APPLICATION_PATH') : realpath(dirname(__FILE__) . '/../application')));

// Define application environment
define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Define base uri (might not be needed if not in sub directory)
define('BASE_URI', (getenv('BASE_URI') ? getenv('BASE_URI') : "/"));

// Define base uri (might not be needed if not in sub directory)
define('LIBRARY_PATH', (getenv('LIBRARY_PATH') ? getenv('LIBRARY_PATH') : realpath(APPLICATION_PATH . '/../library')));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    LIBRARY_PATH,
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();