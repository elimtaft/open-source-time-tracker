<?php
/******************************************************************************
 * Class name: Application_Model_TaskMapper
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_TaskMapper
{
    protected $_dbTable;
    
    public function setDbTable($dbTable)
    {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Task');
        }
        return $this->_dbTable;
    }    
    
    public function save(Application_Model_Task $task)
    {
        $data = array(
            'TaskID'   => $task->getTaskID(),
            'Name'   => $task->getName()
        );
 
        if (null === ($id = $task->getTaskID())) {
            unset($data['TaskID']);
            $this->getDbTable()->insert($data);
        } else {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('TaskID = ?' => $id));
            }
        }
    }
    
    public function find($id, Application_Model_Task $task)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $task->setTaskID($row->TaskID)
                ->setName($row->Name);
    }
 
    public function fetchAll($where = null, $order = null)
    {
        $resultSet = $this->getDbTable()->fetchAll($where, $order);
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Task();
            $entry->setTaskID($row->TaskID)
                ->setName($row->Name);
            $entries[] = $entry;
        }
        return $entries;
    }
}
?>
