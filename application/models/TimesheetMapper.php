<?php
/******************************************************************************
 * Class name: Application_Model_TimesheetMapper
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_TimesheetMapper
{
    protected $_dbTable;
    
    public function setDbTable($dbTable)
    {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Timesheet');
        }
        return $this->_dbTable;
    }    
    
    public function save(Application_Model_Timesheet $timesheet)
    {
        $data = array(
            'TimeSheetID' => $timesheet->getTimeSheetID(),
            'EmployeeID'   => $timesheet->getEmployeeID(),
            'CategoriesXTasksID'   => $timesheet->getCategoriesXTasksID(),
            'Date'   => $timesheet->getDate(),
            'Duration'   => $timesheet->getDuration(),
            'Comments'   => $timesheet->getComments());
 
        if (null === ($id = $timesheet->getTimeSheetID())) {
            unset($data['TimeSheetID']);
            $this->getDbTable()->insert($data);
        } else {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('TimeSheetID = ?' => $id));
            }
        }
    }
    
    public function find($id, Application_Model_Timesheet $timesheet)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $timesheet->setTimeSheetID($row->TimeSheetID)
                  ->setEmployeeID($row->EmployeeID)
                  ->setCategoriesXTasksID($row->CategoriesXTasksID)
                  ->setDate($row->Date)
                  ->setDuration($row->Duration)
                  ->setComments($row->Comments);
    }
 
    public function fetchAll($where = null, $order = null)
    {
        $resultSet = $this->getDbTable()->fetchAll($where, $order);
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Timesheet();
            $entry->setTimeSheetID($row->TimeSheetID)
                  ->setEmployeeID($row->EmployeeID)
                  ->setCategoriesXTasksID($row->CategoriesXTasksID)
                  ->setDate($row->Date)
                  ->setDuration($row->Duration)
                  ->setComments($row->Comments);
            $entries[] = $entry;
        }
        return $entries;
    }
}
?>
