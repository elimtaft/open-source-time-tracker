<?php
/******************************************************************************
 * Class name: Application_Model_DbTable_EmployeeCategories
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/


class Application_Model_DbTable_EmployeeCategories extends Zend_Db_Table_Abstract {

    /** Table name */
    protected $_name = 'employeesxcategories';
    
    /** Primary Key */
    protected $_primary = 'EmployeesXCategoriesID';
    

}
?>