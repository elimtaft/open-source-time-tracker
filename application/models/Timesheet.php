<?php
/******************************************************************************
 * Class name: Application_Model_Timesheet
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/


class Application_Model_Timesheet
{
    protected $_TimeSheetID;
    protected $_EmployeeID;
    protected $_CategoriesXTasksID;
    protected $_Date;
    protected $_Duration;
    protected $_Comments;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid CategoryTask property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid CategoryTask property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** TimeSheetID */
    public function setTimeSheetID($id)
    {
        $this->_TimeSheetID = (int) $id;
        return $this;
    }
    public function getTimeSheetID()
    {
        return $this->_TimeSheetID;
    }
    
    /** EmployeeID */
    public function setEmployeeID($id)
    {
        $this->_EmployeeID = (int) $id;
        return $this;
    }
    public function getEmployeeID()
    {
        return $this->_EmployeeID;
    }
    
    /** CategoriesXTasksID */
    public function setCategoriesXTasksID($id)
    {
        $this->_CategoriesXTasksID = (int) $id;
        return $this;
    }
    public function getCategoriesXTasksID()
    {
        return $this->_CategoriesXTasksID;
    }

    /** Date */
    public function setDate($text)
    {
        $this->_Date = (string) $text;
        return $this;
    }
    public function getDate()
    {
        return $this->_Date;
    }
    
    /** Duration */
    public function setDuration($text)
    {
        $this->_Duration = (string) $text;
        return $this;
    }
    public function getDuration()
    {
        return $this->_Duration;
    }

    /** Comments */
    public function setComments($text)
    {
        $this->_Comments = (string) $text;
        return $this;
    }
    public function getComments()
    {
        return $this->_Comments;
    }

}
?>
