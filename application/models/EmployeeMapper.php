<?php
/******************************************************************************
 * Class name: Application_Model_EmployeeMapper
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_EmployeeMapper
{
    protected $_dbTable;
    
    public function setDbTable($dbTable)
    {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Employee');
        }
        return $this->_dbTable;
    }    
    
    public function save(Application_Model_Employee $employee)
    {
        $data = array(
            'EmployeeID'   => $employee->getEmployeeID(),
            'DepartmentID'   => $employee->getDepartmentID(),
            'Name'   => $employee->getName(),
            'Username'   => $employee->getUsername(),
            'Password' => $employee->getPassword(),
            'IsAdmin' => $employee->getIsAdmin(),
            'IsReporter' => $employee->getIsReporter(),
            'Active' => $employee->getActive()
        );
 
        if (null === ($id = $employee->getEmployeeID())) {
            unset($data['EmployeeID']);
            $this->getDbTable()->insert($data);
        } else {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('EmployeeID = ?' => $id));
            }
        }
    }
    
    public function find($id, Application_Model_Employee $employee)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $employee->setEmployeeID($row->EmployeeID)
                ->setDepartmentID($row->DepartmentID)
                ->setName($row->Name)
                ->setUsername($row->Username)
                ->setPassword($row->Password)
                ->setIsAdmin($row->IsAdmin)
                ->setIsReporter($row->IsReporter)
                ->setActive($row->Active);
        return $employee;
    }
 
    public function fetchAll($where = null, $order = null)
    {
        $resultSet = $this->getDbTable()->fetchAll($where, $order);
        
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Employee();
            $entry->setEmployeeID($row->EmployeeID)
                ->setDepartmentID($row->DepartmentID)
                ->setName($row->Name)
                ->setUsername($row->Username)
                ->setPassword($row->Password)
                ->setIsAdmin($row->IsAdmin)
                ->setIsReporter($row->IsReporter)
                ->setActive($row->Active);
            $entries[] = $entry;
        }
        return $entries;
    }
    
}
?>
