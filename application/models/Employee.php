<?php
/******************************************************************************
 * Class name: Application_Model_Employee
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/


class Application_Model_Employee
{
    protected $_EmployeeID;
    protected $_DepartmentID;
    protected $_Name;
    protected $_Username;
    protected $_Password;
    protected $_IsAdmin;
    protected $_IsReporter;
    protected $_Active;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Department property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Department property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** EmployeeID */
    public function setEmployeeID($id)
    {
        $this->_EmployeeID = (int) $id;
        return $this;
    }
    public function getEmployeeID()
    {
        return $this->_EmployeeID;
    }

    /** DepartmentID */
    public function setDepartmentID($id)
    {
        $this->_DepartmentID = (int) $id;
        return $this;
    }
    public function getDepartmentID()
    {
        return $this->_DepartmentID;
    }
    
    /** Name */
    public function setName($text)
    {
        $this->_Name = (string) $text;
        return $this;
    }
    public function getName()
    {
        return $this->_Name;
    }

    /** Username */
    public function setUsername($text)
    {
        $this->_Username = (string) $text;
        return $this;
    }
    public function getUsername()
    {
        return $this->_Username;
    }    

    /** Password */
    public function setPassword($text)
    {
        $this->_Password = (string) $text;
        return $this;
    }
    public function getPassword()
    {
        return $this->_Password;
    }

    /** IsAdmin */
    public function setIsAdmin($id)
    {
        $this->_IsAdmin = (int) $id;
        return $this;
    }
    public function getIsAdmin()
    {
        return $this->_IsAdmin;
    }
    public function isAdmin()
    {
        if($this->_IsAdmin == 1)
        {
            return true;
        } else {
            return false;
        }
    }

    /** IsReporter */
    public function setIsReporter($id)
    {
        $this->_IsReporter = (int) $id;
        return $this;
    }
    public function getIsReporter()
    {
        return $this->_IsReporter;
    }
    public function isReporter()
    {
        if($this->_IsReporter == 1)
        {
            return true;
        } else {
            return false;
        }
    }
    
    /** Active */
    public function setActive($id)
    {
        $this->_Active = (int) $id;
        return $this;
    }
    public function getActive()
    {
        return $this->_Active;
    }


}
?>
