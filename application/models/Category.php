<?php
/******************************************************************************
 * Class name: Application_Model_Category
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_Category
{
    protected $_CategoryID;
    protected $_ClientID;
    protected $_Name;
    protected $_Description;
    protected $_DateCreated;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Category property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Category property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** CategoryID */
    public function setCategoryID($id)
    {
        $this->_CategoryID = (int) $id;
        return $this;
    }
    public function getCategoryID()
    {
        return $this->_CategoryID;
    }
    
    /** ClientID */
    public function setClientID($id)
    {
        $this->_ClientID = (int) $id;
        return $this;
    }
    public function getClientID()
    {
        return $this->_ClientID;
    }

    /** Name */
    public function setName($text)
    {
        $this->_Name = (string) $text;
        return $this;
    }
    public function getName()
    {
        return $this->_Name;
    }

    /** DateCreated */
    public function setDateCreated($text)
    {
        $this->_DateCreated = (string) $text;
        return $this;
    }
    public function getDateCreated()
    {
        return $this->_DateCreated;
    }


}
?>
