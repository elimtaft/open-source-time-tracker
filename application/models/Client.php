<?php
/******************************************************************************
 * Class name: Application_Model_Client
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_Client
{
    protected $_ClientID;
    protected $_Name;
    protected $_City;
    protected $_State;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Client property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Client property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** ClientID */
    public function setClientID($id)
    {
        $this->_ClientID = (int) $id;
        return $this;
    }
    public function getClientID()
    {
        return $this->_ClientID;
    }

    /** Name */
    public function setName($text)
    {
        $this->_Name = (string) $text;
        return $this;
    }
    public function getName()
    {
        return $this->_Name;
    }

    /** City */
    public function setCity($text)
    {
        $this->_City = (string) $text;
        return $this;
    }
    public function getCity()
    {
        return $this->_City;
    }

    /** State */
    public function setState($text)
    {
        $this->_State = (string) $text;
        return $this;
    }
    public function getState()
    {
        return $this->_State;
    }    

}
?>
