<?php
/******************************************************************************
 * Class name: Application_Model_CategoryTasks
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_CategoryTasks
{
    protected $_CategoriesXTasksID;
    protected $_CategoryID;
    protected $_TaskID;
    protected $_Active;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid CategoryTask property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid CategoryTask property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** CategoriesXTasksID */
    public function setCategoriesXTasksID($id)
    {
        $this->_CategoriesXTasksID = (int) $id;
        return $this;
    }
    public function getCategoriesXTasksID()
    {
        return $this->_CategoriesXTasksID;
    }
    
    /** CategoryID */
    public function setCategoryID($id)
    {
        $this->_CategoryID = (int) $id;
        return $this;
    }
    public function getCategoryID()
    {
        return $this->_CategoryID;
    }
    
    /** TaskID */
    public function setTaskID($id)
    {
        $this->_TaskID = (int) $id;
        return $this;
    }
    public function getTaskID()
    {
        return $this->_TaskID;
    }

    /** Active */
    public function setActive($id)
    {
        $this->_Active = (int) $id;
        return $this;
    }
    public function getActive()
    {
        return $this->_Active;
    }

    


}
?>
