<?php
/******************************************************************************
 * Class name: Application_Model_EmployeeInfo
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      Models in Zend Framework are used in specific ways and is well documented,
 *      so there is really no need to describe each model listed in the models
 *      directory.  However, this model is special.  It doesn't relate to a specific
 *      table or object in the MySQL database.  Rather, it's a custom Object that
 *      gathers together pertinent information about an Employee.  By gathering this
 *      data all at once and storing it in the Employees session at the time of login,
 *      this reduces the amount of MySQL queries needed to display data on various
 *      pages.  The downside to this is that an Employee who gets assigned to
 *      a new category will need to log out, then log back in before that category
 *      will be available to them.
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 * 
 ******************************************************************************/

class Application_Model_EmployeeInfo
{
    protected $_EmployeeID;
    protected $_DepartmentID;
    protected $_DepartmentName;
    protected $_Name;
    protected $_Username;
    protected $_Password;
    protected $_IsAdmin;
    protected $_IsReporter;
    protected $_Active;
    protected $_Clients;
    protected $_Categories;
    protected $_Tasks;
    protected $_CCT;
    protected $_TimeSheetEntryIDs;
    protected $_CreateUsers_MustBeAdmin;
    protected $_CreateUsers_MustBeInSpecificDepartment;
    protected $_CreateUsers_MustBeInSpecificDepartmentID;
    protected $_CreateAdminUsers_MustBeAdmin;
    protected $_CreateAdminUsers_MustBeInSpecificDepartment;
    protected $_CreateAdminUsers_MustBeInSpecificDepartmentID;
    protected $_eiArray;
    
    public function getDataArray() {
        return $this->_eiArray;
    }

    
    public function __construct(array $options = null)
    {
        $this->_eiArray = $options;
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Employee property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Employee property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** EmployeeID */
    public function setEmployeeID($id)
    {
        $this->_EmployeeID = (int) $id;
        return $this;
    }
    public function getEmployeeID()
    {
        return $this->_EmployeeID;
    }

    /** DepartmentID */
    public function setDepartmentID($id)
    {
        $this->_DepartmentID = (int) $id;
        return $this;
    }
    public function getDepartmentID()
    {
        return $this->_DepartmentID;
    }
    
    /** DepartmentName */
    public function setDepartmentName($text)
    {
        $this->_DepartmentName = (string) $text;
        return $this;
    }
    public function getDepartmentName()
    {
        return $this->_DepartmentName;
    }
    
    /** Name */
    public function setName($text)
    {
        $this->_Name = (string) $text;
        return $this;
    }
    public function getName()
    {
        return $this->_Name;
    }

    /** Username */
    public function setUsername($text)
    {
        $this->_Username = (string) $text;
        return $this;
    }
    public function getUsername()
    {
        return $this->_Username;
    }    

    /** Password */
    public function setPassword($text)
    {
        $this->_Password = (string) $text;
        return $this;
    }
    public function getPassword()
    {
        return $this->_Password;
    }

    /** IsAdmin */
    public function setIsAdmin($id)
    {
        $this->_IsAdmin = (int) $id;
        return $this;
    }
    public function getIsAdmin()
    {
        return $this->_IsAdmin;
    }
    public function isAdmin()
    {
        if($this->_IsAdmin == 1)
        {
            return true;
        } else {
            return false;
        }
    }

    /** IsReporter */
    public function setIsReporter($id)
    {
        $this->_IsReporter = (int) $id;
        return $this;
    }
    public function getIsReporter()
    {
        return $this->_IsReporter;
    }
    public function isReporter()
    {
        if($this->_IsReporter == 1)
        {
            return true;
        } else {
            return false;
        }
    }
    
    
    /** Active */
    public function setActive($id)
    {
        $this->_Active = (int) $id;
        return $this;
    }
    public function getActive()
    {
        return $this->_Active;
    }
    
    
    /** Clients */
    public function setClients($clientsArray)
    {

        $this->_Clients = $clientsArray;
        return $this;
    }
    public function getClients()
    {
        return $this->_Clients;
    }    
    
    /** Categories */
    public function setCategories($categoriesArray)
    {
        $this->_Categories = $categoriesArray;
        return $this;
    }
    public function getCategories()
    {
        return $this->_Categories;
    }
    public function getCategoriesByClientID($ClientID)
    {
        return $this->_Categories[$ClientID];
    }    

    /** Tasks */
    public function setTasks($tasksArray)
    {
        $this->_Tasks = $tasksArray;
        return $this;
    }
    public function getTasks()
    {
        return $this->_Tasks;
    }    
    public function getTasksByCategoryId($CategoryID)
    {
        return $this->_Tasks[$CategoryID];
    }    

    /** Get Client -> Category -> Task Hierarchy */
    public function setCCT($cctArray)
    {
        $this->_CCT = $cctArray;
        return $this;
    }
    public function getCCT()
    {
        return $this->_CCT;
    }    

    
    /** Tasks */
    public function setTimeSheetEntryIDs($tseiArray)
    {
        $this->_TimeSheetEntryIDs = $tseiArray;
        return $this;
    }
    public function getTimeSheetEntryIDs()
    {
        return $this->_TimeSheetEntryIDs;
    }      
    
    public function timesheetEntryExists($id)
    {
        foreach($this->_TimeSheetEntryIDs['rawIDs'] as $key => $val)
        {
          if($id==$val) return true;
        }
        return false;
    }

    /*
     * This function returns true if users are allowed to create other users.
     * 
     */
    public function canCreateUsers()
    {
        // This will never happen, because all functions related to creating
        // users are in the admin controller, which only allows access to admins.
        if($this->getCreateUsers_MustBeAdmin()==false) return true;
        
        // If all admins can create accounts, just return true.
        if($this->getCreateUsers_MustBeInSpecificDepartment()==false) return true;
        
        // Otherwise:
        if($this->getDepartmentID()==$this->getCreateUsers_MustBeInSpecificDepartmentID()) return true;
        
        return false;
        
    }
    
    public function setCreateUsers_MustBeAdmin($int)
    {
        $this->_CreateUsers_MustBeAdmin = (int) $int;
    }
    public function getCreateUsers_MustBeAdmin()
    {
        return $this->_CreateUsers_MustBeAdmin;
    }
    
    public function setCreateUsers_MustBeInSpecificDepartment($int)
    {
        $this->_CreateUsers_MustBeInSpecificDepartment = (int) $int;
    }
    public function getCreateUsers_MustBeInSpecificDepartment()
    {
        return $this->_CreateUsers_MustBeInSpecificDepartment;
    }

    public function setCreateUsers_MustBeInSpecificDepartmentID($int)
    {
        $this->_CreateUsers_MustBeInSpecificDepartmentID = (int) $int;
    }    
    public function getCreateUsers_MustBeInSpecificDepartmentID()
    {
        return $this->_CreateUsers_MustBeInSpecificDepartmentID;
    }
    
    public function canCreateAdminUsers()
    {
        // This will never happen, because all functions related to creating
        // users are in the admin controller, which only allows access to admins.
        if($this->getCreateAdminUsers_MustBeAdmin()==false) return true;
        
        // If all admins can create accounts, just return true.
        if($this->getCreateAdminUsers_MustBeInSpecificDepartment()==false) return true;
        
        // Otherwise:
        if($this->getDepartmentID()==$this->getCreateAdminUsers_MustBeInSpecificDepartmentID()) return true;
        
        return false;
    }
    
    public function setCreateAdminUsers_MustBeAdmin($int)
    {
        $this->_CreateAdminUsers_MustBeAdmin = (int) $int;
    }
    public function getCreateAdminUsers_MustBeAdmin()
    {
        return $this->_CreateAdminUsers_MustBeAdmin;
    }
    
    public function setCreateAdminUsers_MustBeInSpecificDepartment($int)
    {
        $this->_CreateAdminUsers_MustBeInSpecificDepartment = (int) $int;
    }
    public function getCreateAdminUsers_MustBeInSpecificDepartment()
    {
        return $this->_CreateAdminUsers_MustBeInSpecificDepartment;
    }
    
    public function setCreateAdminUsers_MustBeInSpecificDepartmentID($int)
    {
        $this->_CreateAdminUsers_MustBeInSpecificDepartmentID = (int) $int;
    }
    public function getCreateAdminUsers_MustBeInSpecificDepartmentID()
    {
        return $this->_CreateAdminUsers_MustBeInSpecificDepartmentID;
    }
    
}
?>
