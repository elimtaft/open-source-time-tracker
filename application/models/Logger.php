<?php
/******************************************************************************
 * Class name: Application_Model_Logger
 * Author: Matthew Taft
 * Date: 02/01/2012
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Application_Model_Logger
{
    protected $_LogID;
    protected $_TimesheetID;
    protected $_Date;
    protected $_EditorEmployeeID;
    protected $_EditDescription;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Log property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Log property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /** setLogID */
    public function setLogID($id)
    {
        $this->_LogID = (int) $id;
        return $this;
    }
    public function getLogID()
    {
        return $this->_LogID;
    }

    
    /** TimesheetID */
    public function setTimesheetID($id)
    {
        $this->_TimesheetID = (int) $id;
        return $this;
    }
    public function getTimesheetID()
    {
        return $this->_TimesheetID;
    }

    /** Date */
    public function setDate($text)
    {
        $this->_Date = (string) $text;
        return $this;
    }
    public function getDate()
    {
        return $this->_Date;
    }
    
    /** EditorEmployeeID */
    public function setEditorEmployeeID($id)
    {
        $this->_EditorEmployeeID = (int) $id;
        return $this;
    }
    public function getEditorEmployeeID()
    {
        return $this->_EditorEmployeeID;
    }
    
    /** EditDescription */
    public function setEditDescription($text)
    {
        $this->_EditDescription = (string) $text;
        return $this;
    }
    public function getEditDescription()
    {
        return $this->_EditDescription;
    }

}
?>
