<?

class Zend_View_Helper_JsPage extends Zend_View_Helper_Abstract {

	public $view;
	
	private $_relPath;
	private $_absPath;
	private $_exists;

	public function setView(Zend_View_Interface $view) {
		$this->view = $view;
	}

	public function jsPage() {
		
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$actionName = $request->getActionName();
		$controllerName = $request->getControllerName();
                $jsConfig = Zend_Registry::get('config')->javascript;
                
		$this->_relPath = $jsConfig->pages->relPath . '/' . $controllerName . '/' . $actionName . '.js';
		$this->_absPath = $_SERVER['DOCUMENT_ROOT'] . $this->_relPath;
		$this->_exists = file_exists($this->_absPath);
		
		return $this;
	}

	public function getAbsolutePath() {
		return $this->_absPath;
	}

	public function getRelativePath() {
		return $this->_relPath;
	}

	public function exists() {
		return $this->_exists;
	}
}

?>
