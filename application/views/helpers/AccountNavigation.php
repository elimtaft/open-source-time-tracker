<?php

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';


class Zend_View_Helper_AccountNavigation extends
                Zend_View_Helper_Abstract
{
    protected $_baseURI;
    

    
    public function AccountNavigation() {
        $this->_baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
        return $this;
    }
    
    public function getMainTabs() {
        
        $isAdmin=false;
        $isReporter=false;
        $html = '
        <ul>';
        
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()) {
            if(Zend_Registry::get('EmployeeInfo'))
            {
                $ei = Zend_Registry::get('EmployeeInfo');
                if($ei->isAdmin())
                { 
                    $isAdmin=true;
                }
                
                if($ei->isReporter())
                {
                    $isReporter=true;
                }
            }
        }
        
        if($isAdmin)
        { 
            $html .= '
            <li class="home"><a href="'.$this->_baseURI.'/admin">Admin</a></li>
            <li><a href="'.$this->_baseURI.'/account">Account</a></li>';
        } else {
            $html .= '
            <li class="home"><a href="'.$this->_baseURI.'/account">Account</a></li>';
        }        
        
        $html .= '
            <li><a href="'.$this->_baseURI.'/timesheet/new-entry" class="add-update-entries">Add / Update Entries</a></li>
            <li><a href="'.$this->_baseURI.'/timesheet/history" class="view-entries">View Entries</a></li>
            <li><a href="'.$this->_baseURI.'/auth/logout">Logout</a></li>
        </ul>'."\r\n";

        return $html;
    }
    
    public function getGreeting()
    {
        $auth = Zend_Auth::getInstance();
        if(!$auth->hasIdentity()) {
            return 'Welcome, <i>guest</i>.';
        } else {
            
            $data = $auth->getStorage()->read();
            $username = !(is_array($data) || is_object($data)) ? $data : $data->Username;
            return 'Welcome, <i>'.$username.'</i>  ';
        }
    }
    
    public function printAdminOptions()
    {
        
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()) {
            if(Zend_Registry::get('EmployeeInfo'))
            {
                $ei = Zend_Registry::get('EmployeeInfo');
                if($ei->isAdmin())
                { 
                    ?>
                    <div class="admin-navigation-options">
                        <?php if($ei->isReporter()) { ?>
                        <a href="<?php echo $this->_baseURI; ?>/reporting">Reports</a>
                        <?php } ?>
                        <a href="<?php echo $this->_baseURI; ?>/admin/list-departments">Departments</a>
                        <a href="<?php echo $this->_baseURI; ?>/admin/list-employees">Employees</a>
                        <a href="<?php echo $this->_baseURI; ?>/admin/list-tasks">Tasks</a>
                        <a href="<?php echo $this->_baseURI; ?>/admin/list-categories">Categories</a>
                        <a href="<?php echo $this->_baseURI; ?>/admin/list-clients">Clients</a>
                    </div>
                    <?php
                } elseif($ei->isReporter()) {
                    ?>
                        <div class="admin-navigation-options">
                        <a href="<?php echo $this->_baseURI; ?>/reporting">Reports</a>
                        </div>
                    <?php
                }

            }
        }
    }
}
?>
