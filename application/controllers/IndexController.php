<?php

/******************************************************************************
 * Class name: IndexController
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      This controller is the default controller on the site.
 * 
 *      Once the application is set up, this page kind of becomes useless.
 * 
 *      However, during setup, if the CheckSetup plugin finds any errors, this
 *      page displays those errors.  So this controller ends up "holding hands"
 *      with the CheckSetup plugin until everything is working good. 
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class IndexController extends Zend_Controller_Action
{

    protected $_baseURI;
    protected $_ei;
    protected $_setupInProgress;
    
    public function init()
    {
        
        $this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
   }

    public function indexAction()
    {
        
        $error = $this->_request->getParam('error');
        if($error && $error != '')
        {
            $this->checkSetupComplete();
        } else {

            $auth = Zend_Auth::getInstance();
            if(!$auth->hasIdentity()) {
                $this->_redirect('/auth/');
            } else {

                $this->view->title = 'Main Page';
                $this->view->content = '<p>This is the main page.  Please click an option from the top navigation bar.</p>';
                $this->_helper->EmployeeInfo->createObject();
                $this->_ei = Zend_Registry::get('EmployeeInfo');
                $this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
            }
        }
    }
    
    
    private function checkSetupComplete()
    {
        $error = $this->_request->getParam('error');
        if($error && $error != '')
        {
            
            $this->view->title = 'Initialization Error';

            $reason = '';
            
            switch($error)
            {
                
                case 'DbVariablesNotSet':
                    $reason = '
                    Could not retrieve MySQL database information.  Please
                    open the configuration file (/application/configs/application.ini) 
                    and edit the following settings: <br><br>
                    
                    <strong>resources.db.params.host = ""</strong><br>
                    This is where you place the address to your MySQL server, such 
                    as "localhost", "yourdomain.com", etc.<br><br>
                    
                    <strong>resources.db.params.username = ""</strong><br>
                    This is the MySQL Username.<br><br>
                    
                    <strong>resources.db.params.password = ""</strong><br>
                    This is the MySQL Password.<br><br> 
                    
                    <strong>resources.db.params.dbname = ""</strong><br>
                    This is the name of the database, such as "timetracker".  This
                    database needs to be created by you, and you must grant the 
                    MySQL user full access to this database.<br><br>
                    <div style="text-align:center;">
                        <a href="'.$this->_baseURI.'">Retry</a>
                    </div>
                    ';
                    break;
                
                case 'DbConnectionInvalid':
                    $reason = '
                    Could not connect to the MySQL host using the provided information.  Please
                    verify your settings in /application/configs/application.ini.<br><br>
                    <div style="text-align:center;">
                        <a href="'.$this->_baseURI.'">Retry</a>
                    </div>
                    ';
                    break;
                
                case 'DbDatabaseNameInvalid':
                    $reason = '
                    A connection to the MySQL server was successful; however, the 
                    database specified could not be accessed.  Please
                    verify your settings in /application/configs/application.ini.<br><br>
                    Specifically, check this value:<br>
                    <strong>resources.db.params.dbname = "..."<br><br>
                    <div style="text-align:center;">
                        <a href="'.$this->_baseURI.'">Retry</a>
                    </div>
                    ';
                    break;
                    
                case 'DbTablesMissing':
                    $reason = '
                    A connection to the MySQL database was successful, but there
                    are no tables.  This probably means this is a fresh install.  
                    Would you like Time Tracker to now create the
                    necessary tables?<br><br>
                    <div style="text-align:center;">
                        <a href="'.$this->_baseURI.'?createTables=yes">Yes</a>
                    </div>
                    ';
                    break;
                case 'NoAdminAccount':
                    $reason = '
                    This appears to be a fresh install.  Please enter a username 
                    and password for the main site administrator account.  You 
                    will need this account to log in to the system.<br><br>';
                    
                    $action = $this->_baseURI.'/index';

                    $form = new Zend_Form();
                    $form->setAction($action)
                         ->setMethod('post');
                    $form->setAttrib('class', 'employee');

                    $name = $form->createElement('text', 'name');
                    $name->setLabel('Full name')
                            ->setRequired(true);

                    $username = $form->createElement('text', 'username');
                    $username->setLabel('Username')
                                ->addValidator('callback', false, array(
                                        'callback' => array($this, 'usernameUnique'),
                                        'messages' => array(
                                            Zend_Validate_Callback::INVALID_VALUE => 'An employee with this username already exists.'
                                        )
                                    ))
                                ->setRequired(true);

                    $PasswordHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('Password');
                    $newPasswordUnhashed = $PasswordHelper->generateNew('nameNotRequired');
                    $password = $form->createElement('text', 'password');
                    $password->setLabel('Password')
                                ->addValidator('stringLength', false, array('min' => '6'))
                                ->setValue($newPasswordUnhashed)
                                ->setRequired(true);

                    $form->addElement($name)
                            ->addElement($username)
                            ->addElement($password)
                            ->addElement('submit', 'submit', array('label' => 'Submit'));
            
                    $reason .= $form;
                
                    break;
            }
            
            $this->view->content = '<div id="errors-messages">
                Failed to initialize for the following reason(s):<br><br>'.$reason.'
                </div>';
            
        }
    }
}

