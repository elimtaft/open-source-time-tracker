<?php

/******************************************************************************
 * Class name: AdminController
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      This is the admin controller, containing all the code relevant to the
 *      Admin account.  I still need to work on adding useful comments throughout
 *      this monster, and although it is recommended to try and keep controllers
 *      as small as possible, the reality is that this tool is primarily an Admin
 *      tool.  The regular user acocunts can't do much other than enter time and
 *      view previously entered time.  The Admin creates, edits and deletes clients,
 *      categories, tasks, employees, departments, and reports.  It makes sense to
 *      place all that code in an Admin controller whose init() function kicks 
 *      out anyone who isn't an admin on the site.  I feel it protects the code
 *      better.
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';

class AdminController extends Zend_Controller_Action
{
    
    protected $_baseURI;
    protected $_ei;
    protected $_zend_action;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if(!$auth->hasIdentity()) {
            return $this->_redirect('/auth/');
            exit();
        }

        $this->_helper->EmployeeInfo->createObject();
        $this->_ei = Zend_Registry::get('EmployeeInfo');
        if(!$this->_ei->isAdmin()) {
            return $this->_redirect('/');
            exit();
        }
        
        $this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->_zend_action = $this->view->zend_action = $this->getRequest()->getActionName();

    }

    public function indexAction() 
    {
        $this->view->title = 'Administration';
    }
    
    
    
    
    
    /**************************************************************************
     * 
     * Employee Actions
     * 
     **************************************************************************/

    public function listEmployeesAction()
    {
        $this->view->title = 'List Employees';
        if($this->_ei->canCreateUsers()) {
            $html = '<a class="createNew" href="'.$this->_baseURI . '/admin/create-employee'.'">Create New</a>';
        } else {
            // @codeCoverageIgnoreStart
            $html = '';
            // @codeCoverageIgnoreEnd
        }
        
        $html .= '<div id="table-wrapper"><table class="administration-details-table">';
        $html .= '<tr class="first">';

        if($this->_ei->canCreateUsers()) {
             $html .= '
                <td class="icon"></td>
                <td class="icon"></td>
                <td class="icon"></td>';
                }
        if($this->_ei->isReporter()) {
            $html .= '
                <td class="icon"></td>';
        }

        $html .= '
                <td>Employee</td>
                <td>Department</td>
                <td>Is Admin</td>
                <td>Is Reporter</td>
                <td>Status</td>
            </tr>'; 



        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                     ->from(
                            array(
                                'e' => 'employees'
                            ),
                            array(
                                'e.EmployeeID',
                                'EmployeeName' => 'e.Name', 
                                'DepartmentName' => 'd.Name',
                                'e.IsAdmin',
                                'e.IsReporter',
                                'e.Active'
                            )
                        )
                    ->join(
                            array(
                                'd' => 'departments'
                            ),
                            'e.DepartmentID = d.DepartmentID'
                        )
                    ->order(
                        'e.Name'
                        );
        
        
        $statement = $db->query($select);
        $employees = $statement->fetchAll();   
        if(count($employees) > 0)
        {
            foreach($employees as $employee)
            {
                $isAdmin = ($employee['IsAdmin']==1 ? 'Yes' : 'No');
                $isReporter = ($employee['IsReporter']==1 ? 'Yes' : 'No');
                $status =  ($employee['Active']==1 ? 'Active' : 'Inactive');

                if($this->_ei->canCreateUsers()) {
                     $html .= '
                        <td class="icon">
                          <a href="'. $this->_baseURI.'/admin/edit-employee/?id='.$employee['EmployeeID'].'">
                            <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                          </a>
                        </td>
                        <td class="icon">
                          <a href="'. $this->_baseURI.'/admin/assign-employee-to-categories/?EmployeeID='.$employee['EmployeeID'].'">
                            <img src="'.$this->_baseURI.'/images/users.png'.'" alt="Assign to Categories" title="Assign to Categories" />
                          </a>
                        </td>
                            <td class="icon">
                              <a href="'. $this->_baseURI.'/admin/reset-employee-password/?eid='.$employee['EmployeeID'].'">
                                <img src="'.$this->_baseURI.'/images/reset_password.png'.'" alt="Reset Password" title="Reset Password" />
                              </a>
                            </td>';
                        }
                if($this->_ei->isReporter()) {
                    $html .= '
                        <td class="icon">
                          <a href="'. $this->_baseURI.'/timesheet/history/?eid='.$employee['EmployeeID'].'">
                            <img src="'.$this->_baseURI.'/images/calender.png'.'" alt="Reporting" title="Reporting" />
                          </a>
                        </td>';
                }
                            
                $html .= '
                    <td>'.$employee['EmployeeName'].'</td>
                    <td>'.$employee['DepartmentName'].'</td>
                    <td>'.$isAdmin.'</td>
                    <td>'.$isReporter.'</td>                            
                    <td>'.$status.'</td>
                </tr>'; 
            }
        } else {
            $html .= '<tr><td colspan="9" align="center"><br />It appears that there are no employees.  How did you get here???<br /><br /></td></tr>';
        }
        
        $html .= '</table></div>';
        $this->view->listTable = $html;
    }
    
    
    // Main function for assigning employees to categories
    // Because there are so many categories, this page gives the user the option
    // to only show categories for a single client, sorted by unique name (applies to
    // ALL clients, or to show all (takes the longest)
    public function assignEmployeeToCategoriesAction()
    {
        
        $EmployeeID = $this->getRequest()->getParam('EmployeeID');
        if(!$EmployeeID)
        {
            return $this->_redirect ('/admin/list-employees');
            exit();
        }
        
        $this->view->messages = array();
        
        
        
        // To Assign Employee to ALL categories of this name:
        $ByName = $this->getRequest()->getParam('ByName');
        if($ByName)
        {
            $CategoryName = $this->getRequest()->getParam('CategoryName');
            $this->view->messages[] = $this->assignEmployeeToCategoryName($EmployeeID,$CategoryName);
        }
        
        
        
        // To Remove Employee from ALL categories of this name:
        $RemoveByName = $this->getRequest()->getParam('RemoveByName');
        if($RemoveByName) 
        {
            $CategoryName = $this->getRequest()->getParam('CategoryName');
            $this->view->messages[] = $this->unassignEmployeeFromCategoryName($EmployeeID,$CategoryName);
        }
        
        
        
        $this->view->title = 'Assign Employee to Categories';
        $this->view->introduction = '<p>Note: The list of categories is getting 
            really long.  Choose whether to view the entire list (be prepared to wait),
            view the list by a particular client only, or to view the list by 
            unique category name (this would assign the employee to every category with
            that name for every client (Client -> Category).</p>';
        
        
        $html = '<div id="employee-assign">';
        
        // CREATE THE FORMS FOR VARIOUS ASSIGN OPTIONS

        // By Client
        $html .= '
            <div class="section">
            <p><strong>Assign to categories of a particular client:</strong></p>
            <form method="get" action="'.$this->_baseURI .'/admin/assign-employee-to-client-categories/">
            <input type="hidden" name="EmployeeID" value="'.$EmployeeID.'" />
            <select name="ClientID">';
            $clients = $this->_helper->form->getClientSelect();
            foreach($clients as $ClientID => $ClientName)
            {
                $html .= '<option value="'.$ClientID.'">'.$ClientName.'</option>';
            }
            $html .= '
            </select>
            (coming soon)
            <!-- <input type="submit" value="Go" /> -->
            </form>
            </div>
            ';

            
        // By Unique Category Name
        $html .= '
            <div class="section">
            <p><strong>Assign to ALL categories by name:</strong></p>
            <form method="post" action="'.$this->_baseURI .'/admin/assign-employee-to-categories/?EmployeeID='.$EmployeeID.'">
            <input type="hidden" name="ByName" value="1" />
            <input type="hidden" name="EmployeeID" value="'.$EmployeeID.'" />
            <select name="CategoryName"">';
        
        
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
            $select = $db->select();
            $select->from('categories')
                   ->order('categories.Name')
                   ->group('categories.Name');
            $statement = $db->query($select);
            $categories = $statement->fetchAll();
            
            if(count($categories) > 0)
            {
                $catSelectOptions = '';
                foreach($categories as $categoryArray)
                {
                    $catSelectOptions .= '<option value="'.$categoryArray['Name'].'">'.$categoryArray['Name'].'</option>';
                }
            }
            $html .= $catSelectOptions . '
            </select>
            <input type="submit" value="ADD" />
            </form>
            
            <p><strong>Remove from ALL categories by name:</strong></p>
            <form method="post" action="'.$this->_baseURI .'/admin/assign-employee-to-categories/?EmployeeID='.$EmployeeID.'">
            <input type="hidden" name="RemoveByName" value="1" />
            <input type="hidden" name="EmployeeID" value="'.$EmployeeID.'" />
            <select name="CategoryName"">' 
                . $catSelectOptions . '
            </select>
            <input type="submit" value="REMOVE" />
            </form>
            

            </div>
            ';       

        // All
        $html .= '
            <div class="section">
            <p><strong>Show All Categories (Takes a long time):</strong></p>
            <form method="get" action="'.$this->_baseURI .'/admin/assign-employee-to-categories-list/">
            <input type="hidden" name="EmployeeID" value="'.$EmployeeID.'" />
            <input type="submit" value="View List" />
            </form>
            </div>
            ';              
        
        $html .= '</div>';
        $this->view->content = $html;
    }
    
    public function assignEmployeeToCategoryName($EmployeeID,$CategoryName)
    {
        $catMapper = new Application_Model_CategoryMapper;
        $catWhere = 'Name = \'' . $CategoryName . '\'';
        $cats = $catMapper->fetchAll($catWhere);
        if(count($cats) == 0)
        {
            return 'No categories were found with the name <strong>'.$CategoryName.'.</strong>';
        }
        
        $excMapper = new Application_Model_EmployeeCategoriesMapper();
        $exc = new Application_Model_EmployeeCategories();
        
        foreach($cats as $cat)
        {
            // Only if the association doesn't already exist
            $excWhere = 'CategoryID = ' . $cat->getCategoryID() . ' AND EmployeeID = ' . $EmployeeID;
            $excs = $excMapper->fetchAll($excWhere);
            if(count($excs)==0)
            {
                $exc->setCategoryID($cat->getCategoryID())->setEmployeeID($EmployeeID);
                $excMapper->save($exc);
            }
        }
        
        return '
            <strong>'.$this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID)->getName().'</strong>
                is now assigned to ALL categories named <strong>'.$CategoryName.'</strong>
                for every client.';
    }    
    
    public function unassignEmployeeFromCategoryName($EmployeeID,$CategoryName)
    {
        $CategoryName = $this->getRequest()->getParam('CategoryName');
        $catMapper = new Application_Model_CategoryMapper;
        $catWhere = 'Name = \'' . $CategoryName . '\'';
        $cats = $catMapper->fetchAll($catWhere);
        
        if(count($cats) == 0)
        {
            return 'No categories were found with the name <strong>'.$CategoryName.'.</strong>';
        }
        
        $excMapper = new Application_Model_EmployeeCategoriesMapper();
        foreach($cats as $cat)
        {
            $cxtMapper = new Application_Model_CategoryTasksMapper();
            $where = 'CategoryID = ' . $cat->getCategoryID();
            $cxts = $cxtMapper->fetchAll($where);
            if(count($cxts) > 0)
            {
                foreach($cxts as $cxt)
                {
                    // If hours assigned, don't delete association
                    $tsMapper = new Application_Model_TimesheetMapper();
                    $tsWhere = 'CategoriesXTasksID = ' . $cxt->getCategoriesXTasksID() . ' AND EmployeeID = ' . $EmployeeID;
                    $tss = $tsMapper->fetchAll($tsWhere);
                    if(count($tss) > 0)
                    {
                        $hasHours=true;
                        break;
                    } else {
                        $hasHours=false;
                    }
                }
            } else {
                // Code coverage cannot be guaranteed because this condition is
                // only met when someone has created a cateogry but not assigned
                // tasks to it yet, and this will generally not be the case.
                // @codeCoverageIgnoreStart
                $hasHours=false;
                // @codeCoverageIgnoreEnd
            }
            
            if(!$hasHours)
            {
                $excMapper->getDbTable()->delete('EmployeeID = ' . $EmployeeID . ' AND CategoryID = ' . $cat->getCategoryID() );
            }
        }
        return '
            <strong>'.$this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID)->getName().'</strong>
                was unassigned from ALL categories named <strong>'.$CategoryName.'</strong>
                for every client.';

    }
    
    public function assignEmployeeToCategoriesListAction()
    {
        $EmployeeID = $this->getRequest()->getParam('EmployeeID');
        if(!$EmployeeID) 
        {
            return $this->_redirect ('/admin/list-employees');
            exit();
        }
        
        $this->view->title = 'Assign Employee to Categories';
        $this->view->introduction = '<p><strong>For Employee: ' . $this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID)->getName().'</strong></p><p>&nbsp;</p>';
        $this->view->EmployeeID = $EmployeeID;
        
        $html = '';
        
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                 ->from(
                        array(
                            'c' => 'categories'
                        ),
                        array(
                            'CatID'       => 'c.CategoryID',
                            'ClientID'    => 'c.ClientID', 
                            'Name'        => 'c.Name',
                            'DateCreated' => 'c.DateCreated'
                        )
                    )
                ->joinLeft(
                        array(
                            'cl' => 'clients'
                        ),
                        'c.ClientID = cl.ClientID',
                        array(
                            'ClientName' => 'cl.Name'
                        )
                    )
                ->joinLeft(
                        array(
                            'exc' => 'employeesxcategories'
                        ),
                        'c.CategoryID = exc.CategoryID AND exc.EmployeeID = ' . $EmployeeID,
                        array(
                            'EmployeeID' => 'exc.EmployeeID'
                        )
                    )
                ->joinLeft(
                        array(
                            'cxt' => 'categoriesxtasks'
                        ),
                        'exc.CategoryID = cxt.CategoryID',
                        array(
                            'CXTID' => 'cxt.CategoriesXTasksID'
                        )
                    )
                ->joinLeft(
                        array(
                            'ts' => 'timesheet'
                        ),
                        'cxt.CategoriesXTasksID = ts.CategoriesXTasksID AND ts.EmployeeID = ' . $EmployeeID,
                        array(
                            'TimesheetID' => 'ts.TimeSheetID'
                        )
                    )             
                ->order(
                    'c.Name'
                    );
        
        $statement = $db->query($select);
        $results = $statement->fetchAll();   
        if($results) {
            
            // Note: You'll need to understand the associations between the various objects
            // to make sense of what is going on here.
            //      client (1 to many) cateogry (many to many) task
            //      employee (many to many) category ( many to many) task
            //      timesheet (many to many) employe (many to many) category (many to many) task
            // 
            // The above query shows all categories, then joins lots of tables to get extra data we need.
            // For example, it joins the timesheet, category and task tables in order to see if the employee 
            // is already assigned to the category, and to see if he has already entered time into the timesheet
            // for tasks associated with that category.  However, because of the many to many relationship between 
            // categories and tasks, this returns multiple rows for a single category (one per task assigned to that 
            // category).  This is in order to see if hours have been logged.  We don't want to show each category
            // multiple times, so instead I first store each client category in an array.  As I loop through each row,
            // if it's already in the array, I'll still check to see if any hours were logged.  Otherwise I'll just
            // pass over it.  Then I loop through the array to create the HTML table.

            
            $listArray = array();
            foreach($results as $id => $row)
            {
                if(isset($listArray[$row['CatID']]))
                {
                    if($listArray[$row['CatID']]['hasHours']==true)
                    {
                        continue;
                    } else {
                        if( !$row['TimesheetID'] )
                        {
                            $listArray[$row['CatID']]['hasHours']=false;
                        } else {
                            $listArray[$row['CatID']]['hasHours']=true;
                        }
                        continue;
                    }
                }
                if( !$row['EmployeeID'] )
                {
                    $listArray[$row['CatID']]['alreadyAssigned']=false;
                } else {
                    $listArray[$row['CatID']]['alreadyAssigned']=true;
                }
                if( !$row['TimesheetID'] )
                {
                    $listArray[$row['CatID']]['hasHours']=false;
                } else {
                    $listArray[$row['CatID']]['hasHours']=true;
                }
                $listArray[$row['CatID']]['CatID'] = $row['CatID'];
                $listArray[$row['CatID']]['Name'] = $row['Name'];
                $listArray[$row['CatID']]['ClientName'] = $row['ClientName'];
                $listArray[$row['CatID']]['DateCreated'] = $row['DateCreated'];
            }

            
            
            $html .= '<div id="table-wrapper"><table class="administration-details-table">';
            $html .= '<tr class="first">
                        <td class="icon"></td>
                        <td class="icon"></td>
                        <td>Category</td>
                        <td>Client</td>
                        <td>Date Created</td>
                    </tr>';
            foreach($listArray as $id => $row)
            {
                
                $alreadyAssigned = $row['alreadyAssigned'];
                $hasHours = $row['hasHours'];
                $html .= '<tr>
                        <td class="icon">
                          <div class="addMe">
                                <a href="#" id="addMe'.$row['CatID'].'" class="assignMe'. ($alreadyAssigned ? ' active" alt="Assigned" title="Assigned"' : '" alt="Assign to category" title="Assign to category"') .'></a>
                          </div>
                        </td>
                        <td class="icon">';
                if($hasHours)
                {
                    $html .= '<div class="removeMeDisabled"> </div>';
                } else {
                    $html .= '<div class="removeMe">
                                <a href="#" id="removeMe'.$row['CatID'].'" class="unassignMe'. ($alreadyAssigned ? '" alt="Remove from category" title="Remove from category"' : ' active" alt="Not assigned" title="Not assigned"') .'></a>
                              </div>';
                }
                $html .= '
                        </td>                    
                        <td>'.$row['Name'].'</td>
                        <td>'.$row['ClientName'].'</td>
                        <td>'.$row['DateCreated'].'</td>
                    </tr>';                 
            }
            $html .= '</table></div>';            
        }
        $this->view->listTable = $html;
        return;
    }
    
    
    
    public function createEmployeeAction()
    {
        $errors = array();
        $messages = array();
        
        if(!$this->_ei->canCreateUsers())
        {
            return $this->_redirect('/admin/');
            exit();
        }
        $this->view->title = 'Register an Employee';
        
        $action = $this->_baseURI . '/admin/create-employee';
        $mode = 'admin-create-employee';
        $form = $this->view->createForm = $this->_helper->Form->getForm($action, $mode);

        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $vals = $form->getValues();
                $empMapper = new Application_Model_EmployeeMapper();
                $emp = new Application_Model_Employee();
                $emp->setName($vals['name'])
                    ->setDepartmentID($vals['department'])
                    ->setUsername($vals['username'])
                    ->setPassword($this->_helper->Password->hashItOut(
                        $vals['username'], 
                        $vals['password']))
                    ->setActive($vals['isactive'])
                    ->setIsReporter($vals['isreporter']);
                if($this->_ei->canCreateAdminUsers())
                {
                    $emp->setIsAdmin($vals['isadmin']);
                } else {
                    $emp->setIsAdmin(0);
                }
                if(isset($_POST['employeeid']) && is_numeric($_POST['employeeid']))
                {
                    $emp->setEmployeeID($_POST['employeeid']);
                }                
                $empMapper->save($emp);
                $form->reset();
                $form->password->setValue($this->_helper->Password->generateNew('asdf'));
                $this->view->createForm = $form;
                $messages[] = 'The user was successfully created with the following credentials:<br><br>Username:' . $vals['username'] . '<br>Password:' . $vals['password'] . '</p>';
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        } else {
            $this->view->messages = array();
        }
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
    }

    public function editEmployeeAction()
    {
        $errors = array();
        
        $this->view->title = 'Edit an Employee';
        
        $EmployeeID = $this->getRequest()->getParam('id');
        if(!$EmployeeID)
        {
            return $this->_redirect('/admin/list-employees/');
            exit();
        } else {
            
            $employee = $this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID);
            if($employee->isAdmin() && !$this->_ei->canCreateAdminUsers())
            {
                $errors[] = 'Sorry; you cannot edit this account with your credentials.  <a href="'.$this->_baseURI . '/admin/list-employees">(Back)</a>';
            } else {
                $action = $this->_baseURI . '/admin/edit-employee/?id=' . $EmployeeID;
                $mode = 'admin-edit-employee';
                $form = $this->view->editForm = $this->_helper->Form->getForm($action, $mode, $EmployeeID);
                if(!$form)
                {
                    $errors[] = '
                        Sorry; there was an error retrieving this employee\'s information.  <a href="'.$this->_baseURI . '/admin/list-employees">(Back)</a>';
                }
            }
        }
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $employee = $this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID);
                
                $vals = $form->getValues();
                $empMapper = new Application_Model_EmployeeMapper();
                $emp = new Application_Model_Employee();
                
                $emp->setActive($vals['isactive'])
                    ->setDepartmentID($vals['department'])
                    ->setEmployeeID($EmployeeID);
                
                if($this->_ei->canCreateAdminUsers())
                {
                    $emp->setIsAdmin($vals['isadmin']);
                } else {
                    $emp->setIsAdmin($employee->getIsAdmin());
                }                
                    $emp->setIsReporter($vals['isreporter'])
                    ->setName($vals['name'])
                    ->setPassword($employee->getPassword())
                    ->setUsername($employee->getUsername());
                $empMapper->save($emp);
                return $this->_redirect('/admin/list-employees');
                exit();

            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        } else {
            $this->view->messages = array();
        }    
        
        if(count($errors) > 0) $this->view->errors = $errors;
    }
    
    public function resetEmployeePasswordAction()
    {
        $errors = array();
        
        if(!$this->_ei->canCreateUsers())
        {
            return $this->_redirect('/admin/');
            exit();
        }
        $this->view->title = 'Reset Employee Password';
        $EmployeeID = $this->getRequest()->getParam('eid');
        $ConfirmCode = $this->getRequest()->getParam('confirmed');
        
        if(!$EmployeeID)
        {
            return $this->_redirect('/admin/list-employees');
            exit();
        }
        
        // Display confirmation screen
        if(!$ConfirmCode)
        {
            $employee = $this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID);
            if($employee->isAdmin() && !$this->_ei->canCreateAdminUsers())
            {
                $errors[] = 'Sorry; you cannot edit this account with your credentials.  <a href="'.$this->_baseURI . '/admin/list-employees">(Back)</a>';
            } else {
                $confirmationText = '<p><strong>Please Confirm</strong></p>'
                    .'<p>Are you sure you want to reset the password '
                    . 'for the following user?</p><p>&nbsp;</p>' 
                    . '<p>Employee: ' . $employee->getName() . '<br>'
                    . 'Department: ' . $this->_helper->TimeSheet->getDepartmentInfoByID($employee->getDepartmentID())->getName()
                    . '</p><p>&nbsp;</p>'
                    . '<p><a href="'.$this->_baseURI.'/admin/reset-employee-password/?eid=' . $EmployeeID.'&confirmed=1">Confirm</a>'
                    . '&nbsp;&nbsp;&nbsp;<a href="'.$this->_baseURI.'/admin/list-employees">Cancel</a></p>';

                $this->view->content = $confirmationText;                
            }
            
        } else {
            
            $employee = $this->_helper->TimeSheet->getEmployeeInfoById($EmployeeID);
            
            $oldPasswordHashed = $employee->getPassword();
            $newPasswordUnhashed = $this->_helper->Password->generateNew($employee->getName());
            $newPasswordHashed = $this->_helper->Password->hashItOut($employee->getUsername(), $newPasswordUnhashed);
            
            $employeeMapper = new Application_Model_EmployeeMapper();
            $employeeObject = new Application_Model_Employee();
            $employeeObject->setActive($employee->getActive())
                           ->setIsReporter($employee->getIsReporter())
                           ->setDepartmentID($employee->getDepartmentID())
                           ->setEmployeeID($EmployeeID)
                           ->setIsAdmin($employee->isAdmin())
                           ->setName($employee->getName())
                           ->setPassword($newPasswordHashed)
                           ->setUsername($employee->getUsername());
            $employeeMapper->save($employeeObject);
            
            $confirmationText = '<p><strong>Password Successfully Reset</strong></p>'
                .'<p>&nbsp;</p><p>'.$employee->getName() . '\'s password was reset to:</p>'
                .'<p>&nbsp;</p><p><strong>'.$newPasswordUnhashed.'</strong></p>';
            $this->view->content = $confirmationText;
        }
        
        if(count($errors) > 0) $this->view->errors = $errors;
    }

    
    
    
    
    /**************************************************************************
     * 
     * Department Actions
     * 
     **************************************************************************/

    public function listDepartmentsAction()
    {
        $errors = array();
        $messages = array();
        
        /* If attempting to remove a department
         * 
         *      -- Not allowed if Employees belong to it
         *      -- Not allowed if company policy requires Admin users be in a 
         *         specific department in order to create other Admin users, and they
         *         are attempting to delete that department
         */
        $DepartmentID = $this->getRequest()->getParam('dptremove');
        if($DepartmentID)
        {
            $employeeMapper = new Application_Model_EmployeeMapper();
            $employeeWhere = 'DepartmentID = ' . $DepartmentID;
            $orderBy = 'Name';
            $employees = $employeeMapper->fetchAll($employeeWhere, $orderBy);
            if(count($employees) > 0)
            {
                $errors[] = '<p>Note: you cannot remove a department while employees are still assigned to it.</p>';
                $errors[] = '<p>The following users are still assigned to the <strong> ' . $this->_helper->TimeSheet->getDepartmentInfoByID($DepartmentID)->getName() . '</strong> department: <br /><br />';
                foreach($employees as $employee)
                {
                    $errors[] = '<a href="'.$this->_baseURI . '/admin/edit-employee/?id='.$employee->getEmployeeID();
                    $errors[] = '">'.$employee->getName();
                    $errors[] = '</a><br />';
                }
            } else {
                if($this->_ei->getCreateAdminUsers_MustBeInSpecificDepartment() 
                    && $this->_ei->getCreateAdminUsers_MustBeInSpecificDepartmentID() == $DepartmentID)
                {
                    $errors[] = '<p>Note: You cannot remove this department because of company policy</p><p>&nbsp;</p>';
                } else {
                    $dptMapper = new Application_Model_DepartmentMapper();
                    $dptMapper->getDbTable()->delete('DepartmentID = ' . $DepartmentID);
                    $messages[] = '<p>Department deleted successfully.</p>';
                }
            }
            if(count($errors) > 0) $this->view->errors = $errors;
            if(count($messages) > 0) $this->view->messages = $messages;
        }
        
        
        $this->view->title = 'List Departments';
        $html = '<a class="createNew" href="'.$this->_baseURI . '/admin/create-department'.'">Create New</a>';
        $dptMapper = new Application_Model_DepartmentMapper();
        $orderBy = 'Name';
        $dpts = $dptMapper->fetchAll(null,$orderBy);
        if(count($dpts) > 0)
        {
            $html .= '<div id="table-wrapper"><table class="administration-details-table">';
            $html .= '<tr class="first">
                        <td class="icon"></td>
                        <td class="icon"></td>
                        <td>Department</td>
                    </tr>';                
            foreach($dpts as $dpt)
            {
                $html .= '<tr id="dept'.$dpt->getDepartmentID().'">
                        <td class="icon">
                          <a href="'. $this->_baseURI.'/admin/edit-department/?id='.$dpt->getDepartmentID().'">
                            <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                          </a>
                        </td>
                        <td class="icon">
                          <a class="removeMe" href="'.$this->_baseURI.'/admin/list-departments/?dptremove='.$dpt->getDepartmentID().'">
                            <img src="'.$this->_baseURI.'/images/delete.png'.'" alt="Delete" title="Delete" />
                          </a>
                        </td>                    
                        <td>'.$dpt->getName().'</td>
                    </tr>'; 
            }
            $html .= '</table></div>';
        }
        $this->view->listTable = $html;
    }
    
    public function createDepartmentAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Create a Department';
        $action = $this->_baseURI . '/admin/create-department';
        $mode = 'admin-create-department';
        $form = $this->view->createForm = $this->_helper->Form->getForm($action, $mode);
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $vals = $form->getValues();
                $dptMapper = new Application_Model_DepartmentMapper();
                $dpt = new Application_Model_Department();
                $dpt->setName($vals['departmentName']);
                if(isset($_POST['departmentID']) && is_numeric($_POST['departmentID']))
                {
                    $dpt->setDepartmentID ($_POST['departmentID']);
                }
                $dptMapper->save($dpt);
                return $this->_redirect('/admin/list-departments');
                exit();
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }
        
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
        
    }
    

    public function editDepartmentAction()
    {
        $errors = array();
        
        $this->view->title = 'Edit a Department';
        
        $DepartmentID = $this->getRequest()->getParam('id');
        if(!$DepartmentID)
        {
            return $this->_redirect('/admin/list-departments');
            exit();
        } else {

           /* 
            * The below "if" statement in English:
            * 
            * 1. If Admin users must be in a certain department in order to create / edit other Admin users
            * 2. If the current user is trying to edit that department
            * 3. If the current user doesn't belong to that department
            *      == Don't allow it!
            * 
            */
            if(    $this->_ei->getCreateAdminUsers_MustBeInSpecificDepartment()
                && $this->_ei->getCreateAdminUsers_MustBeInSpecificDepartmentID() == $DepartmentID
                && $this->_ei->getDepartmentID() != $DepartmentID)
            {
                $errors[] = 'You do not have sufficient permissions to modify this department.
                    <a href="'.$this->_baseURI . '/admin/list-departments">(Back)</a>';
            } else {
                $action = $this->_baseURI . '/admin/edit-department/?id=' . $DepartmentID;
                $mode = 'admin-edit-department';
                $form = $this->view->editForm = $this->_helper->Form->getForm($action, $mode, $DepartmentID);
                
                if($this->getRequest()->isPost())
                {
                    if($form->isValid($_POST))
                    {
                        $vals = $form->getValues();
                        $dptMapper = new Application_Model_DepartmentMapper();
                        $dpt = new Application_Model_Department();
                        $dpt->setDepartmentID($DepartmentID);
                        $dpt->setName($vals['departmentName']);
                        $dptMapper->save($dpt);
                        return $this->_redirect('/admin/list-departments');
                        exit();
                    } else {
                        $errors = $this->_helper->Form->getCustomErrors($form);
                    }
                }                  
            }
        }    
        
        if(count($errors) > 0) $this->view->errors = $errors;
    }   


    
    
    
    /**************************************************************************
     * 
     * Clients Actions
     * 
     **************************************************************************/

    public function listClientsAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'List Clients';
        
        $ClientID = $this->getRequest()->getParam('clientremove');
        if($ClientID)
        {
            
            $categoryMapper = new Application_Model_CategoryMapper();
            $where = 'ClientID = ' . $ClientID;
            $orderBy = 'Name';
            $categories = $categoryMapper->fetchAll($where, $orderBy);
            if(count($categories) > 0)
            {
                $errors[] = '
                    <p>You cannot remove this client because there are categories 
                    assigned to it.</p><p>&nbsp;</p><p>To really remove this client,
                    you must click on each category below and assign it to another 
                    client:<br /><br />';

                foreach($categories as $category)
                {
                    $link = $this->_baseURI . '/admin/edit-category/?id='.$category->getCategoryID();
                    $errors[] = '&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$link.'">'.$category->getName() . '</a><br>';
                }

            } else {
                $taskMapper = new Application_Model_ClientMapper();
                $taskMapper->getDbTable()->delete('ClientID = ' . $ClientID);
                $messages[] = '<p>Client deleted successfully.</p>';
            }
            
            if(count($errors) > 0) $this->view->errors = $errors;
            if(count($messages) > 0) $this->view->messages = $messages;
        }
        
        
        $html = '<a class="createNew" href="'.$this->_baseURI . '/admin/create-client'.'">Create New</a>';
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                     ->from(
                            array(
                                'clients'
                            ),
                            array(
                                'ClientID',
                                'Name',
                                'City', 
                                'State'
                            )
                        )
                    ->order(
                        'Name'
                        );
        $statement = $db->query($select);
        $clients = $statement->fetchAll();  
        if(count($clients) > 0)
        {
            $html .= '<div id="table-wrapper"><table class="administration-details-table">';
            $html .= '<tr class="first">
                        <td class="icon"></td>
                        <td class="icon"></td>
                        <td>Name</td>
                        <td>City</td>
                        <td>State</td>
                    </tr>';                
            foreach($clients as $client)
            {
                $html .= '<tr>
                        <td class="icon">
                          <a href="'. $this->_baseURI.'/admin/edit-client/?id='.$client['ClientID'].'">
                            <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                          </a>
                        </td>
                        <td class="icon">
                          <a href="'.$this->_baseURI.'/admin/list-clients/?clientremove='.$client['ClientID'].'">
                            <img src="'.$this->_baseURI.'/images/delete.png'.'" alt="Delete" title="Delete" />
                          </a>
                        </td>
                        <td>'.$client['Name'].'</td>
                        <td>'.$client['City'].'</td>
                        <td>'.$client['State'].'</td>
                    </tr>'; 
            }
            $html .= '</table></div>';            
        }

        $this->view->listTable = $html;
    }
    
    public function createClientAction()
    {
        $this->view->title = 'Create a Client';
        
        $action = $this->_baseURI . '/admin/create-client';
        $mode = 'admin-create-client';
        $form = $this->view->createForm = $this->_helper->Form->getForm($action, $mode);
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $vals = $form->getValues();
                $clientMapper = new Application_Model_ClientMapper();
                $client = new Application_Model_Client();
                $client->setName($vals['clientName'])
                    ->setCity($vals['clientCity'])
                    ->setState($vals['clientState']);
                if(isset($_POST['clientID']) && is_numeric($_POST['clientID']))
                {
                    $client->setClientID($_POST['clientID']);
                }
                
                $clientMapper->save($client);
                
                return $this->_redirect('/admin/list-clients');
                exit();

            } else {
                $this->view->errors = $this->_helper->Form->getCustomErrors($form);
            }
        } else {
            $this->view->messages = array();
        }

    }
    
    public function editClientAction()
    {
        $errors = array();
        
        $this->view->title = 'Edit a Client';
        
        $ClientID = $this->getRequest()->getParam('id');
        if(!$ClientID)
        {
            return $this->_redirect('/admin/list-clients/');
            exit();
        } else {
            $action = $this->_baseURI . '/admin/edit-client/?id=' . $ClientID;
            $mode = 'admin-edit-client';
            $form = $this->view->editForm = $this->_helper->Form->getForm($action, $mode, $ClientID);
        }
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {

                $vals = $form->getValues();
                $clientMapper = new Application_Model_ClientMapper();
                $client = new Application_Model_Client();
                $client->setClientID($ClientID)
                       ->setName($vals['clientName'])
                       ->setCity($vals['clientCity'])
                       ->setState($vals['clientState']);

                $clientMapper->save($client);
                return $this->_redirect('/admin/list-clients');
                exit();

            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }
        
        if(count($errors) > 0) $this->view->errors = $errors;
    }


    
    
    /**************************************************************************
     * 
     * Category Actions
     * 
     **************************************************************************/
    
    public function listCategoriesAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'List Categories';
        
        // If they want to delete it
        $CategoryID = $this->getRequest()->getParam('categoryremove');
        if($CategoryID)
        {
            $cxtMapper = new Application_Model_CategoryTasksMapper();
            $where = 'CategoryID = ' . $CategoryID;
            $cxts = $cxtMapper->fetchAll($where);
            if(count($cxts) > 0)
            {
                $errors[] = '<p>Note: You cannot remove this category because there are 
                    tasks assigned to it.  To remove this category, you must first
                    click on each task below and unassign it from this category:</p>
                    <p>&nbsp;</p>';

                $cxtArray = array();
                foreach($cxts as $cxt)
                {
                    $cxtArray[] = array(
                        'Name' => $this->_helper->TimeSheet->getTaskInfoById($cxt->getTaskID())->getName(),
                        'TaskID' => $cxt->getTaskID());
                }
                array_multisort(array_values($cxtArray), array_keys($cxtArray), $cxtArray);
                foreach($cxtArray as $key => $val)
                {
                    $link = $this->_baseURI . '/admin/assign-task/?taskID='.$val['TaskID'];
                    $errors[] = '&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$link.'">'.$val['Name'] . '</a><br>';
                }
            } else {
                $taskMapper = new Application_Model_CategoryMapper();
                $taskMapper->getDbTable()->delete('CategoryID = ' . $CategoryID);
                $messages[] = '<p>Category deleted successfully.</p>';
            }
        }

        
        $html = '<a class="createNew" href="'.$this->_baseURI . '/admin/create-category'.'">Create New</a>';
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                     ->from(
                            array(
                                'c' => 'categories'
                            ),
                            array(
                                'c.Name',
                                'c.CategoryID',
                                'c.DateCreated'
                            )
                        )
                    ->join(array('c1' => 'clients'), 'c.ClientID = c1.ClientID', array('ClientName' => 'c1.Name'))
                    ->order('c.Name');
        
        
        $statement = $db->query($select);
        $categories = $statement->fetchAll();   

        if(count($categories) > 0)
        {
            $html .= '<div id="table-wrapper"><table class="category-table">';
            $html .= '<tr class="first">
                        <td id="col1"></td>
                        <td id="col2"></td>
                        <td id="col3"></td>
                        <td>Category</td>
                        <td>Client</td>
                        <td>Date Created</td>
                    </tr>';                
            foreach($categories as $category)
            {
                $html .= '<tr>
                        <td id="col1">
                          <a href="'. $this->_baseURI.'/admin/edit-category/?id='.$category['CategoryID'].'">
                            <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                          </a>
                        </td>
                        <td id="col2">
                          <a href="'. $this->_baseURI.'/admin/assign-category-employees/?id='.$category['CategoryID'].'">
                            <img src="'.$this->_baseURI.'/images/users.png'.'" alt="Assign Employees" title="Assign Employees" />
                          </a>
                        </td>                    
                        <td id="col3">
                          <a href="'. $this->_baseURI .'/admin/list-categories/?categoryremove='.$category['CategoryID'].'">
                            <img src="'.$this->_baseURI.'/images/delete.png'.'" alt="Delete" title="Delete" />
                          </a>
                        </td>
                        <td>'.$category['Name'].'</td>
                        <td>'.$category['ClientName'].'</td>
                        <td>'.$category['DateCreated'].'</td>
                    </tr>'; 
            }
            $html .= '</table></div>';
        }
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
        $this->view->listTable = $html;
    }
    
    
    public function createCategoryAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Create a Category';
        
        $action = $this->_baseURI . '/admin/create-category';
        $mode = 'admin-create-category';
        $form = $this->view->createForm = $this->_helper->Form->getForm($action, $mode);

        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $vals = $form->getValues();

                $categoryMapper = new Application_Model_CategoryMapper();
                
                $where = 'ClientID = ' . $vals['categoryForClient'] . ' AND 	Name = \''.$vals['categoryName'].'\'';
                $categoriesExist = $categoryMapper->fetchAll($where);
                if($categoriesExist && count($categoriesExist) > 0)
                {
                    $errors[] = 'A category with the name "'.$vals['categoryName'].'" already
                         exists for client <strong>'.$this->_helper->TimeSheet->getClientInfoById($vals['categoryForClient'])->getName() .'</strong>';
                } else {
                    $category = new Application_Model_Category();
                    $category->setClientID($vals['categoryForClient'])
                            ->setName($vals['categoryName'])
                            ->setDateCreated(date('Y-m-d H:i:s'));
                if(isset($_POST['categoryID']) && is_numeric($_POST['categoryID']))
                {   
                    $category->setCategoryID($_POST['categoryID']);
                }
                
                    $categoryMapper->save($category);                    
                    $messages[] = 'The category "'.$vals['categoryName'].'" was created successfully.';
                }
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }
        
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
        
    }

    public function createCategoryForAllClientsAction()
    {
        
        $this->view->title = 'Create a Category for ALL Clients';
        
        $action = $this->_baseURI . '/admin/create-category-for-all-clients';
        $mode = 'admin-create-category-for-all-clients';
        $form = $this->view->createForm = $this->_helper->Form->getForm($action, $mode);
        
        $categoryAlreadyExistsForClient = array();
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $vals = $form->getValues();

                $clientMapper = new Application_Model_ClientMapper();
                $clients = $clientMapper->fetchAll();
                
                foreach($clients as $client)
                {

                    // See if category already exists for this client
                    $categoryMapper = new Application_Model_CategoryMapper();
                    $where = 'ClientID = ' . $client->getClientID() . ' AND Name = \''. $vals['categoryName']. '\'';
                    $cats = $categoryMapper->fetchAll($where);
                    if($cats && count($cats) > 0)
                    {
                        // do nothing, category already exists for client.  
                        // notify user?
                        // go to next client
                        $categoryAlreadyExistsForClient[] = $client->getName();
                        continue; 
                    }
                    
                    $category = new Application_Model_Category();
                    $category->setClientID($client->getClientID())
                            ->setName($vals['categoryName'])
                            ->setDateCreated(date('Y-m-d H:i:s'));
                    $categoryMapper->save($category);
                }
                
                $this->view->messages = array('<p>Category "<i>' . $vals['categoryName'] . '</i>" was successfully created and assigned to all clients.</p>');
                
                if(count($categoryAlreadyExistsForClient) > 0)
                {
                    $error = '<p>&nbsp;</p><p><strong>Please note: This category name already '
                        . ' existed for the following client(s), and a duplicate was NOT created:</strong>: </p><p>';
                    
                    foreach($categoryAlreadyExistsForClient as $catexistclient)
                    {
                        $error .= $catexistclient . '<br>';
                    }
                    $error .= '</p>';
                    
                    $this->view->messages[] = $error;
                }


            } else {
                $this->view->messages = $this->_helper->Form->getCustomErrors($form);
            }
        } else {
            $this->view->messages = array();
        }
    }
    
    public function editCategoryAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Edit a Category';
        
        $CategoryID = $this->getRequest()->getParam('id');
        
        if(!$CategoryID)
        {
            return $this->_redirect('/admin/list-categories/');
            exit();
        } else {
            $action = $this->_baseURI . '/admin/edit-category/?id=' . $CategoryID;
            $mode = 'admin-edit-category';
            $form = $this->view->editForm = $this->_helper->Form->getForm($action, $mode, $CategoryID);
        }
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                
                $vals = $form->getValues();

                // If category details changed:
                $currentCategoryInfo = $this->_helper->TimeSheet->getCategoryInfoByID($CategoryID);
                if($vals['categoryForClient'] != $currentCategoryInfo->getClientID()
                    || $vals['categoryName'] != $currentCategoryInfo->getName()) {
                    
                    // Make sure client doesn't already have a category by this name:
                    $categoryMapper = new Application_Model_CategoryMapper();
                    $where = 'ClientID = ' . $vals['categoryForClient'] . ' AND Name = \''. $vals['categoryName'] . '\'';
                    $categoryExists = $categoryMapper->fetchAll($where);
                    if($categoryExists && count($categoryExists) > 0)
                    {
                        $errors[] = 'A category with the name "'.$vals['categoryName'].'" already
                         exists for client <strong>'.$this->_helper->TimeSheet->getClientInfoById($vals['categoryForClient'])->getName() .'</strong>';
                    } else {
                        $category = new Application_Model_Category();
                        $category->setCategoryID($CategoryID)
                                ->setClientID($vals['categoryForClient'])
                                ->setName($vals['categoryName'])
                                ->setDateCreated(date('Y-m-d H:i:s'));
                        $categoryMapper->save($category);
                        return $this->_redirect('/admin/list-categories');
                        exit();
                    }
                } else {
                    return $this->_redirect('/admin/list-categories');
                    exit();
                }
                    
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }        
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
    }    
    

    

    /**************************************************************************
     * 
     * Task Actions
     * 
     **************************************************************************/

    public function listTasksAction()
    {
        $errors = array();
        $messages = array();
        
        // If they want to delete the task
        $TaskID = $this->getRequest()->getParam('taskremove');
        if($TaskID)
        {
            
                $cxtMapper = new Application_Model_CategoryTasksMapper();
            $where = 'TaskID = ' . $TaskID;
            $cxts = $cxtMapper->fetchAll($where);
            if(count($cxts) > 0)
            {
                $errors[] = '<p>Note: You cannot remove this task because it is 
                    currently assigned to one or more categories.</p><p>&nbsp;</p> <p>To remove it, 
                    you must first make sure it\'s not assigned to any categories.  To
                    do this, <a href="'.$this->_baseURI . '/admin/assign-task/?taskID='.$TaskID.'">
                    Click Here</a> and unassign this task from all categories.</p>';
                $errors[] = '<p>The following categories have this task 
                    assigned to them:<br /><br />';

                $categoriesArray = array();

                foreach($cxts as $cxt)
                {
                    $name = $this->_helper->TimeSheet->getCategoryInfoByID($cxt->getCategoryID())->getName();
                    $client = $this->_helper->TimeSheet->getClientInfoByID(
                        $this->_helper->TimeSheet->getCategoryInfoByID($cxt->getCategoryID())->getClientID()
                        )->getName();

                    $line = '&nbsp;&nbsp;&nbsp;&nbsp;<strong>Category</strong>: ' . $name . ' (for Client: ' . $client . ')<br>';
                    if(!in_array($line, $categoriesArray))
                    {
                        $categoriesArray[] = $line;
                    }
                }
                foreach($categoriesArray as $actName)
                {
                    $errors[] = $actName;
                }

            } else {
                $taskMapper = new Application_Model_TaskMapper();
                $taskMapper->getDbTable()->delete('TaskID = ' . $TaskID);
                $messages[] = '<p>Task deleted successfully.</p>';
            }
        }
        
        $this->view->title = 'List Tasks';
        
        $html = '<a class="createNew" href="'.$this->_baseURI . '/admin/create-task'.'">Create New</a>';
        
        $taskMapper = new Application_Model_TaskMapper();
        $orderBy = 'Name';
        $tasks = $taskMapper->fetchAll(null, $orderBy);
        if(count($tasks) > 0)
        {
            $html .= '<div id="table-wrapper"><table class="task-table">';
            $html .= '<tr class="first">
                        <td id="col1"></td>
                        <td id="col2"></td>
                        <td id="col3"></td>
                        <td>Task</td>
                    </tr>';                
            foreach($tasks as $task)
            {
                $html .= '<tr id="task'.$task->getTaskID().'">
                        <td id="col1">
                          <a href="'. $this->_baseURI.'/admin/edit-task/?taskID='.$task->getTaskID().'">
                            <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                          </a>
                        </td>
                        <td id="col2">
                          <a href="'. $this->_baseURI.'/admin/assign-task/?taskID='.$task->getTaskID().'">
                            <img src="'.$this->_baseURI.'/images/users.png'.'" alt="Assign To Categories" title="Assign To Categories" />
                          </a>
                        </td>                    
                        <td id="col3">
                          <a class="removeMe" href="'.$this->_baseURI.'/admin/list-tasks/?taskremove='.$task->getTaskID().'">
                            <img src="'.$this->_baseURI.'/images/delete.png'.'" alt="Delete" title="Delete" />
                          </a>
                        </td>                    
                        <td>'.$task->getName().'</td>
                    </tr>'; 
            }
            $html .= '</table></div>';
        }
        $this->view->listTable = $html;
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
        
    }
    
    public function createTaskAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Create a Task';
        
        $action = $this->_baseURI . '/admin/create-task';
        $mode = 'admin-create-task';
        $form = $this->view->createForm = $this->_helper->Form->getForm($action, $mode);
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {

                $vals = $form->getValues();
                $taskMapper = new Application_Model_TaskMapper();
                $task = new Application_Model_Task();
                $task->setName($vals['taskName']);
                if(isset($_POST['taskID']) && is_numeric($_POST['taskID']))
                {   
                    $task->setTaskID($_POST['taskID']);
                }
                
                $taskMapper->save($task);
                return $this->_redirect('/admin/list-tasks');
                exit();
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
    }
    
    public function editTaskAction()
    {
        $errors = array();
        
        $this->view->title = 'Edit a Task';
        
        $TaskID = $this->getRequest()->getParam('taskID');
        if(!$TaskID)
        {
            return $this->_redirect('/admin/list-tasks/');
            exit();
        } else {
            $action = $this->_baseURI . '/admin/edit-task/?taskID=' . $TaskID;
            $mode = 'admin-edit-task';
            $form = $this->view->editForm = $this->_helper->Form->getForm($action, $mode, $TaskID);
        }    
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {

                $vals = $form->getValues();
                $taskMapper = new Application_Model_TaskMapper();
                $task = new Application_Model_Task();
                $task->setTaskID($TaskID)
                     ->setName($vals['taskName']);
                $taskMapper->save($task);
                return $this->_redirect('/admin/list-tasks');
                exit();
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }   
        if(count($errors) > 0) $this->view->errors = $errors;
    }    
    

    /**************************************************************************
     * 
     * Join Table Assignment Actions
     * 
     **************************************************************************/
    
    // Assigning / Removing Employess to / from Categories
    public function assignCategoryEmployeesAction()
    {
        
        $this->view->title = 'Assign Employees to Category';
        $this->view->CategoryID = $CategoryID = $this->getRequest()->getParam('id');
        $this->view->baseURI = $this->_baseURI;
        if($CategoryID)
        {
            $category = $this->_helper->TimeSheet->getCategoryInfoByID($CategoryID);
            $client = $this->_helper->TimeSheet->getClientInfoByID($category->getClientID());
            
            $this->view->introduction = '
                <p>Client Name: <strong>'.$client->getName().'</strong></p>
                <p>Category Name: <strong>'.$category->getName().'</strong> </p>
                <p>&nbsp;</p>'."\r\n";
            
            $category = $this->_helper->TimeSheet->getCategoryInfoByID($CategoryID);

            $employeeMapper = new Application_Model_EmployeeMapper();
            $orderBy = 'Name';
            $employees = $employeeMapper->fetchAll(null, $orderBy);
            if(count($employees) > 0)
            {
                $html = '<div id="table-wrapper"><table class="employee-table">';
                $html .= '<tr style="background-color:#E5E4E8;padding:10px;">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td>Employee</td>
                            <td>Department</td>
                            <td>Is Admin</td>
                            <td>Status</td>
                        </tr>';                
                foreach($employees as $employee)
                {
                    $department = $this->_helper->TimeSheet->getDepartmentInfoByID($employee->getDepartmentID())->getName();

                    $isAdmin = ($employee->isAdmin()==1 ? 'Yes' : 'No');
                    $status =  ($employee->getActive()==1 ? 'Yes' : 'No');

                    // Are they already assigned to this category?
                    $excMapper = new Application_Model_EmployeeCategoriesMapper();
                    $excWhere = 'CategoryID = ' . $CategoryID . ' AND EmployeeID = ' . $employee->getEmployeeID();
                    $excs = $excMapper->fetchAll($excWhere);
                    if(count($excs) == 1)
                    {
                        $alreadyAssigned=true;
                    } else {
                        $alreadyAssigned=false;
                    }
                    $html .= '<tr>
                            <td id="col1">
                              <div class="addMe">
                                    <a href="#" id="addMe'.$employee->getEmployeeID().'" class="assignMe'. ($alreadyAssigned ? ' active" alt="Assigned" title="Assigned"' : '" alt="Assign to category" title="Assign to category"') .'></a>
                              </div>
                            </td>
                            <td id="col2">
                              <div class="removeMe">
                                    <a href="#" id="removeMe'.$employee->getEmployeeID().'" class="unassignMe'. ($alreadyAssigned ? '" alt="Remove from category" title="Remove from category"' : ' active" alt="Not assigned" title="Not assigned"') .'></a>
                              </div>
                            </td>                    
                            <td>'.$employee->getName().'</td>
                            <td>'.$department.'</td>
                            <td>'.$isAdmin.'</td>
                            <td>'.$status.'</td>
                        </tr>'; 
                }
                $html .= '</table></div>';


            }
            
            $this->view->listTable = $html;
        }
    }

        // Javascript helper for assignCategoryEmployeesAction "Add"
        public function excaddAction()
        {

            $this->_helper->layout->disableLayout();    //disable layout
            $this->_helper->viewRenderer->setNoRender(); //suppress auto-rendering
    
            $this->view->CategoryID = $CategoryID = $this->getRequest()->getParam('category');
            $this->view->EmployeeID = $EmployeeID = $this->getRequest()->getParam('employee');

            $excMapper = new Application_Model_EmployeeCategoriesMapper();
            $excWhere = 'CategoryID = '.$CategoryID.' AND EmployeeID = '.$EmployeeID; 
            $excs = $excMapper->fetchAll($excWhere);

            // If already assigned:
            if(count($excs) == 1)
            {
                return true;
            } else {

                $exc = new Application_Model_EmployeeCategories();
                $exc->setEmployeeID($EmployeeID)
                    ->setCategoryID($CategoryID);
                $excMapper->save($exc);         
            }

            $this->view->data = 'OK';
        }
    
        // Javascript helper for assignCategoryEmployeesAction "Remove"
        public function excremoveAction()
        {
            
            $this->_helper->layout->disableLayout();    //disable layout
            $this->_helper->viewRenderer->setNoRender(); //suppress auto-rendering
            
            $this->view->CategoryID = $CategoryID = $this->getRequest()->getParam('category');
            $this->view->EmployeeID = $EmployeeID = $this->getRequest()->getParam('employee');

            $excMapper = new Application_Model_EmployeeCategoriesMapper();
            $excMapper->getDbTable()->delete('CategoryID = ' . $CategoryID . ' AND EmployeeID = ' . $EmployeeID);

            $this->view->data = 'OK';
        }
    
    // Assigning a specified task to categories
    public function assignTaskAction()
    {
        
        $this->view->TaskID = $TaskID = $this->getRequest()->getParam('taskID');
            
        if(!$TaskID || !is_numeric($TaskID))
        {
            return $this->_redirect('/admin/list-tasks');
            exit();
        }

        $task = $this->_helper->TimeSheet->getTaskInfoById($TaskID);
        $this->view->title = 'Assign "'.$task->getName().'" Task to Categories';
        $this->view->introduction = '
            <p>This page enables you to assign the <strong>'.$task->getName().'</strong> 
                task to categories.  Below is a list of all categories, along with which client 
                each category belongs to.  You can assign the 
                <strong>'.$task->getName().'</strong> task to any of these categories by 
                clicking the green button.  Similarly, you can unassign it from any category 
                by clicking the red button.  <u>Note</u>: You cannot unassign a task from a 
                category if employees have already entered hours for that task.</p>
            <p>&nbsp;</p>
            <p>Task Name: <strong>'.$task->getName().'</strong></p>
            <p>&nbsp;</p>';

        
        // Assign task to category, only if not already assigned
        $CategoryID = $this->getRequest()->getParam('assignToProjID');
        if($CategoryID)
        {
            $cxtMapper = new Application_Model_CategoryTasksMapper();
            $cxtMapperWhere = 'CategoryID = ' . $CategoryID . ' AND TaskID = ' . $TaskID;
            $cxtExists = $cxtMapper->fetchAll($cxtMapperWhere);
            if(count($cxtExists) == 0) // not already assigned
            {
                $cxt = new Application_Model_CategoryTasks();
                $cxt->setActive(1)
                    ->setCategoryID($CategoryID)
                    ->setTaskID($TaskID);
                $cxtMapper->save($cxt);
                return $this->_redirect('/admin/assign-task/?taskID='.$TaskID);
                exit();
            } else {
                $errors[] = 'You cannot assign a task to the same category twice.';
            }
        }

        
        // Unassign task from category, only if already assigned, only if no timesheet entries exist
        $CategoryID = $this->getRequest()->getParam('unassignFromProjID');
        if($CategoryID)
        {
            $cxtMapper = new Application_Model_CategoryTasksMapper();
            $cxtMapperWhere = 'CategoryID = ' . $CategoryID . ' AND TaskID = ' . $TaskID;
            $cxtExists = $cxtMapper->fetchAll($cxtMapperWhere);
            if(count($cxtExists) > 0) // already assigned
            {
                $cxtID = $cxtExists[0]->getCategoriesXTasksID();
                $tsMapper = new Application_Model_TimesheetMapper();
                $tsWhere = 'CategoriesXTasksID = ' . $cxtID;
                $tsExists = $tsMapper->fetchAll($tsWhere);
                if(count($tsExists)==0) // No timesheet entries exist
                {
                    $cxtMapper->getDbTable()->delete('CategoryID = ' . $CategoryID . ' AND TaskID = ' . $TaskID);                    
                    return $this->_redirect('/admin/assign-task/?taskID='.$TaskID);
                    exit();
                } else {
                    $errors[] = 'You cannot unassign a task from a category if there have been hours logged by users for this category -> task association.';
                }
                
            } else {
                $errors[] = 'You cannot unassign a task from a category it is already not assigned to.  How did you get here???';
            }

            
        }




        // The code below might look ugly.  I'm joining multiple tables together
        // to get a list of all categories, determine whether the current $TaskID
        // has already been assigned to any category,
        // and if so, determine whether any hours have already been entered
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                     ->from(
                            array(
                                'c' => 'categories'
                            ),
                            array(
                                'CID' => 'c.CategoryID', 
                                'Name' => 'c.Name'
                            )
                        )
                    ->joinLeft(
                        array('cl'  => 'clients'),          
                        'c.ClientID = cl.ClientID', 
                        array('ClientName' => 'cl.Name'))
                    ->joinLeft(
                        array('cxt' => 'categoriesxtasks'), 
                        'c.CategoryID = cxt.CategoryID AND cxt.TaskID = ' . $TaskID, 
                        array('CXTID' => 'cxt.CategoriesXTasksID'))
                    ->joinLeft(array('t'   => 'tasks'),            
                        'cxt.TaskID = t.TaskID AND t.TaskID = ' . $TaskID, 
                        array('TaskID' => 't.TaskID'))
                    ->joinLeft(
                        array('ts'  => 'timesheet'),        
                        'cxt.CategoriesXTasksID = ts.CategoriesXTasksID AND cxt.TaskID = ' . $TaskID,
                        array('TSID' => 'ts.TimeSheetID'))
                    ->order(array('c.Name', 'cl.Name'));

        // Array to store the results
        $cats = array();

        $statement = $db->query($select);
        $categories = $statement->fetchAll();   
        if(count($categories) > 0)
        {
            $html = '<div id="table-wrapper"><table class="task-table">';
            $html .= '<tr style="background-color:#E5E4E8;padding:10px;">
                        <td id="col1"></td>
                        <td id="col2"></td>
                        <td>Category</td>
                        <td>For Client:</td>
                        <td>Employee Hours Assigned to this Category Task?</td>
                    </tr>';

            foreach($categories as $cat)
            {
                $cats[$cat['CID']]['Name'] = $cat['Name'];
                $cats[$cat['CID']]['ClientName'] = $cat['ClientName'];


                // Find if current task already assigned to this category
                if( !isset($cats[$cat['CID']]['AlreadyAssigned']))
                {
                    $cats[$cat['CID']]['AlreadyAssigned'] = false;
                }
                if($cats[$cat['CID']]['AlreadyAssigned'] == false)
                {
                    if(isset($cat['TaskID']) && !is_null($cat['TaskID']))
                    {
                        if($cat['TaskID'] == $TaskID)
                        {
                            $cats[$cat['CID']]['AlreadyAssigned'] = true; 
                        }
                    }
                }

                // Find if hours exist for a category x task:
                if( !isset($cats[$cat['CID']]['HasHours']))
                {
                    $cats[$cat['CID']]['HasHours'] = false;
                }
                if($cats[$cat['CID']]['HasHours'] == false)
                {
                    if(isset($cat['TSID']) && !is_null($cat['TSID']))
                    {
                        if($cat['TaskID'] == $TaskID)
                        {
                           $cats[$cat['CID']]['HasHours'] = true;
                           $cats[$cat['CID']]['CXTID'] = $cat['CXTID'];
                        }
                    }
                }

            }

            foreach($cats as $cid => $cinfo)
            {

                if($cinfo['AlreadyAssigned']==true)
                {
                    $alreadyAssigned=true;
                } else {
                    $alreadyAssigned=false;
                }

                if($cinfo['HasHours']==true)
                {
                    $hasHours=true;
                } else {
                    $hasHours=false;
                }

                if($hasHours=='Yes')
                {
                    $html .= '<tr>
                            <td id="col1">
                              <div class="addMe">
                                    <a href="#" class="assignMe active" alt="Assigned" title="Assigned"></a>
                              </div>
                            </td>
                            <td id="col2">
                              <div class="removeMeDisabled">

                              </div>
                            </td>
                            <td>'.$cinfo['Name'].'</td>
                            <td>'.$cinfo['ClientName'].'</td>
                            <td>
                                <a href="'.$this->_baseURI.'/admin/list-category-task-hours/?cxtID='.$cinfo['CXTID'].'">
                                Yes (Click to view employee hours for this category task)</a>
                            </td>
                        </tr>'; 
                } else {
                    $html .= '<tr>
                            <td id="col1">
                              <div class="addMe">';

                            if($alreadyAssigned)
                            {
                                $html .= '<a href="#" id="addMe'.$cid.'" class="assignMe active" alt="Assigned" title="Assigned"></a>';
                            } else {
                                $html .= '<a href="'.$this->_baseURI.'/admin/assign-task/?taskID='.$TaskID.'&assignToProjID='.$cid.'" id="addMe'.$cid.'" class="assignMe" alt="Assign task to this category" title="Assign task to this category"></a>';
                            }

                    $html .= '
                              </div>
                            </td>
                            <td id="col2">
                              <div class="removeMe">';

                            if($alreadyAssigned)
                            {
                                $html .= '<a href="'.$this->_baseURI.'/admin/assign-task/?taskID='.$TaskID.'&unassignFromProjID='.$cid.'" id="removeMe'.$cid.'" class="unassignMe" alt="Unassign task from this category" title="Unassign task from this category"></a>';
                            } else {
                                $html .= '<a href="#" id="removeMe'.$cid.'" class="unassignMe active" alt="Task not assigned to this category" title="Task not assigned to this category"></a>';
                            }

                    $html .= '
                              </div>
                            </td>
                            <td>'.$cinfo['Name'].'</td>
                            <td>'.$cinfo['ClientName'].'</td>
                            <td>No</td>
                        </tr>'; 
                }
            }
            $html .= '</table></div>';
        }

        $this->view->listTable = $html;
        if(count($errors) > 0) $this->view->errors = $errors;
    }
    
    
    public function assignTaskByCategoryNameAction()
    {
        
        $TaskID = $this->getRequest()->getParam('taskID');
        
            
        if($TaskID)
        {
            
            $task = $this->_helper->TimeSheet->getTaskInfoById($TaskID);
            $this->view->title = 'Assign "'.$task->getName().'" Task to ALL Categories By Name';
            $this->view->messages = array();
            
            $this->view->introduction = '
                <p>This page enables you to assign and unassign the <strong>'.$task->getName().'</strong> 
                    task from ALL categories that have the selected name from below.  BE CAREFUL WITH THIS.
                    Every client who has a category by the name below will have this task added to 
                    their category / removed from their category (Client -> Category -> Task).  There is one exception: If any employee
                    has entered any hours into a Client\'s Category\'s Task, that Client\'s Category\'s Task
                    cannot be unassigned from its Category.  All the others (with no hours) will be unassigned.</p>
                <p>&nbsp;</p>
                <p>Task Name: <strong>'.$task->getName().'</strong></p>
                <p>&nbsp;</p>';
            
            
            $CategoryName = urldecode($this->getRequest()->getParam('assignToCategoryName'));
            
            
            if($CategoryName)
            {
                
                // Go through categories with this name,
                //  for each category, do categorytasksmapper create cxt
                //  Geez.
                
                $CategoryMapper = new Application_Model_CategoryMapper();
                $where = 'Name = \''. $CategoryName . '\'';
                $categories = $CategoryMapper->fetchAll($where);
                if(count($categories) > 0)
                {
                    
                    foreach($categories as $category)
                    {
                        
                        $cxtMapper = new Application_Model_CategoryTasksMapper();
                        $where = 'CategoryID = ' . $category->getCategoryID() . ' AND TaskID = ' . $TaskID;
                        $cxtRows = $cxtMapper->fetchAll($where);
                        if(count($cxtRows) > 0)
                        {
                            // Task already assigned to this category, skipping
                            continue;
                        } else {
                            $cxt = new Application_Model_CategoryTasks();
                            $cxt->setActive(1)
                                ->setCategoryID($category->getCategoryID())
                                ->setTaskID($TaskID);
                            $cxtMapper->save($cxt);
                        }
                    }
                    $this->view->messages[] = 'Task successfully assigned to all Client Categories named "'.$CategoryName.'"';
                } else {
                    $this->view->messages[] = 'No categories were found by that name.';
                }
                
            }
            
            $CategoryName = $this->getRequest()->getParam('unassignFromCategoryName');
            if($CategoryName)
            {
                
                // Go through categories with this name,
                //  for each category, remove categorytasksmapper
                //  Only if there are no hours associated yet.
                //  Geez.
                
                $CategoryMapper = new Application_Model_CategoryMapper();
                $where = 'Name = \''. $CategoryName . '\'';
                $categories = $CategoryMapper->fetchAll($where);
                if(count($categories) > 0)
                {
                    
                    foreach($categories as $category)
                    {
                        $cxtMapper = new Application_Model_CategoryTasksMapper();
                        $where = 'CategoryID = ' . $category->getCategoryID() . ' AND TaskID = ' . $TaskID;
                        $cxtRows = $cxtMapper->fetchAll($where);
                        if(count($cxtRows) > 0)
                        {
                            foreach($cxtRows as $cxtRow)
                            {
                                $tsMapper = new Application_Model_TimesheetMapper();
                                $where = 'CategoriesXTasksID = ' . $cxtRow->getCategoriesXTasksID();
                                $tsRows = $tsMapper->fetchAll($where);
                                if(count($tsRows) > 0)
                                {
                                    continue;
                                } else {
                                    $cxtMapper->getDbTable()->delete('CategoryID = ' . $category->getCategoryID() . ' AND TaskID = ' . $TaskID);
                                }
                            }
                            
                        } else {
                            // Task not found, not removing
                            continue;
                        }
                    }
                    $this->view->messages[] = 'Task successfully unassigned from all Client Categories named "'.$CategoryName.'" (unless there were hours assigned...)';
                } else {
                    $this->view->messages[] = 'No categories were found by that name.';
                }
            }
            
            
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
            $select = $db->select();
            $select->from('categories')
                   ->order('categories.Name')
                   ->group('categories.Name');
            $statement = $db->query($select);
            $categories = $statement->fetchAll();
            
            if(count($categories) > 0)
            {
                $html = '<div id="table-wrapper"><table class="task-table">';
                $html .= '<tr style="background-color:#E5E4E8;padding:10px;">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td>Assign / Unassign Task to ALL CATEGORIES with this name FOR ALL CLIENTS</td>
                            <td></td>
                            <td></td>
                        </tr>';
                
                foreach($categories as $categoryArray)
                {
                    
                        $html .= '<tr>
                                <td id="col1">
                                  <div class="addMe">
                                        <a href="'.$this->_baseURI.'/admin/assign-task-by-category-name/?taskID='.$TaskID.'&assignToCategoryName='.urlencode($categoryArray['Name']).'" id="addMe'.$categoryArray['CategoryID'].'" class="assignMe" alt="Assign task to this category" title="Assign task to this category"></a>
                                  </div>
                                </td>
                                <td id="col2">
                                  <div class="removeMe">
                                        <a href="'.$this->_baseURI.'/admin/assign-task-by-category-name/?taskID='.$TaskID.'&unassignFromCategoryName='.urlencode($categoryArray['Name']).'" id="removeMe'.$categoryArray['CategoryID'].'" class="unassignMe" alt="Unassign task from this category" title="Unassign task from this category"></a>
                                  </div>
                                </td>
                                <td>'.$categoryArray['Name'].'</td>
                                <td>
                                </td>
                            </tr>'; 
                }
                $html .= '</table>';
            }
            
            $this->view->listTable = $html;
        }
    }    

    // List Employee Hours of Category Tax
    public function listCategoryTaskHoursAction()
    {
        
        $cxtID = $this->getRequest()->getParam('cxtID');
        
        if($cxtID)
        {
            $CategoryID='';
            $CategoryName='';
            $TaskID='';
            $TaskName='';
            
            // Verify the Category Task exists
            // Custom Join Query To Save Queries
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
            $select = $db->select();
            $select->from('categoriesxtasks')
                   ->where('CategoriesXTasksID = '.$cxtID)
                   ->join('categories','categories.CategoryID = categoriesxtasks.CategoryID', 'Name as CategoryName')
                   ->join('tasks','tasks.TaskID = categoriesxtasks.TaskID', 'Name as TaskName');
            $statement = $db->query($select);
            $results = $statement->fetchAll();
            
            if($results) {
                
                // Get Category Name
                $CategoryID = $results[0]['CategoryID'];
                $CategoryName = $results[0]['CategoryName'];
                
                // Get Task Name
                $TaskID = $results[0]['TaskID'];
                $TaskName = $results[0]['TaskName'];
                
                $this->view->title = 'Employee Hours for task "'.$TaskName.'" in category "'.$CategoryName.'"';

                $html = '';
                $html .= "\r\n".'
                    <table class="timesheet-table">
                        <tr style="background-color:#E5E4E8;padding:10px;">
                            <td style="text-align:center;" width="40"></td>
                            <td>Employee</td>
                            <td>Date</td>
                            <td>Duration</td>
                            <td width="200">Comments</td>
                        </tr>';

                $timeSheetMapper = new Application_Model_TimesheetMapper();
                $where = 'CategoriesXTasksID = ' . $cxtID; 
                $tsEntries = $timeSheetMapper->fetchAll($where);
                
                if(count($tsEntries) > 0)
                {
                    foreach($tsEntries as $timeSheet)
                    {
                        $html .= '<tr>
                            <td id="col1">
                              <a href="'.$this->_baseURI 
                                . '/timesheet/edit-entry/?tsID='.$timeSheet->getTimeSheetID() . '">
                                <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                              </a>
                            </td>
                            <td>' . $this->_helper->TimeSheet->getEmployeeInfoById($timeSheet->getEmployeeID())->getName() . '</td>
                            <td>' . $timeSheet->getDate() . '</td>
                            <td>' . $timeSheet->getDuration() . '</td>
                            <td width="200">' . $timeSheet->getComments() . '</td>
                        </tr>';
                    }
                }

                $html .= '            </table>';        

                $this->view->listTable = $html;
            }
        }
    }

   
}

