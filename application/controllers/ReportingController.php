<?php

/******************************************************************************
 * Class name: ReportingController
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      This controller contains all code for reporting timesheets.
 *      If the current user requesting this page is not marked as a reporter
 *      in the system, they are redirected to the IndexController.
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';

class ReportingController extends Zend_Controller_Action
{

    protected $_ei;    
    protected $_baseURI;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if(!$auth->hasIdentity()) {
            $this->_redirect('/auth/');
        }

		$this->_helper->EmployeeInfo->createObject();
		$this->_ei = Zend_Registry::get('EmployeeInfo');
		if(!$this->_ei->isReporter())
		{
			return $this->_redirect('/');
			exit();
		}
		$this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
    }
    
    public function indexAction()
    {
        $fromDate = $this->getRequest()->getParam('fromDate');
        $toDate = $this->getRequest()->getParam('toDate');
        $empID = $this->getRequest()->getParam('EmployeeID');
        
        if(!$fromDate || !$toDate)
        {
            $fromTime = strtotime( date('Y') . '-' . date('m') . '-'. '01' );
            $fromDate = date ( 'Y-m-d' , $fromTime );
            $toTime = strtotime('- 1 day', strtotime('+ 1 month', $fromTime));
            $toDate = date('Y-m-d',$toTime);
        }
            
        // No matter where the dates came from, make sure they're formatted now
        $fromDate = date ( 'Y-m-d' , strtotime($fromDate) );
        $toDate = date ( 'Y-m-d' , strtotime($toDate) );
       
        $adminReport = $this->_helper->Reporting->createReport($fromDate, $toDate, $empID);
        if(!$adminReport)
        {
            $adminReport = 'No data for this employee yet.';
        }
        
        $this->view->title = 'Reporting';
        $this->view->fromDate = $fromDate;
        $this->view->toDate = $toDate;
        $this->view->employeeID = $empID;
        $this->view->baseURI = $this->_baseURI;
        $this->view->content = $adminReport;
    }
   
    // This function exports the data with the specified filters to an excel file
    function exportAction()
    {
        $where='';
        
        $employeeID = $this->getRequest()->getParam('employee');
        if(isset($employeeID) && is_numeric($employeeID))
        {
            $where = 'e.EmployeeID = ' . $employeeID . ' AND '; 
        }
        
        $fromDate = $this->getRequest()->getParam('fromDate');
        if(!isset($fromDate))
        {
            return $this->_redirect('/reporting');
            exit();
        }

        $toDate = $this->getRequest()->getParam('toDate');
        if(!isset($toDate))
        {
            return $this->_redirect('/reporting');
            exit();
        }
        
        $fromTime = strtotime($fromDate);
        $toTime = strtotime($toDate);
        
        $where .= 'ts.Date >= \''. date('Y-m-d', $fromTime) . '\'' .
                  ' AND ts.Date <= \''. date('Y-m-d', $toTime) . '\'';

        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                     ->from(
                            array(
                                'ts' => 'timesheet'
                            ),
                            array(
                                'Client' => 'c.Name',
                                'Category' => 'cat.Name',
                                'Task' => 't.Name',
                                'Employee' => 'e.Name',
                                'Department' => 'd.Name',
                                'ts.Date',
                                'ts.Duration',
                                'ts.Comments'
                            )
                        )
                    ->joinLeft(
                            array(
                                'cxt' => 'categoriesxtasks'
                            ),
                            'ts.CategoriesXTasksID = cxt.CategoriesXTasksID'
                        )
                    ->joinLeft(
                            array(
                                'cat' => 'categories'
                            ),
                            'cxt.CategoryID = cat.CategoryID'
                        )
                    ->joinLeft(
                            array(
                                't' => 'tasks'
                            ),
                            'cxt.TaskID = t.TaskID'
                        )
                    ->joinLeft(
                            array(
                                'e' => 'employees'
                            ),
                            'ts.EmployeeID = e.EmployeeID'
                        )
                    ->joinLeft(
                            array(
                                'd' => 'departments'
                            ),
                            'e.DepartmentID = d.DepartmentID'
                        )
                    ->joinLeft(
                            array(
                                'c' => 'clients'
                            ),
                            'cat.ClientID = c.ClientID'
                        )
                    ->where($where)
                    ->order(
                        'c.Name',
                        'cat.Name',
                        't.Name',
                        'e.Name',
                        'd.Name',
                        'ts.Date'
                        );
        $statement = $db->query($select);
        $entries = $statement->fetchAll();   
        if(count($entries) > 0)
        {
            $line = '"Client","Category","Task","Employee","Department","Date","Duration","Comments"'."\r\n";            
            foreach($entries as $entry)
            {
                $line .= '"'.$entry['Client'] . '",';
                $line .= '"'.$entry['Category'] . '",';
                $line .= '"'.$entry['Task'] . '",';
                $line .= '"'.$entry['Employee'].'",';
                $line .= '"'.$entry['Department'].'",';
                $line .= '"'.$entry['Date'] . '",';
                $line .= '"'.$entry['Duration'] . '",';
                $line .= '"'.$entry['Comments'] . '"';
                $line .= "\r\n";
            }

            $this->_helper->viewRenderer->setNeverRender();
            header("Content-type: application/vnd.ms-excel");
            header("Content-disposition: attachment; filename=time-tracker-data-export-".date('YmdHis').".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $line;
            die();
        
        } else {
            $errors = array();
            
            $this->view->title = 'Export';            
            $error = 'Sorry, but there were no results for the following filter: ' . "<br /><br />";
            
            if(is_numeric($employeeID))
            {
                $error .= 'Employee : ' . $this->_helper->TimeSheet->getEmployeeInfoById($employeeID)->getName();
            } else {
                $error .= 'Employee :  (All)';
            }
            $error .= 'From : ' . $fromDate . "<br />";
            $error .= 'To : ' . $toDate . "<br /><br />";
            $error .= 'Click <a href="'.$this->_baseURI.'/reporting/">here</a> to return to the reporting page.';
            $errors[] = $error;
            if(count($errors) > 0) $this->view->errors = $errors;
        }
    }    

}
