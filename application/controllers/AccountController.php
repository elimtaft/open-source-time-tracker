<?php

/******************************************************************************
 * Class name: AccountController
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      This controller contains all account-specific functionality.  Currently,
 *      the only provided functionality is changing the password.  Thus, the index
 *      Action creates a form for a user to enter the current password and
 *      enter the new password, and this form posts to the password-change action.
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';

class AccountController extends Zend_Controller_Action
{
    
    protected $_baseURI;
    protected $_ei;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if(!$auth->hasIdentity()) {
            return $this->_redirect('/auth/'); // test
        } else {
            $this->_helper->EmployeeInfo->createObject();
            $this->_ei = Zend_Registry::get('EmployeeInfo');
            $this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
        }
    }
    
    public function indexAction() 
    {
        $this->view->title = 'Account';
        $action = $this->_baseURI . '/account/password-change';
        $mode = 'account-password-change';
        $this->view->changePasswordForm = $this->_helper->Form->getForm($action, $mode);
    }
    
    
    public function passwordChangeAction()
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Account';
        $action = $this->_baseURI . '/account/password-change';
        $mode = 'account-password-change';
        $form = $this->view->changePasswordForm = $this->_helper->Form->getForm($action, $mode);
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $ei = Zend_Registry::get('EmployeeInfo');
                $values = $form->getValues();
                $currentPassword = $values['currentPassword'];
                $newPass = $values['newPass'];
                $newPassRepeat = $values['newPassRepeat'];
                $passForInsert = $this->_helper->Password->hashItOut(
                    $ei->getUsername(), $newPass);

                $employeeMapper = new Application_Model_EmployeeMapper();
                $employee = new Application_Model_Employee();
                $employeeMapper->find($ei->getEmployeeID(),$employee);
                if($employee)
                {
                    $employee->setPassword($passForInsert);
                }
                $employeeMapper->save($employee);
                $messages[] = '<strong>Password updated successfully.</strong>' . '<br />';

           } else {
               $errors = $this->_helper->Form->getCustomErrors($form);
           }
        }
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
    }
    

}
