<?php
/******************************************************************************
 * Class name: TimesheetController
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      Contains all the functionality related to entering time.  Ther'es actually
 *      a lot going on here, which is why this file is so huge.  Employees must
 *      select Client -> Category -> Task before they can enter time, and they only
 *      see applicable data (when client is selected, only the Categories belonging
 *      to that client are displayed, and the lists update dynamically).  
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';

class TimesheetController extends Zend_Controller_Action
{

    protected $_ei;
    protected $_baseURI;
    
    public function init()
    {
        
        $auth = Zend_Auth::getInstance();
 
        if(!$auth->hasIdentity()) {
            $this->_redirect('/auth/');
        }
        
        $this->_helper->EmployeeInfo->createObject();
        $this->_ei = Zend_Registry::get('EmployeeInfo');
        $this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();

    }
    
    
    public function indexAction() 
    {
        $this->_redirect('/timesheet/new-entry/');
    }
    

    
    public function newEntryAction() 
    {
        
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Timesheet Data Entry';
        $this->view->EmployeeID = $this->_ei->getEmployeeID();

        $myCCT = $this->_ei->getCCT();
        $this->view->CTT = $myCCT;

        // New Form
        $form = $this->getNewForm();
        $this->view->form = $form;

        // If new entry submitted:
        if($this->getRequest()->getParam('createEdit'))
        {
            if($form->isValid($_POST))
            {
                $formValues = $form->getValues();
                $timeSheetMapper = new Application_Model_TimesheetMapper();                
                $timeSheet = new Application_Model_Timesheet();
                $timeSheet->setEmployeeID($this->_ei->getEmployeeID())
                          ->setCategoriesXTasksID($formValues['task'])
                          ->setDate($this->fixDateForTimeSheetInsert($formValues['date']))
                          ->setDuration($formValues['duration'])
                          ->setComments($formValues['comments']);
                $timeSheetMapper->save($timeSheet);
                $messages[] = 'Timesheet entry was accepted.';
                $form->reset();
                $form = $this->getNewForm();
                $this->view->form = $form;
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }

        // If Edit Existing
        if($this->getRequest()->getParam('editExistingTimesheet'))
        {
            
            $timeSheetID = $this->getRequest()->getParam('editExistingTimesheet');
            $ts = $this->_helper->TimeSheet->getTimeSheetInfoById($timeSheetID);

            if(!is_numeric($timeSheetID) || !$ts->getTimeSheetID())
            {
                $errors[] = 'The timesheet you attempted to edit ('.$timeSheetID.') could not be found.';
            } else {

                // If this timesheet doesn't belong to me, I better be an admin...
                if(!$this->_ei->timesheetEntryExists($timeSheetID))
                {
                    if(!$this->_ei->isAdmin)
                    {
                        $errors[] = 'You are attempting to access a restricted area.';
                    } else {
                        $employee = $this->getEmployeeInfoFromTimeSheet($timeSheetID);
                        $this->view->EmployeeName = $employee->getName();
                    }
                }

                $form2 = $this->getEditForm($timeSheetID);
                
                if ($form2->isValid($_POST))
                {

                    $formValues = $form2->getValues();
                    if(!$this->_ei->timesheetEntryExists($timeSheetID))
                    {
                        $employee = $this->getEmployeeInfoFromTimeSheet($timeSheetID);
                        $EmployeeID = $employee->getEmployeeID();

                        // If we've gotten this far, we're an admin, we're editing 
                        // a user's timesheet, and it's going to save.  Log it. 
                        $this->_helper->Logger->logTimeSheetEdit(
                            $timeSheetID, 
                            date('Y-m-d H:i:s'), 
                            $this->_ei->getEmployeeID(), 
                            $form2->getValues());

                    } else {
                        $EmployeeID = $this->_ei->getEmployeeID();
                    }

                    $timeSheet = new Application_Model_Timesheet();
                    $timeSheet->setTimeSheetID($timeSheetID);
                    $timeSheet->setEmployeeID($EmployeeID);
                    $timeSheet->setCategoriesXTasksID($formValues['task']);
                    $timeSheet->setDate($this->fixDateForTimeSheetInsert($formValues['date']));
                    $timeSheet->setDuration($formValues['duration']);
                    $timeSheet->setComments($formValues['comments']);
                    $timeSheetMapper = new Application_Model_TimesheetMapper();
                    $timeSheetMapper->save($timeSheet);
                    $messages[] = 'Timesheet entry was updated.';
                } else {
                    $errors = $this->_helper->Form->getCustomErrors($form2);
                }                    
            }
        }
        
        if($this->getRequest()->getParam('deleteTimesheet'))
        {
            
            $timeSheetID = $this->getRequest()->getParam('deleteTimesheet');
            if(!$timeSheetID || !is_numeric($timeSheetID)) $this->_redirect('/admin');

            // If this timesheet doesn't belong to me, I better be an admin...
            if(!$this->_ei->timesheetEntryExists($timeSheetID))
            {
                $errors[] = 'You are attempting to access a restricted area.';
            } else {
                
                $tsMapper = new Application_Model_TimesheetMapper();
                $tsMapper->getDbTable()->delete('TimesheetID = ' . $timeSheetID);
                $messages[] = 'Timesheet entry was deleted.';
            }
        }
        
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
        $this->view->dayViewEditForm = $this->createDayViewTimeSheetData();        
    }    
    
    public function editEntryAction() 
    {
        $errors = array();
        $messages = array();
        
        $this->view->title = 'Timesheet Data Entry';

        $timeSheetID = $this->getRequest()->getParam('tsID');
        if(!$timeSheetID || !is_numeric($timeSheetID)) $this->_redirect('/admin');

        $ts = $this->_helper->TimeSheet->getTimeSheetInfoById($timeSheetID);
        if(!$ts->getTimeSheetID())
        {
            $errors[] = 'The specified timesheet ('.$timeSheetID.') could not be found.';
        } else {
            
            // If this timesheet doesn't belong to me, I better be an admin...
            if(!$this->_ei->timesheetEntryExists($timeSheetID))
            {
                if(!$this->_ei->isAdmin)
                {
                    $errors[] = 'You are attempting to access a restricted area.';
                } else {
                    $employee = $this->getEmployeeInfoFromTimeSheet($timeSheetID);
                    $this->view->EmployeeName = $employee->getName();
                    
                    $empData = new stdClass();
                    $empData->EmployeeID = $employee->getEmployeeID();
                    $empData->Name = $employee->getName();
                    $empData->DepartmentID = $employee->getDepartmentID();
                    $empData->Username = $employee->getUsername();
                    $empData->Password = $employee->getPassword();
                    $empData->IsAdmin = $employee->getIsAdmin();
                    $empData->IsReporter = $employee->getIsReporter();
                    $empData->Active = $employee->getActive();
                    
                    $empEIObject = $this->_helper->EmployeeInfo->createObject($empData);
        
                    $myCCT = $empEIObject->getCCT();
                    $this->view->CTT = $myCCT;                    
                }
            } else {
                $myCCT = $this->_ei->getCCT();
                $this->view->CTT = $myCCT;
            }
        }

        if(count($errors) == 0)
        {
            $this->view->form = $form = $this->getEditForm($timeSheetID);

            if ($this->getRequest()->isPost())
            {
                if ($form->isValid($_POST))
                {

                    $formValues = $form->getValues();
                    if(count($errors) == 0)
                    {
                        if(!$this->_ei->timesheetEntryExists($timeSheetID))
                        {
                            $employee = $this->getEmployeeInfoFromTimeSheet($timeSheetID);
                            $EmployeeID = $employee->getEmployeeID();

                            // If we've gotten this far, we're an admin, we're editing 
                            // a user's timesheet, and it's going to save.  Log it. 
                            $this->_helper->Logger->logTimeSheetEdit(
                                $timeSheetID, 
                                date('Y-m-d H:i:s'), 
                                $this->_ei->getEmployeeID(), 
                                $form->getValues());

                        } else {
                            $EmployeeID = $this->_ei->getEmployeeID();
                        }                        
                    }


                    $timeSheet = new Application_Model_Timesheet();
                    $timeSheet->setTimeSheetID($timeSheetID);
                    $timeSheet->setEmployeeID($EmployeeID);
                    $timeSheet->setCategoriesXTasksID($formValues['task']);
                    $timeSheet->setDate($this->fixDateForTimeSheetInsert($formValues['date']));
                    $timeSheet->setDuration($formValues['duration']);
                    $timeSheet->setComments($formValues['comments']);
                    $timeSheetMapper = new Application_Model_TimesheetMapper();
                    $timeSheetMapper->save($timeSheet);
                    $messages[] = 'Timesheet entry was accepted.';
                } else {
                    $errors = $this->_helper->Form->getCustomErrors($form);
                }
            }
        }
        if(count($errors) > 0) $this->view->errors = $errors;
        if(count($messages) > 0) $this->view->messages = $messages;
    }

    public function historyAction()
    {
        $this->view->title = 'Timesheet History';
        $timeSheetData = $this->createTimeSheetHistoryData();
        $timeSheetData .= '<br><br>';
        $this->view->content = $timeSheetData;
    }


    // The code for the timesheet New and Edit forms have not yet been placed
    // in the Form helper.   
    public function getNewForm()
    {

        if($this->getRequest()->getParam('date'))
        {
            $date = $this->getRequest()->getParam('date');
            $dateTime = strtotime($date);
            $dateVal = date('m/d/Y',$dateTime);
            $dateParam = '?date='.$dateVal;
        } else {
            $dateVal = date('m/d/Y');
            $dateParam = '';
        }
        
        $form = new Zend_Form();
        $form->setMethod('post')
             ->setAction($this->_baseURI . '/timesheet/new-entry/'.$dateParam)
             ->setAttrib('class', 'new-create-form');
        
        
        $clientsSelect = $this->getClientSelectByEmployeeID($this->_ei->getEmployeeID());
        $client = $form->createElement('select', 'client');
        $client->setRequired(true)
               ->addMultiOptions($clientsSelect);
        
        
        // Create drop-down list of categories (only if Client previously selected)
        $ClientID = $this->getRequest()->getParam('client');
        if($ClientID)
        {
            $categoriesSelect = $this->getCategorySelectByClientId($ClientID);
        } else {
            $categoriesSelect = array('' => 'Select Category...');
        }
        $category = $form->createElement('select', 'category');
        $category->setRequired(true)
                ->setRegisterInArrayValidator(false)
                ->addMultiOptions($categoriesSelect);
        
        $CategoryID = $this->getRequest()->getParam('category');
        // Create drop-down list of tasks (only if Category previously selected)
        if($CategoryID && $CategoryID > 0)
        {
            $taskSelect = $this->getTaskSelectByCategoryID($CategoryID);
        } else {
            $taskSelect = array('' => 'Select Task... ');
        }

        
        $task = $form->createElement('select', 'task');
        $task->setRequired(true)
             ->setRegisterInArrayValidator(false)
             ->addMultiOptions($taskSelect);

        $date = $form->createElement('hidden', 'date');
        $date->setValue($dateVal);
        $date->setRequired(true);
        
        $durationSelect = $this->getDurationSelect();
        $duration = $form->createElement('select', 'duration');
        $duration->setLabel('Duration')
             ->setRequired(true)
             ->setRegisterInArrayValidator(false)
             ->addMultiOptions($durationSelect);
        
        $comments = $form->createElement('text', 'comments');
        $comments->setRequired(false)
                 ->setAttrib('COLS', '30')
                 ->setAttrib('ROWS', '4');
        
        $hiddenName = $form->createElement('hidden', 'createEdit');
        $hiddenName->setValue("1");
        
        // Add elements to form:
        $form->addElement($client)
             ->addElement($date)
             ->addElement($category)
             ->addElement($task)
             ->addElement($duration)
             ->addElement($comments)
             ->addElement($hiddenName)
             ->addElement('submit', 'submit', array('label' => 'Add New'));
        
        return $form;
    }
    
    
    public function getEditForm($id)
    {
        $timesheet = $this->_helper->TimeSheet->getTimeSheetInfoById($id);

        if(!$timesheet || !$timesheet->getTimeSheetID()) return false;
        $ClientID = false;
        $CategoryID = false;
        $TaskID = false;

        $cxtMapper = new Application_Model_CategoryTasksMapper();
        $cxt = new Application_Model_CategoryTasks();
        $cxtMapper->find($timesheet->getCategoriesXTasksID(), $cxt);
        if($cxt)
        {
            $CategoryID = $cxt->getCategoryID();
            $TaskID = $cxt->getTaskID();
            
            // Get client ID from CategoryID
            $categoryMapper = new Application_Model_CategoryMapper();
            $category = new Application_Model_Category();
            $categoryMapper->find($CategoryID, $category);
            if($category)
            {
                $ClientID = $category->getClientID();
            }
        }
        if($this->getRequest()->getParam('client'))
        {
            $ClientID = $this->getRequest()->getParam('client');
        }
        if($this->getRequest()->getParam('category'))
        {
            $CategoryID = $this->getRequest()->getParam('category');
        }
        if($this->getRequest()->getParam('task'))
        {
            $TaskID = $this->getRequest()->getParam('task');
        } else {
            $TaskID = $timesheet->getCategoriesXTasksID();
        }        
        $form = new Zend_Form();
        $form->setMethod('post');
        $form->setAction($this->_baseURI . '/timesheet/edit-entry/?tsID='.$id);        
        
        
        $clientsSelect = $this->getClientSelectByEmployeeID($timesheet->getEmployeeID());
        $client = $form->createElement('select', 'client');
        $client->setLabel('Client')
               ->setRequired(true)
               ->addMultiOptions($clientsSelect)
               ->setValue($ClientID);
        
        if($this->_ei->timesheetEntryExists($timesheet->getTimeSheetID()))
        {
            $categoriesSelect = $this->getCategorySelectByClientId($ClientID);
        } else {
            $categoriesSelect = $this->getCategorySelectByClientId($ClientID, $timesheet->getEmployeeID());
        }
        $category = $form->createElement('select', 'category');
        $category->setLabel('Category')
                ->setRequired(true)
                ->setRegisterInArrayValidator(false)
                ->addMultiOptions($categoriesSelect)
                ->setValue($CategoryID);
                
        
        $taskSelect = $this->getTaskSelectByCategoryID($CategoryID);
        $task = $form->createElement('select', 'task');
        $task->setLabel('Task')
             ->setRequired(true)
             ->setRegisterInArrayValidator(false)
             ->addMultiOptions($taskSelect)
             ->setValue($TaskID);

        $tmpDate = $timesheet->getDate();
        $dateVal = date('m/d/Y', strtotime($tmpDate));
        $date = $form->createElement('text', 'date');
        $date->setLabel('Date');
        $date->setValue($dateVal);
        $date->setRequired(true);
        
        $durationSelect = $this->getDurationSelect();
        $duration = $form->createElement('select', 'duration');
        $duration->setLabel('Duration')
             ->setRequired(true)
             ->setRegisterInArrayValidator(false)
             ->addMultiOptions($durationSelect)
             ->setValue($timesheet->getDuration());
        
        $comments = $form->createElement('textarea', 'comments');
        $comments->setLabel('Comments')
                 ->setRequired(false)
                 ->setValue($timesheet->getComments())
                 ->setAttrib('COLS', '30')
                 ->setAttrib('ROWS', '4');

        // Add elements to form:
        $form->addElement($client)
             ->addElement($category)
             ->addElement($task)
             ->addElement($date)
             ->addElement($duration)
             ->addElement($comments)
             ->addElement('submit', 'submit', array('label' => 'Submit'));
     
        return $form;
    }
    
    
    function createDayViewTimeSheetData()
    {
        // Get Filter Parameters, if any
        $EmployeeID = $this->_ei->getEmployeeID();
        
        $date = $this->getRequest()->getParam('date');
        if(!$date)
        {
            $date = date('m/d/Y');
            $dateTime = strtotime($date);
        } else {
            $dateTime = strtotime($date);
            $date = date('m/d/Y', $dateTime);
        }

        $dateMySQLWhereClaus = " AND Date = '". date('Y-m-d', $dateTime) ."'";
        
        // Table Header
        $tableRowHeader = "\r\n".'
            <table class="timesheet-entries-table">
                <tr class="first">';
        
                $tableRowHeader .= '
                    <td id="col1">Client</td>
                    <td id="col2">Category</td>
                    <td id="col3">Task</td>
                    <td id="col4">Duration</td>
                    <td id="col5">Comments</td>
                    <td id="col6"></td>
                </tr>';
        
                
        $tableRowFooter = '
            </table>';
        
        
        // Table Rows
        $tableRows = array();
        $timeSheetMapper = new Application_Model_TimesheetMapper();
        $timeSheetWhere = 'EmployeeID = ' . $this->_ei->getEmployeeID();
        $timeSheetWhere .= $dateMySQLWhereClaus;

        $timeSheets = $timeSheetMapper->fetchAll($timeSheetWhere);
        $totalHours = 0;

        if(count($timeSheets) > 0)
        {
            $rowCnt = 0;
            foreach($timeSheets as $timeSheet)
            {
                $rowCnt++;
                
                // Use this to get client and category data
                $CategoryID = '';
                $CategoryName = '';
                $ClientID = '';
                $ClientName = '';
                $TaskID = '';
                $TaskName = '';
                
                $timeSheet->getCategoriesXTasksID();
                
                // First get category id
                $cxtMapper = new Application_Model_CategoryTasksMapper();
                $cxtWhere = 'CategoriesXTasksID = ' . $timeSheet->getCategoriesXTasksID();
                $cxts = $cxtMapper->fetchAll($cxtWhere);
                if(count($cxts)==1)
                {
                    $CategoryID = $cxts[0]->getCategoryID();
                    $TaskID = $cxts[0]->getTaskID();
                }
                
                if(!isset($CategoryID) || is_null($CategoryID) || $CategoryID=='')
                { 
                    die('this category id is blank: ' . $timeSheet->getCategoriesXTasksID());
                }
                
                // Get category name and client ID
                $categoryMapper = new Application_Model_CategoryMapper();
                $categoryWhere = 'CategoryID = ' . $CategoryID;
                $categories = $categoryMapper->fetchAll($categoryWhere);
                if(count($categories)==1)
                {
                    $CategoryName = $categories[0]->getName();
                    $ClientID = $categories[0]->getClientID();
                }
                
                // Get client name
                $clientMapper = new Application_Model_ClientMapper();
                $clientWhere = 'ClientID = ' . $ClientID;
                $clients = $clientMapper->fetchAll($clientWhere);
                if(count($clients)==1)
                {
                    $ClientName = $clients[0]->getName();
                }
                
                // Get task name.
                $taskMapper = new Application_Model_TaskMapper();
                $taskWhere = 'TaskID = ' . $TaskID;
                $tasks = $taskMapper->fetchAll($taskWhere);
                if(count($tasks)==1)
                {
                    $TaskName = $tasks[0]->getName();
                }                
                
                $singleRow = '
                <tr>
                    <td id="container" class="container" colspan="6">
                      <div class="single-entry-edit">
                        <form method="post" action="'.$this->_baseURI . '/timesheet/new-entry/">
                            <div class="client">' . $this->getClientSelectedSelect($rowCnt, $ClientID) . '</div>
                            <div class="category">' . $this->getCategorySelectedSelect($rowCnt, $ClientID, $CategoryID) . '</div>
                            <div class="task">' . $this->getTaskSelectedSelect($rowCnt, $CategoryID, $timeSheet->getCategoriesXTasksID()) . '</div>
                            <div class="duration">' . $this->getDurationSelectedSelect($timeSheet->getDuration()) . '</div>
                                <!--
                            <div class="duration"><input type="text" name="duration" value="' . $timeSheet->getDuration() . '" /></div>
                                -->
                            <div class="comments"><input type="text" name="comments" value="' . $timeSheet->getComments() . '" /></div>
                            <div class="submit">
                                        <input type="hidden" name="date" value="'.$timeSheet->getDate().'" />
                                        <input type="hidden" name="editExistingTimesheet" value="'.$timeSheet->getTimeSheetID().'" />
                                        <input id="update" type="image" src="'.$this->_baseURI . '/images/update.jpg" title="Update" />
                                        <a class="delete" href="'.$this->_baseURI.'/timesheet/new-entry/?date='.$date.'&deleteTimesheet='.$timeSheet->getTimeSheetID().'"><img src="'.$this->_baseURI . '/images/delete.jpg" title="Delete" /></a>
                            </div>
                        </form>
                        </div>
                    </td>
                </tr>';
                
                $tableRows[] = $singleRow;
                
                list ($hr, $min) = explode(':', $timeSheet->getDuration());
                $hr = $hr * 60; // convert hours to minutes, then add to minutes for total time in minutes
                $duration = $hr + $min;
                $totalHours += $duration;
            }
        }
        $tableRowsSection = '';
        foreach($tableRows as $row)
        {
            $tableRowsSection .= $row;
        }

        // Form Filter
        $totalHours = $this->convertMinutesToHours($totalHours);
        $formFilter = '';          
        $formFilter .= '<div class="filter">'."\r\n";
        $formFilter .= '  <div id="date-form">'."\r\n";
        $formFilter .= '    <form method="post" action="'.$this->_baseURI . '/timesheet/new-entry/'.'" method="GET">';
        $formFilter .= '      <label for="date">Date: </label>'."\r\n";
        $formFilter .= '      <input id="date" type="text" value="'.$date.'" name="date">'."\r\n";
        
        $formFilter .= '      <input type="submit" value="Go">'."\r\n";        
        $formFilter .= '    </form>'."\r\n";
        $formFilter .= '  </div>'."\r\n";
        $formFilter .= '  <div id="totalHours"><strong>Total hours: ' . $totalHours . '</strong></div>'."\r\n";
        $formFilter .= '</div>'."\r\n";


        $html='';
        $html .= $formFilter;
        $html .= $tableRowHeader;
        $html .= $tableRowsSection;
        $html .= $tableRowFooter;
        
        return $html;
    }
    
    public function getClientSelectedSelect($rowCnt, $ClientID)
    {
        $clients = $this->_ei->getClients();
        $html = '<select id="row-'.$rowCnt.'-client" class="editClient" name="client">'."\r\n";
        
        foreach($clients['ByClientID'] as $id => $element)
        {
            
            if($id==$ClientID)
            {
                $selected='selected="selected"';
            } else {
                $selected='';
            }
            $html .= '  <option '.$selected.' label="'.$element['Name'].'" value="'.$id.'">'.$element['Name'].'</option>'."\r\n";
        }
        $html .= '</select>'."\r\n";
        return $html;
    }
    
    public function getCategorySelectedSelect($rowCnt, $ClientID, $CategoryID)
    {
        $categories = $this->_ei->getCategories();
        
        $html = '<select id="row-'.$rowCnt.'-category" class="editCategory" name="category">'."\r\n";
        
        foreach($categories['ByClientID'][$ClientID] as $id => $element)
        {
            if($element['CategoryID']==$CategoryID)
            {
                $selected='selected="selected"';
            } else {
                $selected='';
            }
            $html .= '  <option '.$selected.' label="'.$element['Name'].'" value="'.$element['CategoryID'].'">'.$element['Name'].'</option>'."\r\n";
        }
        $html .= '</select>'."\r\n";
        return $html;
    }
    
    public function getDurationSelect()
    {
        $d = $this->getDurationValues();
        $durationSelect = array();
        foreach($d as $val)
        {
            $durationSelect[$val] = $val;
        }
        return $durationSelect;
    }
    public function getDurationSelectedSelect($duration)
    {
        $d = $this->getDurationValues();
        $html = '<select name="duration">'."\r\n";
        foreach($d as $dur)
        {
            if($dur==$duration)
            {
                $selected='selected="selected"';
            } else {
                $selected='';
            }
            $html .= '  <option '.$selected.' value="'.$dur.'">'.$dur.'</option>'."\r\n";
        }
        $html .= '</select>'."\r\n";
        return $html;        
    }
    public function getTaskSelectedSelect($rowCnt, $CategoryID, $TaskID)
    {
        $tasks = $this->_ei->getTasks();
        
        $html = '<select id="row-'.$rowCnt.'-task" name="task">'."\r\n";
        
        foreach($tasks['ByCategoryID'][$CategoryID] as $id => $element)
        {
            if($id==$TaskID)
            {
                $selected='selected="selected"';
            } else {
                $selected='';
            }
            $html .= '  <option '.$selected.' label="'.$element['Name'].'" value="'.$id.'">'.$element['Name'].'</option>'."\r\n";
        }
        $html .= '</select>'."\r\n";
        return $html;
    }
    
    public function getDurationValues()
    {
        $hours = array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14');
        $increments = array('00','15','30','45');
        $d = array();
        foreach($hours as $hour)
        {
            foreach($increments as $increment)
            {
                $d[] = $hour.':'.$increment;
            }
        }
        $d[] = '15:00';
        return $d;
    }    
    
    function createTimeSheetHistoryData()
    {
        
        // Get Filter Parameters, if any
        $fromDate = $this->getRequest()->getParam('fromDate');
        $toDate = $this->getRequest()->getParam('toDate');
        $EmployeeID = $this->getRequest()->getParam('eid');
        
        if(isset($EmployeeID) && !is_numeric($EmployeeID)) $EmployeeID=null;
        
        if(isset($fromDate))
        {
            $fromTime = strtotime($fromDate);
            $fromDate = date ( 'm/d/Y' , $fromTime );
        } else {
            $fromTime = strtotime( date('Y') . '-' . date('m') . '-'. '01' );
            $fromDate = date ( 'm/d/Y' , $fromTime );
        }
        $fromDateMySQLWhereClaus = " AND ts.Date >= '". date('Y-m-d', $fromTime) ."'";

        if(isset($toDate))
        {
            $toTime = strtotime($toDate);
            $toDate = date('m/d/Y',$toTime);
        } else {
            $toTime = strtotime('- 1 day', strtotime('+ 1 month', $fromTime));
            $toDate = date('m/d/Y',$toTime);
        }
        $toDateMySQLWhereClaus = " AND ts.Date <= '". date('Y-m-d', $toTime) ."'";
        
        
        if(isset($EmployeeID) && ( $this->_ei->isAdmin() || $this->_ei->isReporter()) )
        {
            $timeSheetWhere = 'e.EmployeeID = ' . $EmployeeID;
        } else {
            if($this->_ei->isAdmin() || $this->_ei->isReporter())
            {
                $timeSheetWhere = '1';
            } else {
                $timeSheetWhere = 'e.EmployeeID = ' . $this->_ei->getEmployeeID();
            }
        }
        
        $timeSheetWhere .= $fromDateMySQLWhereClaus . $toDateMySQLWhereClaus;
        
        // Table Header
        $tableRowHeader = "\r\n".'
            <table class="timesheet-table">
                <tr style="background-color:#E5E4E8;padding:10px;">
                    <td style="text-align:center;" width="40"></td>';
        
                if($this->_ei->isAdmin() || $this->_ei->isReporter()) $tableRowHeader .= '
                    <td>Employee</td>';
        
                $tableRowHeader .= '
                    <td>Client</td>
                    <td>Category</td>
                    <td>Task</td>
                    <td>Date</td>
                    <td>Duration</td>
                    <td width="200">Comments</td>
                </tr>';
                
        $tableRowFooter = '
            </table>';
        
        
        // Table Rows
        $tableRows = array();
        $totalHours = 0;
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                     ->from(
                            array(
                                'ts' => 'timesheet'
                            ),
                            array(
                                'Employee' => 'e.Name',
                                'Client' => 'c.Name', 
                                'Category' => 'cat.Name',
                                'Task' => 't.Name',
                                'ts.Date',
                                'ts.Duration',
                                'ts.Comments',
                                'ts.TimeSheetID'
                            )
                        )
                    ->joinLeft(
                        array('e' => 'employees'), 'ts.EmployeeID = e.EmployeeID')
                    ->joinLeft(
                        array('cxt' => 'categoriesxtasks'), 'cxt.CategoriesXTasksID = ts.CategoriesXTasksID')
                    ->joinLeft(
                        array('cat' => 'categories'), 'cxt.CategoryID = cat.CategoryID')
                    ->joinLeft(
                        array('t' => 'tasks'), 'cxt.TaskID = t.TaskID')
                    ->joinLeft(
                        array('c' => 'clients'), 'cat.ClientID = c.ClientID')
            
                    ->where($timeSheetWhere)
                    ->order('ts.Date');
        
        $statement = $db->query($select);
        $entries = $statement->fetchAll();   
        if(count($entries) > 0)
        {
            foreach($entries as $entry)
            {
                $singleRow = '';
                
                $singleRow = '<tr>
                    <td id="col1">
                      <a href="' . $this->_baseURI
                      .'/timesheet/edit-entry/?tsID='.$entry['TimeSheetID'] . '">
                        <img src="'.$this->_baseURI.'/images/pencil.png'.'" alt="Edit" title="Edit" />
                      </a>
                    </td>';
                
                if($this->_ei->isAdmin() || $this->_ei->isReporter()) $singleRow .= '
                    <td>'.$entry['Employee'].'</td>';
                
                $singleRow .= '
                    <td>' . $entry['Client'] . '</td>
                    <td>' . $entry['Category'] . '</td>
                    <td>' . $entry['Task'] . '</td>
                    <td>' . $entry['Date'] . '</td>
                    <td style="text-align:center;">' . $entry['Duration'] . '</td>
                    <td width="200">' . $entry['Comments'] . '</td>
                </tr>';
                
                $tableRows[] = $singleRow;
                
                list ($hr, $min) = explode(':', $entry['Duration']);
                $hr = $hr * 60; // convert hours to minutes, then add to minutes for total time in minutes
                $duration = $hr + $min;
                $totalHours += $duration;
            }
        }
        
        
        
        $tableRowsSection = '';
        foreach($tableRows as $row)
        {
            $tableRowsSection .= $row;
        }
        
        // Form Filter
        //$employeeSelect = $this->getEmployeeSelectSection($EmployeeID);
        
        $this->_ei = Zend_Registry::get('EmployeeInfo');
        $employeeSelect = '';
        $employeeSelect .= '<option value="">-- All --</option>';
        $employeesArr = array();
        $employeesMapper = new Application_Model_EmployeeMapper();
        $employees = $employeesMapper->fetchAll(null,'Name');
        if(count($employees) > 0)
        {
            foreach($employees as $employee)
            {
                $employeesArr[$employee->getEmployeeID()] = $employee->getName();
            }
        }

        
        foreach($employeesArr as $employeeID => $employeeName)
        {
            if(!is_null($EmployeeID) && $EmployeeID == $employeeID)
            {
                $employeeSelect .= '<option value="'.$employeeID.'" selected="selected">'.$employeeName.'</option>';    
            } else {
                $employeeSelect .= '<option value="'.$employeeID.'">'.$employeeName.'</option>';    
            }
            
        }


        
        
        
        $totalHours = $this->convertMinutesToHours($totalHours);
        $formFilter = '';          
        $formFilter .= '<div class="filter">'."\r\n";
        $formFilter .= '  <div id="filter-form">'."\r\n";
        $formFilter .= '    <form method="post" action="'.$this->_baseURI . '/timesheet/history/'.'" method="GET">';
        $formFilter .= '      <label for="fromDate">From: </label>'."\r\n";
        $formFilter .= '      <input id="fromDate" type="text" value="'.$fromDate.'" name="fromDate">'."\r\n";
        $formFilter .= '      <label for="toDate">To: </label>'."\r\n";
        $formFilter .= '      <input id="toDate" type="text" value="'.$toDate.'" name="toDate">'."\r\n";
        
        if($this->_ei->isAdmin() || $this->_ei->isReporter())
        {
            $formFilter .= '      <label for="EmployeeID" id="forEmployeeID">For Employee: </label>'."\r\n";
            $formFilter .= '      <select id="EmployeeID" name="eid">'."\r\n";
            $formFilter .= '        '.$employeeSelect."\r\n";
            $formFilter .= '      </select>'."\r\n";
        }
        
        $formFilter .= '      <input type="submit" value="Update Filter">'."\r\n";        
        $formFilter .= '    </form>'."\r\n";
        $formFilter .= '  </div>'."\r\n";

        $formFilter .= '  <div id="totalHours"><strong>Total hours: ' . $totalHours . '</strong></div>'."\r\n";
        $formFilter .= '</div>'."\r\n";


        $html='';
        $html .= $formFilter;
        $html .= $tableRowHeader;
        $html .= $tableRowsSection;
        $html .= $tableRowFooter;
        
        return $html;
    }

    
    public function convertMinutesToHours($minutes)
    {
        $decimalTime = (float) ($minutes / 60);
        $decimalTime = number_format($decimalTime,2);

        list ($hr, $min) = explode('.', $decimalTime);
        if($min == 0)
        {
            $totalHours = $hr . ':00';
        } else {
            $min = str_pad($min, 2, "0", STR_PAD_RIGHT);
            $newMin = round(60 / (100 / $min));
            $newMin = str_pad($newMin, 2, "0", STR_PAD_LEFT);
            $totalHours = $hr . ':' . $newMin;
        }

        return $totalHours;
    }
    
    public function getEmployeeInfoFromTimeSheet($timeSheetID)
    {
        $EmployeeID='';
        
        $timesheetMapper = new Application_Model_TimesheetMapper();
        $tswhere = 'TimeSheetID = ' . $timeSheetID;
        $timesheets = $timesheetMapper->fetchAll($tswhere);
        if(count($timesheets) > 0)
        {
            $EmployeeID = $timesheets[0]->getEmployeeID();
            $employeeMapper = new Application_Model_EmployeeMapper();
            $employee = new Application_Model_Employee();
            return $employeeMapper->find($EmployeeID, $employee);
        }
    }


    
    
    
    /** Create arrays that Zend uses to populate select boxes. */
    function getClientSelectByEmployeeID($EmployeeID)
    {
        // If for currently logged in user, get client list from employee info object
        if($this->_ei->getEmployeeID() == $EmployeeID)
        {
            $clientsById = $this->_ei->getClients();
            echo '<!-- ' ;
            print_r($clientsById);
            echo '-->';
            
            if(isset($clientsById['ByClientID']) && count($clientsById['ByClientID']) > 0)
            {
                $clientsSelect = array('' => 'Select Client...');
                
                foreach($clientsById['ByClientID'] as $clientDetails)
                { 
                    $clientsSelect[$clientDetails['ClientID']] = $clientDetails['Name'];
                }
            } else {
                $clientsSelect = array('' => 'No clients available.');
            }
            
        } else {
            
            // First, get categories assigned to use
            $excMapper = new Application_Model_EmployeeCategoriesMapper();
            $where = 'EmployeeID = ' . $EmployeeID;
            $excs = $excMapper->fetchAll($where);
            if(count($excs)==0)
            {
                $clientsSelect = array('' => 'No clients available.');
            } else {

                $projIDs = '';
                foreach($excs as $exc)
                {
                    $projIDs .= $exc->getCategoryID() . ',';
                }
                $projIDs = substr($projIDs,0,-1);
                
                // Now get clientIDs from categories table
                $categoryMapper = new Application_Model_CategoryMapper();
                $where = 'CategoryID IN (' . $projIDs . ')';
                $categories = $categoryMapper->fetchAll($where);
                if(count($categories) == 0)
                {
                    $clientsSelect = array('' => 'No clients available.');
                } else {
                    
                    
                    // Now get client name for each clientID
                    $clientIDs = array();
                    foreach($categories as $category)
                    {
                        // Unique clientIDs
                        if(!in_array($category->getClientID(), $clientIDs))
                        {
                            $clientIDs[] = $category->getClientID();
                        }
                    }

                    $clientMapper = new Application_Model_ClientMapper();
                    
                    
                    foreach($clientIDs as $id)
                    {
                        $client = new Application_Model_Client();
                        $clientMapper->find($id, $client);
                        $clientsSelect[$client->getClientID()] = $client->getName();
                    }
                }
            }
        }
        return $clientsSelect;
    }
    
    function getCategorySelectByClientId($ClientID,$EmployeeID=null)
    {

        if(is_null($EmployeeID))
        {
            $this->_ei = $this->_helper->EmployeeInfo->getObject();
            $categoriesByID = $this->_ei->getCategories();
        } else {
            $eMapper = new Application_Model_EmployeeMapper();
            $employee = new Application_Model_Employee();
            $eMapper->find($EmployeeID,$employee);
            $empData = new stdClass();
            $empData->EmployeeID = $employee->getEmployeeID();
            $empData->Name = $employee->getName();
            $empData->DepartmentID = $employee->getDepartmentID();
            $empData->Username = $employee->getUsername();
            $empData->Password = $employee->getPassword();
            $empData->IsAdmin = $employee->getIsAdmin();
            $empData->IsReporter = $employee->getIsReporter();
            $empData->Active = $employee->getActive();
            $empEIObject = $this->_helper->EmployeeInfo->createObject($empData);            
            $categoriesByID = $empEIObject->getCategories();
        }
        
        if(isset($categoriesByID['ByClientID']) && count($categoriesByID['ByClientID']) > 0)
        {
            $categoriesSelect = array('' => 'Select a category here...');
            foreach($categoriesByID['ByClientID'][$ClientID] as $categoryDetails)
            { 
                $categoriesSelect[$categoryDetails['CategoryID']] = $categoryDetails['Name'];
            }            
        } else {
            $categoriesSelect = array('' => 'No categories available.');
        }
        return $categoriesSelect;
    }
    
    function getTaskSelectByCategoryID($CategoryID)
    {

        $cxtMapper = new Application_Model_CategoryTasksMapper();
        $where = 'CategoryID = ' . $CategoryID;
        $cxts = $cxtMapper->fetchAll($where);
        if(count($cxts) > 0)
        {
            $taskSelect = array('' => 'Select a task here...');
            foreach($cxts as $cxt)
            {
                $taskSelect[$cxt->getCategoriesXTasksID()] = 
                    $this->_helper->TimeSheet->getTaskInfoById($cxt->getTaskID())->getName();
            }
        } else {
            $taskSelect = array('' => 'No tasks available.');
        }
        return $taskSelect;
    }

    function fixDateForTimeSheetInsert($formDate)
    {
        return date("Y-m-d", strtotime($formDate));
    }
    
}
