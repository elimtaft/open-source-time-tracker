<?php

/******************************************************************************
 * Class name: Auth Controller
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      This controller handles authentication, providing a login form and a logout action
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';

class AuthController extends Zend_Controller_Action
{
    protected $_baseURI;
    protected $_ei;
    
    public function init()
    {
        $this->_baseURI = $this->view->baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
    }
    
    public function indexAction()
    {
        
        // Redirect if already logged in
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()) {
            // @codeCoverageIgnoreStart
            $this->_helper->EmployeeInfo->createObject();
            $this->_ei = Zend_Registry::get('EmployeeInfo');
            if($this->_ei->isAdmin())
            {
                return $this->_redirect('/admin');
            } else {
                return $this->_redirect('/');
            }
            // @codeCoverageIgnoreEnd
        }        
        
        $errors = array();
        
        $this->view->title = 'Please login below.';
        
        $action = $this->_baseURI . '/auth';
        $mode = 'auth-log-in';
        $form = $this->view->form = $this->_helper->Form->getForm($action, $mode);
        
        if ($this->getRequest()->isPost())
        { 
            if ($form->isValid($_POST))
            {

                $values = $form->getValues();
                $uname = $values['username'];
                $paswd = $values['password'];
                $form->getElement('username')->setValue($uname);
                $auth = Zend_Auth::getInstance();
                $authAdapter = new Zend_Auth_Adapter_DbTable();
                $authAdapter->setTableName('employees')
                       ->setIdentityColumn('Username')
                       ->setCredentialColumn('Password');    
                $authAdapter->setIdentity($uname);
                $authAdapter->setCredential($this->_helper->Password->hashItOut($uname,$paswd));

                try {
                    $result = $auth->authenticate($authAdapter);
                } catch(Exception $e) {
                    if($e->getCode()==1045)
                    {
                        $errors[] = 'The following error occured:<br>' . $e->getMessage() . '.  
                            This usually occurs when the MySQL database has not been set up.  
                            You need to create a MySQL Database, create a MySQL user
                            with read and write permissions on this database, and then 
                            enter this information into the file located at 
                            /application/configs/application.ini.';
                    } else {
                        $errors[] = 'The following error occured:<br>' . $e->getMessage();
                    }
                }
                

                if(isset($result))
                {
                    if($result->isValid())
                    {
                        $data = $authAdapter->getResultRowObject(null,'password');
                    	if($data->Active != 1)
                    	{
	                        $errors = array('This account has been disabled.');;
                    	} else {
	                        $auth->getStorage()->write($data);
	                        $this->_helper->EmployeeInfo->createObject();
	                        $this->_ei = Zend_Registry::get('EmployeeInfo');
	
	                        if($this->_ei->isAdmin())
	                        {
	                            $this->_redirect('/admin/');
	                        } else {
	                            $this->_redirect ('/timesheet/new-entry');
	                        }
						}
                    } else {
                    
                        $errors = $result->getMessages();
                        $form->getElement('username')->setValue($uname);
                    }
                }
            } else {
                $errors = $this->_helper->Form->getCustomErrors($form);
            }
        }
        if(count($errors) > 0) $this->view->errors = $errors;
    }
    
    function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        return $this->_redirect("/index/");
    }

}
