<?php

/******************************************************************************
 * Class name: Zend_Controller_Action_Helper_TimeSheet
 * Author: Matthew Taft
 * Date: 03/22/2012
 * Description:
 * 
 *      Functions needed in many controllers regarding timesheet data
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Zend_Controller_Action_Helper_TimeSheet extends
                Zend_Controller_Action_Helper_Abstract
{
    
    
    public function getEmployeeInfoById($EmployeeID)
    {
        $employeeMapper = new Application_Model_EmployeeMapper();
        $employee = new Application_Model_Employee();
        $employeeMapper->find($EmployeeID,$employee);
        return $employee;        
    }
    
    public function getDepartmentInfoByID($departmentID)
    {
        $departmentMapper = new Application_Model_DepartmentMapper();
        $department = new Application_Model_Department();
        $departmentMapper->find($departmentID, $department);
        return $department;

    }

    public function getClientInfoByID($ID)
    {
        $clientMapper = new Application_Model_ClientMapper();
        $client = new Application_Model_Client();
        $clientMapper->find($ID,$client);
        return $client;
    }
    
    public function getCategoryInfoByID($ID)
    {
        $categoryMapper = new Application_Model_CategoryMapper();
        $category = new Application_Model_Category();
        $categoryMapper->find($ID,$category);
        return $category;        
    }
    
    public function getTaskInfoById($id)
    {
        $taskMapper = new Application_Model_TaskMapper();
        $task = new Application_Model_Task();
        $taskMapper->find($id, $task);
        return $task;
    }    
    
    function getTimeSheetInfoById($id)
    {
        $tsMapper = new Application_Model_TimesheetMapper();
        $ts = new Application_Model_Timesheet();
        $tsMapper->find($id,$ts);
        return $ts;
    }
}
?>
