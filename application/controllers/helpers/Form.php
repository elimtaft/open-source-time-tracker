<?php

/******************************************************************************
 * Class name: Zend_Controller_Action_Helper_Form
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      Used for common / shared Zend_Form processing logic, such as 
 *      creating forms and generating custom error messages.
 *      
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Zend_Controller_Action_Helper_Form extends
                Zend_Controller_Action_Helper_Abstract
{

    
    public function getForm($action, $mode, $ID = null)
    {
        $form = new Zend_Form();
        $form->setAction($action)
             ->setMethod('post');
        
        switch ($mode)
        {
            case 'auth-log-in':

                $username = $form->createElement('text', 'username');
                $username->setLabel("Username");
                $username->setRequired(true);

                $password = $form->createElement('password', 'password');
                $password->setLabel("Password");
                $password->setRequired(true);

                $form->addElement($username)
                     ->addElement($password)
                     ->addElement('submit', 'login', array('label' => 'Login'));
                
                break;
            
            case 'account-password-change':
                $currentPass = $form->createElement('password', 'currentPassword');
                $currentPass->setLabel('Current Password')
                            ->addValidator('callback', false, array(
                                 'callback' => array($this, 'currentPasswordGood'),
                                 'messages' => array(
                                     Zend_Validate_Callback::INVALID_VALUE => 'Invalid password'
                                    )
                                ))
                            ->setRequired(true);
                
                $newPass = $form->createElement('password', 'newPass');
                $newPass->setLabel('New Password')
                        ->addValidator('stringLength', false, array('min' => '6'))
                        ->setRequired(true);
                
                $newPassRepeat = $form->createElement('password', 'newPassRepeat');
                $newPassRepeat->setLabel('New Password (Repeat)')
                              ->addValidator('identical',false,array(
                                  'token' => 'newPass',
                                  'messages' => array(
                                      Zend_Validate_Identical::NOT_SAME => 'Passwords do not match')
                                ))
                              ->setRequired(true);
                
                $form->addElement($currentPass)
                     ->addElement($newPass)
                     ->addElement($newPassRepeat)
                     ->addElement('submit', 'submit', array('label' => 'Submit'));
                break;
            
            // Employee Forms -------------------------------------------------

            case 'admin-create-employee':
                
                $form->setAttrib('class', 'employee');
                
                $name = $form->createElement('text', 'name');
                $name->setLabel('Full name')
                     ->setRequired(true);
                
                $department = $form->createElement('select','department');
                $department->setLabel('Department')
                           ->setRequired(true)
                           ->setRegisterInArrayValidator(false)
                           ->addMultiOptions($this->getDepartmentSelect());
                
                $username = $form->createElement('text', 'username');
                $username->setLabel('Username')
                         ->addValidator('callback', false, array(
                                 'callback' => array($this, 'usernameUnique'),
                                 'messages' => array(
                                     Zend_Validate_Callback::INVALID_VALUE => 'An employee with this username already exists.'
                                    )
                                ))
                         ->setRequired(true);
                
                
                $PasswordHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('Password');
                $newPasswordUnhashed = $PasswordHelper->generateNew('nameNotRequired');
                $password = $form->createElement('text', 'password');
                $password->setLabel('Password')
                         ->addValidator('stringLength', false, array('min' => '6'))
                         ->setValue($newPasswordUnhashed)
                         ->setRequired(true);
                

                
                $isactive = $form->createElement('select', 'isactive');
                $isactive->setLabel('Status')
                         ->addMultiOptions(array(
                             '1' => 'Active',
                             '0' => 'Inactive'
                         ));

                
                $isreporter = $form->createElement('select', 'isreporter');
                $isreporter->setLabel('Reporter?')
                         ->addMultiOptions(array(
                             '0' => 'No',
                             '1' => 'Yes'
                         ));

                $ei = Zend_Registry::get('EmployeeInfo');
                if($ei->canCreateAdminUsers())
                {
                    $isadmin = $form->createElement('select', 'isadmin');
                    $isadmin->setLabel('Site Admin?')
                             ->addMultiOptions(array(
                                 '0' => 'No',
                                 '1' => 'Yes'
                             ));
                } else {
                    $isadmin = $form->createElement('hidden', 'isadmin');
                    $isadmin->setValue(0)
                            ->setAttrib('style', 'visibility:hidden;display:none;');
                }
                
                $form->addElement($name)
                     ->addElement($department)
                     ->addElement($username)
                     ->addElement($password)
                     ->addElement($isactive)
                     ->addElement($isreporter)
                     ->addElement($isadmin)                    
                     ->addElement('submit', 'submit', array('label' => 'Submit'));

                break;
                
            case 'admin-edit-employee':
                
                if(is_null($ID)) return false;

                $form->setAttrib('class', 'employee');
                
                $employeeMapper = new Application_Model_EmployeeMapper();
                $employee = new Application_Model_Employee();
                $employeeMapper->find($ID,$employee);
                if(!$employee)
                {
                    return false;
                }
                
                $hiddenFlag = $form->createElement('hidden','editEmployee');
                $hiddenFlag->setValue(1);

                $hiddenID = $form->createElement('hidden','EmployeeID');
                $hiddenID->setValue($ID);
                
                $name = $form->createElement('text', 'name');
                $name->setLabel('Full name')
                     ->setValue($employee->getName())
                     ->setRequired(true);
                
                $department = $form->createElement('select','department');
                $department->setLabel('Department')
                           ->setRequired(true)
                           ->setValue($employee->getDepartmentID())
                           ->setRegisterInArrayValidator(false)
                           ->addMultiOptions($this->getDepartmentSelect($ID));
                
                $username = $form->createElement('text', 'username');
                $username->setLabel('Username')
                         ->setValue($employee->getUsername())
                         ->setAttrib('disabled', true);
                
                $password = $form->createElement('password', 'password');
                $password->setLabel('Password')
                         ->setValue('aaaaaaaaaaaaaaaaaaa')
                         ->setAttrib('disabled', true);
                
                $isactive = $form->createElement('select', 'isactive');
                $isactive->setLabel('Status')
                         ->setValue($employee->getActive())
                         ->addMultiOptions(array(
                             '1' => 'Active',
                             '0' => 'Inactive'
                             
                         ));
                
                $isreporter = $form->createElement('select', 'isreporter');
                $isreporter->setLabel('Reporter?')
                           ->setValue($employee->getIsReporter())
                           ->addMultiOptions(array(
                             '0' => 'No',
                             '1' => 'Yes'
                         ));
                
                $ei = Zend_Registry::get('EmployeeInfo');
                if($ei->canCreateAdminUsers())
                {
                    $isadmin = $form->createElement('select', 'isadmin');
                    $isadmin->setLabel('Site Admin?')
                            ->setValue($employee->getIsAdmin())
                             ->addMultiOptions(array(
                                 '0' => 'No',
                                 '1' => 'Yes'
                             ));
                } else {
                    $isadmin = $form->createElement('hidden', 'isadmin');
                    $isadmin->setValue($employee->getIsAdmin())
                            ->setAttrib('style', 'visibility:hidden;display:none;');
                }
                
                
                $form->addElement($hiddenFlag)
                     ->addelement($hiddenID)
                     ->addElement($name)
                     ->addElement($department)
                     ->addElement($username)
                     ->addElement($password)
                     ->addElement($isactive)
                     ->addElement($isreporter)
                     ->addElement($isadmin)
                     ->addElement('submit', 'submit', array('label' => 'Submit'));
                
                break;    
                

            // Department Forms ------------------------------------------------               
            case 'admin-create-department':
                
                $form->setAttrib('class', 'department');
                
                $name = $form->createElement('text', 'departmentName');
                $name->setLabel('Department Name:')
                     ->addValidator('callback', false, array(
                             'callback' => array($this, 'departmentUnique'),
                             'messages' => array(
                                 Zend_Validate_Callback::INVALID_VALUE => 'An department with this name already exists.'
                                )
                            ))
                     ->setRequired(true);
                
                $departmentID = $form->createElement('text', 'departmentID');
                $departmentID->setAttrib('style', 'display:none;visibility:hidden;');
                
                $form->addElement($name)->addElement($departmentID)
                     ->addElement('submit', 'departmentCreateSubmit', array('label' => 'Submit'));
                
                break;
            
            case 'admin-edit-department':
                
                if(is_null($ID)) return false;

                $departmentMapper = new Application_Model_DepartmentMapper();
                $department = new Application_Model_Department();
                $departmentMapper->find($ID, $department);
                
                $form->setAttrib('class', 'department');
                
                $name = $form->createElement('text', 'departmentName');
                $name->setLabel('Department Name:')
                     ->setValue($department->getName())
                     ->setAttrib('width', '500')
                     ->addValidator('callback', false, array(
                             'callback' => array($this, 'departmentUnique'),
                             'options' => array('currentDepartmentName' => $department->getName()),
                             'messages' => array(
                                 Zend_Validate_Callback::INVALID_VALUE => 'An department with this name already exists.'
                                )
                            ))
                     ->setRequired(true);
                $form->addElement($name)
                     ->addElement('submit', 'departmentCreateSubmit', array('label' => 'Submit'));
                
                break;   

            // Client Forms ----------------------------------------------------
            case 'admin-create-client':
                
                $form->setAttrib('class', 'client');
                
                $name = $form->createElement('text', 'clientName');
                $name->setLabel('Name:')
                     ->addValidator('callback', false, array(
                             'callback' => array($this, 'clientNameUnique'),
                             'messages' => array(
                                 Zend_Validate_Callback::INVALID_VALUE => 'A client with this name already exists.'
                                )
                            ))
                     ->setRequired(true);
                
                $city = $form->createElement('text', 'clientCity');
                $city->setLabel('City:')
                     ->setRequired(true);

                $state = $form->createElement('text', 'clientState');
                $state->setLabel('State:')
                      ->addValidator('stringLength', false, array('min' => '2', 'max' => '2'))
                      ->setRequired(true);

                
                $form->addElement($name)
                     ->addElement($city)
                     ->addElement($state)
                     ->addElement('submit', 'clientCreateSubmit', array('label' => 'Submit'));
                
                break;
            
            case 'admin-edit-client' :

                if(is_null($ID)) return false;

                $clientMapper = new Application_Model_ClientMapper();
                $client = new Application_Model_Client();
                $clientMapper->find($ID,$client);
                
                $form->setAttrib('class', 'client');
                
                $name = $form->createElement('text', 'clientName');
                $name->setLabel('Name:')
                     ->setValue($client->getName())
                     ->setAttrib('width', '500')
                     ->addValidator('callback', false, array(
                             'callback' => array($this, 'clientNameUnique'),
                             'options' => array('currentClientName' => $client->getName()),
                             'messages' => array(
                                 Zend_Validate_Callback::INVALID_VALUE => 'A client with this name already exists.'
                                )
                            ))
                     ->setRequired(true);

                $city = $form->createElement('text', 'clientCity');
                $city->setLabel('City:')
                     ->setValue($client->getCity())
                     ->setRequired(true);

                $state = $form->createElement('text', 'clientState');
                $state->setLabel('State:')
                      ->setValue($client->getState())
                      ->addValidator('stringLength', false, array('min' => '2', 'max' => '2'))
                      ->setRequired(true);
                
                $form->addElement($name)
                     ->addElement($city)
                     ->addElement($state)
                     ->addElement('submit', 'clientCreateSubmit', array('label' => 'Submit'));
                

                break;

            // Category Forms  --------------------------------------------------               
            case 'admin-create-category':
                
                $form->setAttrib('class', 'category');
                
                $clientSelect = $this->getClientSelect();
                $client = $form->createElement('select', 'categoryForClient');
                $client->setLabel('For Client')
                       ->setRequired(true)
                       ->addMultiOptions($clientSelect);
                
                $name = $form->createElement('text', 'categoryName');
                $name->setLabel('Name:')
                     ->setRequired(true);
                
                $form->addElement($client)
                     ->addElement($name)
                     ->addElement('submit', 'categoryCreateSubmit', array('label' => 'Submit'));
                
                break;
            
            case 'admin-edit-category' :

                if(is_null($ID)) return false;
                
                $categoryMapper = new Application_Model_CategoryMapper();
                $category = new Application_Model_Category();
                $categoryMapper->find($ID,$category);
                
                $form->setAttrib('class', 'category');
                
                $clientSelect = $this->getClientSelect();
                $client = $form->createElement('select', 'categoryForClient');
                $client->setLabel('For Client')
                         ->addMultiOptions($clientSelect)
                         ->setValue($category->getClientID());
                
                $name = $form->createElement('text', 'categoryName');
                $name->setLabel('Name:')
                     ->setValue($category->getName())
                     ->setRequired(true);
                
                $form->addElement($client)
                     ->addElement($name)
                     ->addElement('submit', 'categoryCreateSubmit', array('label' => 'Submit'));

                break;

            case 'admin-create-category-for-all-clients':
                
                $form->setAttrib('class', 'category');
                
                $name = $form->createElement('text', 'categoryName');
                $name->setLabel('Name:')
                     ->setRequired(true);
                
                $form->addElement($name)
                     ->addElement('submit', 'categoryCreateSubmit', array('label' => 'Submit'));
                
                break;
            
            // Task Forms ------------------------------------------------------
            case 'admin-create-task':
                
                $form->setAttrib('class', 'task');
                
                $name = $form->createElement('text', 'taskName');
                $name->setLabel('Task Name:')
                     ->addValidator('callback', false, array(
                             'callback' => array($this, 'taskUnique'),
                             'messages' => array(
                                 Zend_Validate_Callback::INVALID_VALUE => 'A task with this name already exists.'
                                )
                            ))
                     ->setRequired(true);
                
                $form->addElement($name)
                     ->addElement('submit', 'taskCreateSubmit', array('label' => 'Submit'));
                
                break;
            
            case 'admin-edit-task':
                
                if(is_null($ID)) return false;
                
                $taskMapper = new Application_Model_TaskMapper();
                $task = new Application_Model_Task();
                $taskMapper->find($ID, $task);
                
                $form->setAttrib('class', 'task');
                
                $name = $form->createElement('text', 'taskName');
                $name->setLabel('Task Name:')
                     ->setValue($task->getName())
                     ->setAttrib('width', '500')
                     ->addValidator('callback', false, array(
                             'callback' => array($this, 'taskUnique'),
                             'options' => array('currentTaskName' => $task->getName()),
                             'messages' => array(
                                 Zend_Validate_Callback::INVALID_VALUE => 'An task with this name already exists.'
                                )
                            ))
                     ->setRequired(true);

                $form->addElement($name)
                     ->addElement('submit', 'taskCreateSubmit', array('label' => 'Submit'));
                
                break;   
           
        }
        
        return $form;
    }
    
    

    
    /**************************************************************************
     * 
     * Custom Form Validation Callback Functions
     * 
     *************************************************************************/
    
    public function currentPasswordGood($currentPassword)
    {
            $ei = Zend_Registry::get('EmployeeInfo');
            $PasswordHelper = 
                Zend_Controller_Action_HelperBroker::getStaticHelper('Password');
            

            return $PasswordHelper->hashItOut(
                $ei->getUsername(),
                $currentPassword) == $ei->getPassword();
    }        

    public function usernameUnique($username)
    {
        $empMapper = new Application_Model_EmployeeMapper();
        $empWhere = "Username = '" . $username . "'";
        $emps = $empMapper->fetchAll($empWhere);
        if(count($emps) > 0) return false; 
        else return true;
    }

    
    public function departmentUnique($departmentName, $options = null, $currentDepartmentName = null)
    {
        if(isset($currentDepartmentName))
        {
            if($currentDepartmentName==$departmentName)
            {
                return true;   // They aren't changing the name
            }
        }
        $dptMapper = new Application_Model_DepartmentMapper();
        $dptWhere = "Name = '" . $departmentName . "'";
        $dpts = $dptMapper->fetchAll($dptWhere);
        if(count($dpts) > 0) return false; 
        else return true;        
    }
    
    public function taskUnique($taskName, $options = null, $currentTaskName = null)
    {
        if(isset($currentTaskName))
        {
            if($currentTaskName==$taskName)
            {
                return true; // They aren't changing the name
            }
        }
        $taskMapper = new Application_Model_TaskMapper();
        $where = "Name = '" . $taskName . "'";
        $tasks = $taskMapper->fetchAll($where);
        if(count($tasks) > 0) return false; 
        else return true;
    }
    
    public function clientNameUnique($clientName, $options = null, $currentClientName = null)
    {
        if(isset($currentClientName))
        {
            if($currentClientName==$clientName) // They aren't changing the name
            {
                return true;
            }
        }
        $clientMapper = new Application_Model_ClientMapper();
        $clientWhere = "Name = '" . $clientName . "'";
        $clients = $clientMapper->fetchAll($clientWhere);
        if(count($clients) > 0)
        {
            return false; 
        } else {
            return true;
        }
        
    }
    
    /**************************************************************************
     * 
     * Custom Errors
     * 
     *************************************************************************/
    
    public function getCustomErrors($form)
    {
        $errors = $form->getErrors();
        $messageConverArray = $this->getCustomErrorsArray();
        $newMessages = array('<strong>Please note: The following errors occured:</strong>' . '<br />');
        foreach($errors as $name => $val)
        {
            if(count($val) > 0 && $name != 'submit')
            {
                $form->getElement($name)->getDecorator('label')->setOption('style', 'color: #ff0000;font-weight:bold;');
                $form->getElement($name)->removeDecorator('errors');
            }

            if(is_array($val))
            {
                foreach($val as $name1 => $val1)
                {
                    if(isset($messageConverArray[$name][$val1]))
                    {
                        $newMessages[] = $messageConverArray[$name][$val1]['fieldname'] . ': ' . $messageConverArray[$name][$val1]['message'] . "<br />";
                    } else {
                        $newMessages[] = $name . ': ' . $val1 . "<br />";
                    }
                }
            }
        }

        return $newMessages;
    }
    
    
    public function getCustomErrorsArray()
    {
        return array (
            'name' => array (
                'isEmpty' => array (
                    'fieldname' => 'Full name',
                    'message' => 'Cannot be blank'
                )
            ),
            'department' => array (
                'isEmpty' => array (
                    'fieldname' => 'Department',
                    'message' => 'Cannot be blank'
                )
            ),                
            'username' => array (
                'callbackValue' => array (
                    'fieldname' => 'Username',
                    'message' => 'Already in use'
                ),
                'isEmpty' => array (
                    'fieldname' => 'Username',
                    'message' => 'Cannot be blank'
                )
            ),
            'password' => array(
                'notSame'   => array (
                    'fieldname' => 'New Password (Repeat)',
                    'message' => 'You must enter the same new password twice.'
                ),
                'isEmpty' => array (
                    'fieldname' => 'Password',
                    'message' => 'Cannot be blank'
                ),
                'stringLengthTooShort' => array (
                    'fieldname' => 'Password',
                    'message' => 'Must be at least 6 characters'
                )
            ),
            'currentPassword' => array (
                'callbackValue' => array (
                    'fieldname' => 'Current Password',
                    'message' => 'This is not your current password.'
                ),
                'isEmpty' => array (
                    'fieldname' => 'Current Password',
                    'message' => 'Cannot be blank'
                )
            ),
            'newPass' => array (
                'stringLengthTooShort' => array (
                    'fieldname' => 'New Password',
                    'message' => 'Password must be at least 6 characters'
                ),
                'isEmpty' => array (
                    'fieldname' => 'New Password',
                    'message' => 'Cannot be blank'
                )
            ),
            'newPassRepeat' => array(
                'notSame'   => array (
                    'fieldname' => 'New Password (Repeat)',
                    'message' => 'You must enter the same new password twice.'
                ),
                'isEmpty' => array (
                    'fieldname' => 'New Password (Repeat)',
                    'message' => 'Cannot be blank'
                )
            ),
            'client' => array(
                'isEmpty' => array (
                    'fieldname' => 'Client',
                    'message' => 'Cannot be blank'
                )
            ),
            'category' => array(
                'isEmpty' => array (
                    'fieldname' => 'Category',
                    'message' => 'Cannot be blank'
                )
            ),
            'task' => array(
                'isEmpty' => array (
                    'fieldname' => 'Task',
                    'message' => 'Cannot be blank'
                )
            ),
            'date' => array(
                'isEmpty' => array (
                    'fieldname' => 'Date',
                    'message' => 'Cannot be blank'
                )
            ),
            'duration' => array(
                'isEmpty' => array (
                    'fieldname' => 'Duration',
                    'message' => 'Cannot be blank'
                ),
                'callbackValue' => array (
                    'fieldname' => 'Duration',
                    'message' => 'Please use the format hh:mm (example: 00:15, 05:00, 01:30, etc.)'
                )
            ),
            'comments' => array(
                'isEmpty' => array (
                    'fieldname' => 'Comments',
                    'message' => 'Cannot be blank'
                )
            ),
            'departmentName' => array(
                'isEmpty' => array (
                    'fieldname' => 'Department Name',
                    'message' => 'Cannot be blank'                    
                ),
                'callbackValue' => array (
                    'fieldname' => 'Department Name',
                    'message' => 'A department with this name already exists.'
                ),
            ),
            'clientName' => array(
                'isEmpty' => array (
                    'fieldname' => 'Name',
                    'message' => 'Cannot be blank'                    
                ),
                'callbackValue' => array (
                    'fieldname' => 'Client Name',
                    'message' => 'A client with this name already exists.'
                ),
            ),
            'clientCity' => array(
                'isEmpty' => array (
                    'fieldname' => 'City',
                    'message' => 'Cannot be blank'                    
                )
            ),
            'clientState' => array(
                'isEmpty' => array (
                    'fieldname' => 'State',
                    'message' => 'Cannot be blank'                    
                ),
                'stringLengthTooShort' => array (
                    'fieldname' => 'State',
                    'message' => 'Must be 2 characters, like NC'
                ),
                'stringLengthTooLong' => array (
                    'fieldname' => 'State',
                    'message' => 'Must be 2 characters, like NC'
                ),
            ),
            'categoryName' => array(
                'isEmpty' => array (
                    'fieldname' => 'Category Name',
                    'message' => 'Cannot be blank'                    
                )
            ),
            'categoryForClient' => array(
                'isEmpty' => array (
                    'fieldname' => 'For Client',
                    'message' => 'Cannot be blank'
                ),
                'notInArray' => array (
                    'fieldname' => 'For Client',
                    'message' => 'Client doesn\'t exist'
                )

            ),
            'categoryDescription' => array(
                'isEmpty' => array (
                    'fieldname' => 'Description',
                    'message' => 'Cannot be blank'                    
                )
            ),
            'taskName' => array(
                'isEmpty' => array (
                    'fieldname' => 'Task Name',
                    'message' => 'Cannot be blank'                    
                ),
                'callbackValue' => array (
                    'fieldname' => 'Task Name',
                    'message' => 'A task with this name already exists.'
                ),
            ),
            'taskDescription' => array(
                'isEmpty' => array (
                    'fieldname' => 'Description',
                    'message' => 'Cannot be blank'                    
                )
            ),
        );
    }
    
    
    /**************************************************************************
     * 
     * Object to Array (Key => Val) functions
     *      Inserted into select elements of Zend_Form for drop-down lists
     * 
     **************************************************************************/
    
    public function getDepartmentSelect($editEmpID = null)
    {
        $dontDisplayThisDepartmentID = array();
        
        $ei = Zend_Registry::get('EmployeeInfo');
        
        if($ei->getCreateUsers_MustBeInSpecificDepartment())
        {
            if($ei->getDepartmentID() != $ei->getCreateUsers_MustBeInSpecificDepartmentID())
            {
                $dontDisplayThisDepartmentID[] = $ei->getCreateUsers_MustBeInSpecificDepartmentID();
            }
        }
        
        if(!is_null($editEmpID))
        {
            $empMapper = new Application_Model_EmployeeMapper();
            $emp = new Application_Model_Employee();
            $empMapper->find($editEmpID, $emp);
            if($emp)
            {
                if($emp->isAdmin)
                {
                    if($ei->getCreateAdminUsers_MustBeInSpecificDepartment())
                    {
                        if($ei->getDepartmentID() != $ei->getCreateAdminUsers_MustBeInSpecificDepartmentID())
                        {
                            $dontDisplayThisDepartmentID[] = $ei->getCreateAdminUsers_MustBeInSpecificDepartmentID();
                        }
                    }
                }
            }
        }
        $departmentMapper = new Application_Model_DepartmentMapper();
        $orderBy = 'Name';
        $departments = $departmentMapper->fetchAll(null,$orderBy);
        $departmentSelect = array();
        if(count($departments) > 0)
        {
            foreach($departments as $dpt)
            {
                if(!in_array($dpt->getDepartmentID(), $dontDisplayThisDepartmentID))
                {
                    $departmentSelect[$dpt->getDepartmentID()] = $dpt->getName();
                }
            }
        }
        return $departmentSelect;
    }
    
    public function getClientSelect()
    {
        $clientMapper = new Application_Model_ClientMapper();
        $orderBy = 'Name';
        $clients = $clientMapper->fetchAll(null, $orderBy);
        $clientSelect = array();
        if(count($clients) > 0)
        {
            foreach($clients as $client)
            {
                $clientSelect[$client->getClientID()] = $client->getName();
            }
        }
        return $clientSelect;
    }    
}
?>
