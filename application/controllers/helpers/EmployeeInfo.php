<?php
/******************************************************************************
 * Class name: Zend_Controller_Action_Helper_EmployeeInfo
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      Creates an employee object from the user's login credentials.
 *      The idea behind this is that the database is highly normalized, but
 *      this can also cause some overhead in PHP code to get to certain kinds of
 *      data (like which tasks belong to an employee.  With this action helper,
 *      once the user is logged in this object is created with common functions
 *      such as isAdmin() or getTimeSheetEntryIDs() or getTasks().
 * 
 *      As far as how this object is created: This class simply queries the database
 *      for the information and stores it in an array with a particular format.
 *      This array is then passed to an instance of Application_Model_EmployeeInfo, which 
 *      is then placed in the global scope.
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Zend_Controller_Action_Helper_EmployeeInfo extends
                Zend_Controller_Action_Helper_Abstract
{
    
    
    

    
    /**
     *
     * function createObject() 
     * 
     * @return Application_Model_EmployeeInfo 
     * 
     * I suppose a brief explanation could be helpful here as to what in the world
     * this does.  
     * 
     * This function basically gathers lots of data relevant to a user when they log in.  
     * 
     * When the user logs in, some data is already stored in a persistent session by
     * Zend_Auth (see indexAction() in application/AuthController).  It basically
     * grabs everything from the employees table and stores it in a session.  However, there is 
     * other information not in the employees table that we'd like to have access to
     * without having to query the database every time.  So we get this additional data,
     * pack it together with the data from Zend_Auth, and put it all into an array...
     * 
     * I've also created a custom model that doesn't map directly to a specific database table.  The model is called
     * Application_Model_EmployeeInfo.  When I instantiate this model (toward the bottom
     * of this function), I "inject" it with this array.  The model's constructor 
     * receives the array and attempts to call functions named after the variable
     * element names.  This might sound confusing, but further explanation here would be overkill.
     * Just a short example: One of the elements of the array is $eiArray['EmployeeID'].  When this array
     * is passed into the constructor of that Application_Model_EmployeeInfo model, 
     * the constructor searches through the elements of the array.  When it comes to
     * "EmployeeID," it searches itself for a function called "setEmployeeID."  If found,
     * it calls that function, passing the value from the array element.  So every top-level element
     * of the $eiArray should have a corresponding get and set function in the
     * Application_Model_EmployeeInfo model.
     * 
     * Most of the data stored below is self explanatory, but some stuff might need
     * explanation:
     * 
     * I won't go into all the details here of how the database is structured, but
     * employees are assigned to categories which belong to clients, and tasks
     * are also assigned to categories.  In a few places in the website, the
     * client -> category -> task and employee -> category -> task associations
     * are important, but it usually requires some MySQL joins or sub queries to
     * get all the relevant data.  For example, when a user enters time, they 
     * select the client, then the category, then the task.  We only show the clients
     * relevant to the user, but users aren't assigned to clients; they are assigned
     * to categories which belong to clients, so we first query the employeesxcategories
     * table, get all the categories to which the employee has been assigned, 
     * loop through them to get all the clients owning those categories, and also
     * loop through them to get all the tasks associated with them.  Instead of
     * having to do this nightmare over and over again in the site, we do it
     * once here.  It is stored in a couple of ways for various needs:
     * 
     * $eiArray['CCT']
     *  this is used in /timesheet/new-entry.  Data is sorted by client
     * -> category -> task, so when the user selects client, it populates the
     * category drop-down with all the selected client's categories.  When the
     * user selects a category, the tasks drop-down gets automatically populated.
     * 
     * The following array elements store data in ways that can be helpful:
     * 
     * $eiArray['Categories']['ByClientID']
     * 
     * This holds an array of categories for each client (only the categores to which
     * the user has been assigned)
     * 
     * $eiArray['Categories']['ByCategoryID']
     * 
     * An array of categories by category ID.  This will get all categories the
     * user has been assigned to, regardless of which clients owns them.
     * 
     * $eiArray['Clients']['ByClientID']
     * 
     * An array of all clients the employee has been associated with (they are
     * associated with a client simply when they are assigned to a category owned
     * by that client)
     * 
     * $eiArray['Tasks']['ByCategoryID']
     * 
     * An array of tasks by category ID
     * 
     * $eiArray['Tasks']['rawIDs']
     * 
     * An array of ALL task IDs, regardless of which category they belong to.
     * 
     */
    public function createObject($authData=null) {
        
        $eiArray = array();
        $storeGlobally=false;
        
        if(is_null($authData))
        {
            $storeGlobally=true;
            $authData = Zend_Auth::getInstance()->getStorage()->read();
        }
        
        $eiArray['EmployeeID'] = $authData->EmployeeID;
        $eiArray['Name'] = $authData->Name;
        $eiArray['DepartmentID'] = $authData->DepartmentID;
        $dptMapper = new Application_Model_DepartmentMapper();
        $department = new Application_Model_Department();
        $dpt = $dptMapper->find($eiArray['DepartmentID'], $department);
        $eiArray['DepartmentName'] = $dpt->Name;
        $eiArray['Username'] = $authData->Username;
        $eiArray['Password'] = $authData->Password;
        $eiArray['IsAdmin'] = $authData->IsAdmin;
        $eiArray['IsReporter'] = $authData->IsReporter;
        $eiArray['Active'] = $authData->Active;          

        
        
        $config = Zend_Registry::get('config');
        $eiArray['CreateUsers_MustBeAdmin']=$config->timesheet->createusers->mustbeadmin;
        $eiArray['CreateUsers_MustBeInSpecificDepartment']=$config->timesheet->createusers->mustbeinspecificdepartment;
        $eiArray['CreateUsers_MustBeInSpecificDepartmentID']=$config->timesheet->createusers->mustbeinspecificdepartmentid;
        $eiArray['CreateAdminUsers_MustBeAdmin']=$config->timesheet->createadminusers->mustbeadmin;
        $eiArray['CreateAdminUsers_MustBeInSpecificDepartment']=$config->timesheet->createadminusers->mustbeinspecificdepartment;
        $eiArray['CreateAdminUsers_MustBeInSpecificDepartmentID']=$config->timesheet->createadminusers->mustbeadmin;
        
        
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
        $select = $db->select()
                 ->from(
                        array(
                            'exc' => 'employeesxcategories'
                        ),
                        array(
                            'CategoryID'       => 'exc.CategoryID'
                        )
                    )
                ->joinLeft(
                        array(
                            'cat' => 'categories'
                        ),
                        'exc.CategoryID = cat.CategoryID AND exc.EmployeeID = ' . $eiArray['EmployeeID'],
                        array(
                            'CategoryName' => 'cat.Name'
                        )
                    )
                ->joinLeft(
                        array(
                            'cl' => 'clients'
                        ),
                        'cat.ClientID = cl.ClientID',
                        array(
                            'ClientID' => 'cl.ClientID',
                            'ClientName' => 'cl.Name'
                        )
                    )
                ->joinLeft(
                        array(
                            'cxt' => 'categoriesxtasks'
                        ),
                        'exc.CategoryID = cxt.CategoryID',
                        array(
                            'CXTID' => 'cxt.CategoriesXTasksID'
                        )
                    )
                ->joinLeft(
                        array(
                            't' => 'tasks'
                        ),
                        'cxt.TaskID = t.TaskID',
                        array(
                            'TaskID' => 't.TaskID',
                            'TaskName' => 't.Name'
                        )
                    )
            ->where('exc.EmployeeID = ' . $eiArray['EmployeeID']);

        $statement = $db->query($select);
        $results = $statement->fetchAll();   
        
        $catIDs = array();
        if($results) {
            foreach($results as $row)
            {
                if(!in_array($row['CategoryID'], $catIDs))
                {
                    $catIDs[] = $row['CategoryID'];
                    $eiArray['Categories']['ByClientID'][$row['ClientID']][] = array(
                        'ClientID' => $row['ClientID'],
                        'CategoryID' => $row['CategoryID'],
                        'Name' => $row['CategoryName']
                    );

                    $eiArray['Categories']['ByCategoryID'][$row['CategoryID']] = array (
                        'ClientID' => $row['ClientID'],
                        'CategoryID' => $row['CategoryID'],
                        'Name' => $row['CategoryName']
                    );                    
                }

                if($row['ClientID'])
                {
                    $eiArray['Clients']['ByClientID'][$row['ClientID']] = array(
                        'ClientID' => $row['ClientID'],
                        'Name' => $row['ClientName']
                    );
                    $eiArray['CCT']['clients'][$row['ClientID']]['name'] = $row['ClientName'];
                    $eiArray['CCT']['clients'][$row['ClientID']]['categories'][$row['CategoryID']]['name'] = $row['CategoryName'];
                } else {
                    //echo 'oops: ' . $row['CategoryID'];
                }
                
                if($row['CXTID'])
                {
                    $eiArray['Tasks']['ByCategoryID'][$row['CategoryID']][$row['CXTID']] = array (
                        'CategoryID' => $row['CategoryID'],
                        'TaskID' => $row['TaskID'],
                        'Name' => $row['TaskName'],
                        'CategoriesXTasksID' => $row['CXTID']
                        );     

                    $eiArray['Tasks']['rawIDs'][] = $row['CXTID'];
                    $eiArray['CCT']['clients'][$row['ClientID']]['categories'][$row['CategoryID']]['tasks'][$row['CXTID']]['name'] = $row['TaskName'];
                }
            }
        }  

        // Raw list of timesheet entries by employee ID
        $select2 = $db->select()
                 ->from('timesheet','TimeSheetID')
                 ->where('EmployeeID = ' . $eiArray['EmployeeID']);
        $statement2 = $db->query($select2);
        $results2 = $statement2->fetchAll();   
        if($results2) {
            foreach($results2 as $row)
            {
                $eiArray['TimeSheetEntryIDs']['rawIDs'][] = $row['TimeSheetID'];
            }
        }
        
        $eiArray = $this->applySorting($eiArray);
        
        // Inject array into model
        $eiObject = new Application_Model_EmployeeInfo($eiArray);

        // Store model in the global scope
        if($storeGlobally===true)
        {
            Zend_Registry::set('EmployeeInfo',$eiObject);
        }
        
        // Return it too
        return $eiObject;
    }
    
    public function getObject() {
        return Zend_Registry::get('EmployeeInfo');
    }
    
    public function applySorting($eiArray)
    {
        if(isset($eiArray['Clients']))
        {
            $eiArray['Clients'] = $this->sortClients($eiArray['Clients']);
        }
        if(isset($eiArray['Categories']))
        {
            $eiArray['Categories'] = $this->sortClientCategories($eiArray['Categories']);
        }
        
        if(isset($eiArray['Tasks']))
        {
            $eiArray['Tasks'] = $this->sortCategoryTasks($eiArray['Tasks']);
        }
        
        if(isset($eiArray['CCT']))
        {
            $eiArray['CCT'] = $this->sortCCT($eiArray['CCT']);
        }
        
        return $eiArray;
    }
    public function sortClients($array)
    {
        $e1 = array();
        $e2 = array();
        $newClientsArray = array();
        
        foreach($array['ByClientID'] as $key => $row)
        {
            $e1[$key]  = $row['Name'];
            $e2[$key]  = $row['ClientID'];
        }
        asort($e1);
        foreach($e1 as $key => $row)
        {
            $newClientsArray['ByClientID'][$key]['Name'] = $row;
            $newClientsArray['ByClientID'][$key]['ClientID'] = $e2[$key];
        }
        return $newClientsArray;
    }

    public function sortClientCategories($array)
    {return $array;
        $newArray = array();
        foreach($array['ByClientID'] as $key => $row)
        {
            $newArray['ByClientID'][$key] = $this->sortCategories($row);
        }
        $newArray['ByCategoryID'] = $this->sortCategories($array['ByCategoryID']);

        return $newArray;
    }
    
    public function sortCategories($array)
    {
        $e1 = array();
        $e2 = array();
        $e3 = array();
        foreach($array as $key => $row)
        {
            
            $e1[$key]  = $row['Name'];
            $e2[$key]  = $row['CategoryID'];
            $e3[$key]  = $row['ClientID'];
        }
        asort($e1);
        $newArray = array();
        foreach($e1 as $key => $row)
        {
            $newArray[$key]['Name'] = $row;
            $newArray[$key]['CategoryID'] = $e2[$key];
            $newArray[$key]['ClientID'] = $e3[$key];
        }
        return $newArray;
    }    
    
    public function sortCategoryTasks($array)
    {
        $newClientsArray = array();
        foreach($array['ByCategoryID'] as $key => $row)
        {
            $newClientsArray['ByCategoryID'][$key] = $this->sortTasks($row);
        }
        $newClientsArray['rawIDs'] = $array['rawIDs'];
        return $newClientsArray;
    }
    
    public function sortTasks($array)
    { 
        $e1 = array();
        $e2 = array();
        $e3 = array();
        $e4 = array();
        foreach($array as $key => $row)
        {
            $e1[$key]  = $row['Name'];
            $e2[$key]  = $row['CategoryID'];
            $e3[$key]  = $row['TaskID'];
            $e4[$key]  = $row['CategoriesXTasksID'];
        }
        asort($e1);
        $newArray = array();
        foreach($e1 as $key => $row)
        {
            $newArray[$key]['Name'] = $row;
            $newArray[$key]['CategoryID'] = $e2[$key];
            $newArray[$key]['TaskID'] = $e3[$key];
            $newArray[$key]['CategoriesXTasksID'] = $e4[$key];
        }
        return $newArray;
    }    
    
    
    public function sortCCT($array)
    { 
        $array['clients'] = $this->sortClientsCCT($array['clients']);
        return $array;
    }
    public function sortClientsCCT($array)
    {
        $e1 = array();
        $e2 = array();
        foreach($array as $key => $row)
        {
            $e1[$key]  = $row['name'];
            $e2[$key]  = $this->sortCategoriesCCT($row['categories']);
        }
        asort($e1);
        $newClientsArray = array();
        foreach($e1 as $key => $row)
        {
            $newClientsArray[$key]['name'] = $row;
            $newClientsArray[$key]['categories'] = $e2[$key];
        }
        return $newClientsArray;
    }
    public function sortCategoriesCCT($array)
    {
        $e1 = array();
        $e2 = array();
        foreach($array as $key => $row)
        {
            $e1[$key]  = $row['name'];
            if(isset($row['tasks']))
            {
                $e2[$key]  = $this->sortTasksCCT($row['tasks']);
            }
        }
        asort($e1);
        $newCategoriesArray = array();
        foreach($e1 as $key => $row)
        {
            $newCategoriesArray[$key]['name'] = $row;
            if(isset($e2[$key])) $newCategoriesArray[$key]['tasks'] = $e2[$key];
        }
        return $newCategoriesArray;
    }    
    public function sortTasksCCT($array)
    {
        $e1 = array();

        foreach($array as $key => $row)
        {
            $e1[$key]  = $row['name'];
        }
        asort($e1);
        $newTasksArray = array();
        foreach($e1 as $key => $row)
        {
            $newTasksArray[$key]['name'] = $row;
        }
        return $newTasksArray;
    }
    

    
    

}
?>
