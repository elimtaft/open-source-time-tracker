<?php
/******************************************************************************
 * Class name: Zend_Controller_Action_Helper_Logger
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      Logs the activity when an Admin edits another user's timesheet information.
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Zend_Controller_Action_Helper_Logger extends
                Zend_Controller_Action_Helper_Abstract
{
    
    public function logTimeSheetEdit($TimesheetID, $Date, $EditorEmployeeID, $newValues) {
        
        $tsMapper = new Application_Model_TimesheetMapper();
        $ts = new Application_Model_Timesheet();
        $tsMapper->find($TimesheetID, $ts);
        
        $description = '';
        
        if($ts->getCategoriesXTasksID() != $newValues['task'])
        {
            $description .= 'CategoriesXTasksID:' 
                . $ts->getCategoriesXTasksID().'|'
                . $newValues['task']
                . "\r\n";
        }
        
        $newValuesDate = date("Y-m-d", strtotime($newValues['date']));
        if($ts->getDate() != $newValuesDate)
        {
            $description .= 'Date:' 
                . $ts->getDate().'|'
                . $newValuesDate
                . "\r\n";
        }

        if($ts->getDuration() != $newValues['duration'])
        {
            $description .= 'Duration:' 
                . $ts->getDuration().'|'
                . $newValues['duration']
                . "\r\n";
        }
        
        if($ts->getComments() != $newValues['comments'])
        {
            $description .= 'Curation:' 
                . $ts->getComments().'|'
                . $newValues['comments']
                . "\r\n";
        }
            
        $LoggerMapper = new Application_Model_LoggerMapper();
        $Logger = new Application_Model_Logger();
        $Logger->setTimesheetID($TimesheetID)
               ->setDate($Date)
               ->setEditorEmployeeID($EditorEmployeeID)
               ->setEditDescription($description);
        $LoggerMapper->save($Logger);
        
    }
}
?>
