<?php
/******************************************************************************
 * Class name: Zend_Controller_Action_Helper_AdminReporting
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      All the ugly PHP + HTML code for displaying advanced reporting for
 *      administrators /admin/reporting
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Zend_Controller_Action_Helper_Reporting extends
                Zend_Controller_Action_Helper_Abstract
{
    
    private $_fromDate;
    private $_toDate;
    private $_employeeID;
    private $_baseURI;
    private $_employeeClientIDs;
    private $_employeeCategoryIDs;
    private $_employeeCxtIDs;
    
    
    
    public function createReport($fromDate, $toDate, $employeeID = null) {
        
        $this->_baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
        
        $this->_fromDate = $fromDate;
        $this->_toDate = $toDate;
        $fromDate = date('m/d/Y', strtotime($this->_fromDate));
        $toDate = date('m/d/Y', strtotime($this->_toDate));

        $exportLink = '<a id="export" href="'.$this->_baseURI.'/reporting/export/?employee='.$employeeID.'&fromDate='.$fromDate.'&toDate='.$toDate.'">';
        $exportLink .= 'Export';
        $exportLink .= '</a>';
        
        $HTML  = "\r\n"; 
        $HTML .= '<div class="filter">'."\r\n";
        $HTML .= '  <div id="filter-form1">'."\r\n";
        $HTML .= '    <form method="post" action="'.$this->_baseURI.'/reporting">'."\r\n";
        $HTML .= '      <label for="fromDate">From: </label>'."\r\n";
        $HTML .= '      <input id="fromDate" type="text" value="'.$fromDate.'" name="fromDate">'."\r\n";
        $HTML .= '      <label for="toDate">To: </label>'."\r\n";
        $HTML .= '      <input id="toDate" type="text" value="'.$toDate.'" name="toDate">'."\r\n";
        $HTML .= '      <label for="EmployeeID" id="forEmployeeID">For Employee: </label>'."\r\n";
        $HTML .= '      <select id="EmployeeID" name="EmployeeID">'."\r\n";
        $HTML .= '        '.$this->getEmployeeDropDownList()."\r\n";
        $HTML .= '      </select>'."\r\n";
        $HTML .= '      <input type="submit" value="Update Filter">'."\r\n";        
        $HTML .= '    </form>'."\r\n";
        $HTML .= '  '.$exportLink."\r\n";
        $HTML .= '  </div>'."\r\n";
        $HTML .= '</div>'."\r\n";
        $HTML .= '<div class="wrapper">'."\r\n";

        // If requesting data for only one employee, grab all category IDs,
        // Client IDs and CategoryXTaskIDs for employee, return false if no
        // data yet.
        if($employeeID && is_numeric($employeeID))
        {
            $this->_employeeID = $employeeID;
            $this->setUpEmployeeValues();
            if(!$this->_employeeCxtIDs)
            {
                $HTML .= '  <div class="summary">'."\r\n";
                $HTML .= '    <div class="name">No data to display</div>'."\r\n";
                $HTML .= '  </div>'."\r\n";
                return $HTML;                
            }
        }
        
        // Total Hours
        $HTML .= '  <div class="summary">'."\r\n";
        $HTML .= '    <div class="name"><a id="totalName" href="#">Total Hours</a></div>'."\r\n";
        $HTML .= '    <div class="hours">' . $this->getTotalHoursFromRange() . '</div>'."\r\n";
        $HTML .= '  </div>'."\r\n";
        
        $HTML .= '</div> <!-- end .wrapper -->'."\r\n";
        return $HTML; // Below needs to be optimized before put into production.  Developed based on a much smaller system.
        
        
        /*
        $HTML .= '  <div class="details" id="totalDetails">'."\r\n";
        $HTML .= '    <div class="filter">Clients:</div>'."\r\n";
        
                // By Client (Loop)

                $clientMapper = new Application_Model_ClientMapper();
                $clients = $clientMapper->fetchAll($where);        

                foreach($clients as $client)
                {
                    $HTML .= '  <div class="wrapper">'."\r\n";
                    $HTML .= '    <div class="summary">'."\r\n";
                    $HTML .= '      <div class="name"><a href="#" class="openClient" id="'.$client->getClientID().'">'.$client->getName().'</a></div>'."\r\n";
                    $HTML .= '      <div class="hours">'.$this->getTotalHoursByClientID($client->getClientID()).'</div>'."\r\n";
                    $HTML .= '    </div>'."\r\n";
                    $HTML .= '    <div class="details" id="detailsClientId'.$client->getClientID().'">'."\r\n";
                    $HTML .= '      <div class="filter">Categories:</div>'."\r\n";
                    
                    // By Category (Loop)
                    $categoriesWhere = 'ClientID = ' . $client->getClientID();
                    if($this->_employeeID)
                    {
                        $categoriesWhere .= ' AND CategoryID IN ('.$this->_employeeCategoryIDs.')';
                    }
                    $categories = $this->getCategoriesWhere($categoriesWhere);

                    foreach($categories as $category)
                    {
                        
                        $HTML .= '    <div class="wrapper">'."\r\n";
                        $HTML .= '      <div class="summary">'."\r\n";
                        $HTML .= '        <div class="name"><a href="#" class="openCategory" id="'.$category->getCategoryID().'">'.$category->getName().'</a></div>'."\r\n";
                        $HTML .= '        <div class="hours">'.$this->getTotalHoursByCategoryID($category->getCategoryID()).'</div>'."\r\n";
                        $HTML .= '      </div>'."\r\n";
                        $HTML .= '      <div class="details" id="detailsCategoryId'.$category->getCategoryID().'">'."\r\n";
                        $HTML .= '        <div class="filter">Tasks:</div>'."\r\n";
                        

                            // By Task (Loop)
                            
                            $cxts = $this->getCategoryTasksWhere('CategoryID = ' . $category->getCategoryID());
                            $taskMapper = new Application_Model_TaskMapper();
                            $task = new Application_Model_Task();
                            
                            foreach($cxts as $cxt)
                            {
                                $taskMapper->find($cxt->getTaskID(),$task);
                                
                                $HTML .= '    <div class="wrapper">'."\r\n";
                                $HTML .= '      <div class="summary">'."\r\n";
                                $HTML .= '        <div class="name">'.$task->getName().'</div>'."\r\n";
                                $HTML .= '        <div class="hours">'.$this->getTotalHoursByCXT($cxt->getCategoriesXTasksID()).'</div>'."\r\n";
                                $HTML .= '      </div>'."\r\n";
                                $HTML .= '    </div>'."\r\n";
                            }
                        
                        $HTML .= '      </div>'."\r\n";
                        $HTML .= '    </div>'."\r\n";
                    }

                    
                    
                    $HTML .= '    </div>'."\r\n";
                    $HTML .= '  </div>'."\r\n";
                }
        $HTML .= '  </div> <!-- end .details -->'."\r\n";
        $HTML .= '</div> <!-- end .wrapper -->'."\r\n";

        return $HTML;
         * 
         */
    }

    public function setUpEmployeeValues()
    {
        
        // Get Category IDs
        $excMapper = new Application_Model_EmployeeCategoriesMapper();
        $excWhere = 'EmployeeID = ' . $this->_employeeID;
        $excs = $excMapper->fetchAll($excWhere);
        if(count($excs) > 0)
        {
            
            foreach($excs as $exc)
            {
                $this->_employeeCategoryIDs .= $exc->getCategoryID() . ',';
            }
            $this->_employeeCategoryIDs = substr($this->_employeeCategoryIDs,0,-1);
            
            // Get Client IDs
            $actMapper = new Application_Model_CategoryMapper();
            $actWhere = 'CategoryID IN (' . $this->_employeeCategoryIDs . ')';
            $categories = $actMapper->fetchAll($actWhere);
            $clientIDsArray = array();
            if(count($categories) > 0)
            {
                foreach($categories as $category)
                {
                    if(!in_array($category->getClientID(),$clientIDsArray))
                    {
                        $clientIDsArray[] = $category->getClientID();
                        $this->_employeeClientIDs .= $category->getClientID() . ',';
                    }
                }
                $this->_employeeClientIDs = substr($this->_employeeClientIDs, 0, -1);
            }
            

            // Get Category Task IDs
            $cxtMapper = new Application_Model_CategoryTasksMapper();
            $cxtWhere = 'CategoryID IN (' . $this->_employeeCategoryIDs . ')';
            $cxts = $cxtMapper->fetchAll($cxtWhere);
            $cxtsArray = array();
            if(count($cxts) > 0)
            {
                foreach($cxts as $cxt)
                {
                    if(!in_array($cxt->getCategoriesXTasksID(),$cxtsArray))
                    {
                        $cxtsArray[] = $cxt->getCategoriesXTasksID();
                        $this->_employeeCxtIDs .= $cxt->getCategoriesXTasksID() . ',';
                    }
                }
                $this->_employeeCxtIDs = substr($this->_employeeCxtIDs, 0, -1);
            }
        }
    }

    public function getEmployeeDropDownList()
    {
        $HTML = '      <option>All</option>'."\r\n";
        $empMapper = new Application_Model_EmployeeMapper();
        $employees = $empMapper->fetchAll();
        if(count($employees) > 0)
        {
            foreach($employees as $employee)
            {
                if($this->_employeeID && $this->_employeeID == $employee->getEmployeeID())
                {
                    $selected = ' selected="selected"';
                } else {
                    $selected = '';
                }
                
                $HTML .= '      <option value="'.$employee->getEmployeeID().'"'.$selected.'>'.$employee->getName().'</option>'."\r\n";
            }
        }
        return $HTML;
    }
        
        
    public function getTotalHoursFromRange()
    {
        $totalHours = 0;
        if($this->_employeeID)
        {
            $where = 'EmployeeID = ' . $this->_employeeID . ' AND '.
                     '(Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\')';
        } else {
            $where = 'Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\'';
        }

        $timesheetEntries = $this->getTimeSheetEntriesWhere($where);
        
        foreach($timesheetEntries as $timesheet)
        {
            list ($hr, $min) = explode(':', $timesheet->getDuration());
            $hr = $hr * 60; // convert hours to minutes, then add to minutes for total time in minutes
            $duration = $hr + $min;
            $totalHours += $duration;
        }
        $totalHours = $this->convertMinutesToHours($totalHours);
        return $totalHours;
    }

    public function getTotalHoursByClientID($ClientID)
    {
        $totalHours = 0;

        $categories = $this->getCategoriesWhere('ClientID = ' . $ClientID);
        if(count($categories) < 1) return 0;
        $actIDs = '';
        foreach($categories as $act)
        {
            
            if($this->_employeeID)
            {
                $excMapper = new Application_Model_EmployeeCategoriesMapper();
                $excWhere = 'EmployeeID = ' . $this->_employeeID;
                $excs = $excMapper->fetchAll($excWhere);
                if(count($excs) > 0)
                {
                    $actIDs .= $act->getCategoryID().',';
                }
            } else {
                $actIDs .= $act->getCategoryID().',';
            }
        }
        $actIDs = substr($actIDs,0,-1);
        
        $cxts = $this->getCategoryTasksWhere('CategoryID IN ('.$actIDs.')');
        if(count($cxts) < 1) return 0;
        $cxtIDs = '';
        foreach($cxts as $cxt)
        {
            $cxtIDs .= $cxt->getCategoriesXTasksID().', ';
        }
        $cxtIDs = substr($cxtIDs,0,-2);

        if($this->_employeeID) {
            $where = 'EmployeeID = ' . $this->_employeeID . 
            ' AND (Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\')' .
            ' AND CategoriesXTasksID IN ( '.$cxtIDs.' )';
        } else {
            $where = '(Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\')' .
            ' AND CategoriesXTasksID IN ( '.$cxtIDs.' )';
        }

        $timesheetEntries = $this->getTimeSheetEntriesWhere($where);
        if(count($timesheetEntries) < 1) return 0;

        foreach($timesheetEntries as $timesheet)
        {
            list ($hr, $min) = explode(':', $timesheet->getDuration());
            $hr = $hr * 60; // convert hours to minutes, then add to minutes for total time in minutes
            $duration = $hr + $min;
            $totalHours += $duration;
        }
        
        $totalHours = $this->convertMinutesToHours($totalHours);
        return $totalHours;
    }

    public function getTotalHoursByCategoryID($CategoryID)
    {
        $totalHours=0;
        
        $cxts = $this->getCategoryTasksWhere('CategoryID = '.$CategoryID);
        if(count($cxts) < 1) return 0;
        $cxtIDs = '';
        foreach($cxts as $cxt)
        {
            $cxtIDs .= $cxt->getCategoriesXTasksID().', ';
        }
        $cxtIDs = substr($cxtIDs,0,-2);

        
        if($this->_employeeID)
        {
            $where = 'EmployeeID = ' . $this->_employeeID . ' AND ' .
                     '(Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\') AND ' . 
                     'CategoriesXTasksID IN ( '.$cxtIDs.' )';
        } else {
            $where = '(Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\') AND ' . 
                     'CategoriesXTasksID IN ( '.$cxtIDs.' )';
        }
        
        
        $timesheetEntries = $this->getTimeSheetEntriesWhere($where);
        if(count($timesheetEntries) < 1) return 0;
        foreach($timesheetEntries as $timesheet)
        {
            list ($hr, $min) = explode(':', $timesheet->getDuration());
            $hr = $hr * 60; // convert hours to minutes, then add to minutes for total time in minutes
            $duration = $hr + $min;
            $totalHours += $duration;
        }
        
        $totalHours = $this->convertMinutesToHours($totalHours);
        return $totalHours;
    }
    
    public function getTotalHoursByCXT($cxtID)
    {
        $totalHours = 0;
        
        if($this->_employeeID)
        {
            $where = 'EmployeeID = ' . $this->_employeeID . ' AND ' .
                     '(Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\') AND ' . 
                     'CategoriesXTasksID = '.$cxtID;
        } else {
            $where = '(Date >= \''.$this->_fromDate.'\' AND Date <= \''.$this->_toDate.'\') AND ' . 
                     'CategoriesXTasksID = '.$cxtID;
        }
        
        $timesheetEntries = $this->getTimeSheetEntriesWhere($where);
        
        if(count($timesheetEntries) < 1) return 0;
        foreach($timesheetEntries as $timesheet)
        {
            list ($hr, $min) = explode(':', $timesheet->getDuration());
            $hr = $hr * 60; // convert hours to minutes, then add to minutes for total time in minutes
            $duration = $hr + $min;
            $totalHours += $duration;
        }
        
        $totalHours = $this->convertMinutesToHours($totalHours);
        return $totalHours;
        
    }
    
    public function getTimeSheetEntriesWhere($where = null)
    {
        $tsMapper = new Application_Model_TimesheetMapper();
        return $tsMapper->fetchAll($where);
   }
    
    public function getCategoriesWhere($where = null)
    {
        $categoryMapper = new Application_Model_CategoryMapper();
        return $categoryMapper->fetchAll($where);
    }
    
    public function getCategoryTasksWhere($where = null)
    {
        $cxtMapper = new Application_Model_CategoryTasksMapper();
        return $cxtMapper->fetchAll($where);
    }
    
    public function convertMinutesToHours($minutes)
    {
        $decimalTime = (float) ($minutes / 60);
        $decimalTime = number_format($decimalTime,2);

        list ($hr, $min) = explode('.', $decimalTime);
        if($min == 0)
        {
            $totalHours = $hr . ':00';
        } else {
            $min = str_pad($min, 2, "0", STR_PAD_RIGHT);
            $newMin = round(60 / (100 / $min));
            $newMin = str_pad($newMin, 2, "0", STR_PAD_LEFT);
            $totalHours = $hr . ':' . $newMin;
        }

        return $totalHours;
    }
}
?>
