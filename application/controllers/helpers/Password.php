<?php
/******************************************************************************
 * Class name: Zend_Controller_Action_Helper_Password
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      Used for common / shared password functions.
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Zend_Controller_Action_Helper_Password extends
                Zend_Controller_Action_Helper_Abstract
{
    
    public function hashItOut($username, $pass)
    {
        return hash('sha512', $username . $pass);
    }
    
    public function generateNew($username,$length=9,$strength=4)
    {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength >= 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength >= 2) {
            $vowels .= "AEUY";
        }
        if ($strength >= 4) {
            $consonants .= '23456789';
        }
        if ($strength >= 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }
}
?>
