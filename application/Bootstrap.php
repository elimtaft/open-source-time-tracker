<?php
/******************************************************************************
 * Class name: Bootstrap
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      The bootstrap loads initial data needed for the application to run
 *      properly.
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initActionHelpers() {
		Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/controllers/helpers');
	}
    
	protected function _initRegistry() {
		$registry = new Zend_Registry();
		return $registry;
	}

	protected function _initConfig() {
		Zend_Registry::set('config', new Zend_Config($this->getOptions()));
	}

	protected function _initDocRoot() {
		$localpath=getenv("SCRIPT_NAME");
		$absolutepath=getenv("SCRIPT_FILENAME");
		$_SERVER['DOCUMENT_ROOT'] = substr($absolutepath,0,strpos($absolutepath,$localpath));
	}
        
	protected function _initView() {
		$view = new Zend_View();
		$view->doctype('XHTML1_STRICT');
		$view->headTitle('Open Source Time Tracker');
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
		$viewRenderer->setView($view);
		return $view;
	}

        protected function _initEmployeeInfo()
        {
            Zend_Registry::set('EmployeeInfo', false);
        }

        protected function _initBaseUrl()
        {
            $options = $this->getOptions();
            $baseUrl = isset($options['settings']['baseUrl']) 
                ? $options['settings']['baseUrl']
                : null;  // null tells front controller to use autodiscovery, the default
            $this->bootstrap('frontcontroller');
            $front = $this->getResource('frontcontroller');
            $front->setBaseUrl($baseUrl);
        }
    
}

