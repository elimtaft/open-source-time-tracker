<?php

/******************************************************************************
 * Class name: Timetracker_Controller_Plugin_CheckSetup
 * Author: Matthew Taft
 * Date: 02/01/2012
 * Description:
 * 
 *      This class is defined as a front controller plugin in 
 *      /application/configs/application.ini on this line:
 * 
 *      resources.frontController.plugins.CheckSetup = "Timetracker_Controller_Plugin_CheckSetup"
 * 
 *      If any of the functions below result in an error (setup not yet complete),
 *      this plugin tells the tool to redirect to /index, passing the parameter "error" with a value.
 *      
 *      If this plugin detects that the error parameter has been passed, it exits.
 *      The IndexController.php function CheckSetupComplete() looks for this parameter
 *      and displays pertinent information on how to complete the setup process.  In 
 *      this way, this plugin and the IndexController.php communicate back and forth
 *      until the application has all it needs to continue.
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0 (GPL-3.0) 
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/GPL-3.0
 *  
 ******************************************************************************/

class Timetracker_Controller_Plugin_CheckSetup extends Zend_Controller_Plugin_Abstract {
    
    protected $_config;
    protected $_dbconfig;
    protected $_dbh; // database handle
    protected $_baseURI;
    
  
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        
        $this->_baseURI = Zend_Controller_Front::getInstance()->getBaseUrl();
        
        if($this->_request->getParam('error') != '')
        {
            return;
        }
        
        $this->_config = Zend_Registry::get('config');
        
        if($this->checkDbVariablesSet()==false)
        {
            $this->_response->setRedirect('index?error=DbVariablesNotSet'); 
            return;
        }

        if($this->checkDbConnection()==false)
        {
            $this->_response->setRedirect('index?error=DbConnectionInvalid'); 
            return;
        }
        
        if($this->checkDbDatabase()==false)
        {
            $this->_response->setRedirect('index?error=DbDatabaseNameInvalid'); 
            return;
        }
        
        if($this->checkDbTables()==false)
        {
            $this->_response->setRedirect('index?error=DbTablesMissing'); 
            return;
        }
        if($this->checkAdminAccount()==false)
        {
            $this->_response->setRedirect('index?error=NoAdminAccount'); 
            return;
        }

    }
    
    // Are the database settings defined in /application/configs/application.ini?
    private function checkDbVariablesSet()
    {
        $this->_dbconfig = $this->_config->resources->db;
        
        if(
           (!isset($this->_dbconfig->params->host)     || $this->_dbconfig->params->host=='') ||
           (!isset($this->_dbconfig->params->username) || $this->_dbconfig->params->username=='') ||
           (!isset($this->_dbconfig->params->password) || $this->_dbconfig->params->password=='') ||
           (!isset($this->_dbconfig->params->dbname)   || $this->_dbconfig->params->dbname=='') 
          )
        {
            return false;
        } else {
            return true;
        }       
    }
    
    // Are the database settings valid?
    private function checkDbConnection()
    {
        $this->_dbh = @mysql_connect(
                $this->_dbconfig->params->host, 
                $this->_dbconfig->params->username, 
                $this->_dbconfig->params->password);
        if(!$this->_dbh)
        {
            return false;
        } else {
            return true;
        }
    }
    
    // Is the defined database accessible?
    private function checkDbDatabase()
    {
        $rslt = @mysql_select_db($this->_dbconfig->params->dbname, $this->_dbh);
        
        if(!$rslt)
        {
            return false;
        } else {
            return true;
        }        
    }
    
    // Are the already tables in the database?
    private function checkDbTables()
    {
        $sql = 'SHOW TABLES;';
        $rslt = mysql_query($sql, $this->_dbh);
        if($rslt)
        {
            if(mysql_num_rows($rslt) > 0)
            {
                return true;
            } else {
                
                // Important: When this function first results in an error, then
                // IndexController.php asks if the tool should create the default tables.
                // If the user clicks "Yes," the tool redirects to /index, 
                // passing the parameter "createTables" with the value of "Yes."
                // When the redirect occurs, just like with every page request, this
                // plugin is called, and we're back here because there are still no
                // tables.  Then we check for the parameter "createTables" with the
                // value of "yes."  If it's there, we create the tables, redirect again
                // to /index, and we're on to the next step.
                $createTables = $this->_request->getParam('createTables');
                if($createTables && $createTables=='yes')
                {

                    $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                    $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
                    

                    
                    $select = 'CREATE TABLE IF NOT EXISTS `categories` (
                        `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
                        `ClientID` int(11) NOT NULL,
                        `Name` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        `Description` varchar(500) COLLATE latin1_general_ci NOT NULL,
                        `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`CategoryID`),
                        KEY `Name` (`Name`),
                        KEY `ClientID` (`ClientID`,`Name`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `categories` (
                        `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
                        `ClientID` int(11) NOT NULL,
                        `Name` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        `Description` varchar(500) COLLATE latin1_general_ci NOT NULL,
                        `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`CategoryID`),
                        KEY `Name` (`Name`),
                        KEY `ClientID` (`ClientID`,`Name`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `categoriesxtasks` (
                        `CategoriesXTasksID` int(11) NOT NULL AUTO_INCREMENT,
                        `CategoryID` int(11) NOT NULL,
                        `TaskID` int(11) NOT NULL,
                        `Active` tinyint(4) NOT NULL,
                        PRIMARY KEY (`CategoriesXTasksID`),
                        KEY `CategoryID` (`CategoryID`,`TaskID`),
                        KEY `TaskID` (`TaskID`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `clients` (
                        `ClientID` int(11) NOT NULL AUTO_INCREMENT,
                        `Name` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        `City` varchar(100) COLLATE latin1_general_ci NOT NULL,
                        `State` char(2) COLLATE latin1_general_ci NOT NULL,
                        PRIMARY KEY (`ClientID`),
                        KEY `Name` (`Name`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `departments` (
                        `DepartmentID` int(11) NOT NULL AUTO_INCREMENT,
                        `Name` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        PRIMARY KEY (`DepartmentID`),
                        KEY `Name` (`Name`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'INSERT INTO `departments` (
                        `DepartmentID` ,
                        `Name`
                        )
                        VALUES (
                        0, \'Site Admin\'
                        );';
                    $rslt = $db->query($select);
                    
                    
                    $select = 'CREATE TABLE IF NOT EXISTS `employees` (
                        `EmployeeID` int(11) NOT NULL AUTO_INCREMENT,
                        `DepartmentID` int(11) NULL,
                        `Name` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        `Username` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        `Password` varchar(128) COLLATE latin1_general_ci NOT NULL,
                        `IsAdmin` tinyint(4) NOT NULL,
                        `IsReporter` int(11) NOT NULL,
                        `Active` int(11) NOT NULL,
                        PRIMARY KEY (`EmployeeID`),
                        KEY `Username` (`Username`),
                        KEY `DepartmentID` (`DepartmentID`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `employeesxcategories` (
                        `EmployeesXCategoriesID` int(11) NOT NULL AUTO_INCREMENT,
                        `EmployeeID` int(11) NOT NULL,
                        `CategoryID` int(11) NOT NULL,
                        PRIMARY KEY (`EmployeesXCategoriesID`),
                        KEY `CategoryID` (`CategoryID`,`EmployeeID`),
                        KEY `EmployeeID` (`EmployeeID`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `logger` (
                        `LogID` int(11) NOT NULL AUTO_INCREMENT,
                        `TimesheetID` int(11) NOT NULL,
                        `Date` varchar(20) COLLATE latin1_general_ci NOT NULL,
                        `EditorEmployeeID` int(11) NOT NULL,
                        `EditDescription` varchar(500) COLLATE latin1_general_ci NOT NULL COMMENT \'Field:Old|New\',
                        PRIMARY KEY (`LogID`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `tasks` (
                        `TaskID` int(11) NOT NULL AUTO_INCREMENT,
                        `Name` varchar(250) COLLATE latin1_general_ci NOT NULL,
                        `Description` varchar(500) COLLATE latin1_general_ci NOT NULL,
                        PRIMARY KEY (`TaskID`),
                        KEY `Name` (`Name`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);

                    $select = 'CREATE TABLE IF NOT EXISTS `timesheet` (
                        `TimeSheetID` int(11) NOT NULL AUTO_INCREMENT,
                        `EmployeeID` int(11) NOT NULL,
                        `CategoriesXTasksID` int(11) NOT NULL,
                        `Date` date NOT NULL,
                        `Duration` varchar(5) COLLATE latin1_general_ci NOT NULL,
                        `Comments` text COLLATE latin1_general_ci NOT NULL,
                        PRIMARY KEY (`TimeSheetID`),
                        KEY `EmployeeID` (`EmployeeID`,`Date`),
                        KEY `CategoriesXTasksID` (`CategoriesXTasksID`,`EmployeeID`),
                        KEY `Date` (`Date`,`CategoriesXTasksID`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;';
                    $rslt = $db->query($select);
        
                    return true;                    
                } else {
                    return false;
                }
                
                return false;
            };
        }
    }
    
    private function checkAdminAccount()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $db = New Zend_Db_Adapter_Pdo_Mysql($dbAdapter->getConfig());
                $select = $db->select()
                     ->from(
                            array('employees'),
                            array('EmployeeID')
                        )
                     ->where('IsAdmin');
        $statement = $db->query($select);
        $employees = $statement->fetchAll();
        if(count($employees) > 0)
        {
            return true;
        } else {
            
            // Get the ID of the Site Admin department created in the last function.
            // Sure I could guess that it's still 1, but the user might have tried to
            // be smart and change it, so search by name.
            $dptMapper = new Application_Model_DepartmentMapper();
            $dptWhere = 'Name = \'Site Admin\'';
            $dpts = $dptMapper->fetchAll();
            if(count($dpts) ==0)
            {
                // Huh?  If not there, we've got a problem.  I guess just make it 0.
                $DepartmentID = 0;
            } else {
                $DepartmentID = $dpts[0]->getDepartmentID();
            }
            
            // Important: When this function first results in an error, then
            // IndexController.php displays a form for the user to fill out
            // info for the main site admin.  When the user submits that, 
            // the tool redirects to /index, passing the values from the form via
            // post.  When the redirect occurs, just like with every page request, this
            // plugin is called, and we're back here because there is still no
            // main site admin account.  Then we check for form submitted values.
            // If they are there, use them.  If not, result in error so the user
            // sees that screen again until they do it.
            $name = $this->_request->getParam('name');
            $username = $this->_request->getParam('username');
            $password = $this->_request->getParam('password');
            
            if($name && $username && $password)
            {
                    $empMapper = new Application_Model_EmployeeMapper();
                    $emp = new Application_Model_Employee();
                    $emp->setName($name)
                        ->setUsername($username)
                        ->setPassword(hash('sha512', $username . $password))
                        ->setDepartmentID($DepartmentID) // Site Admin Department
                        ->setActive(1)
                        ->setIsReporter(1)
                        ->setIsAdmin(1);
                    $empMapper->save($emp);
                    return true;

            } else {
                return false;
            }
            
            return false;
        }
    }
}